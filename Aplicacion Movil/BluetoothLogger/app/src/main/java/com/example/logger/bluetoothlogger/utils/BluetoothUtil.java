package com.example.logger.bluetoothlogger.utils;

import android.Manifest;
import android.bluetooth.*;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class BluetoothUtil {

    private static String TAG = BluetoothUtil.class.getSimpleName();
    public static final int BLUETOOTH_REQUEST = 0;
    public static final int REQUEST_ENABLE_BT = 3;
    private String permissionsToRequest [];
    private static long STOP_TIME_SCAN = 3500;
    private List<BluetoothDevice> listaDispositivos;

    private BluetoothAdapter bluetoothAdapter;
    private AppCompatActivity activity;

    public static UUID convertFromInteger(int i) {
        final long MSB = 0x0000000000001000L;
        final long LSB = 0x800000805f9b34fbL;
        long value = i & 0xFFFFFFFF;
        return new UUID(MSB | (value << 32), LSB);
    }

    public BluetoothUtil (AppCompatActivity activity) {
        this.activity = activity;
        this.listaDispositivos = new ArrayList<>();

        permissionsToRequest = new String[] {
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.BLUETOOTH,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
    }

    public BluetoothAdapter habilitarBluetooth () {

        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
            BluetoothManager manager = (BluetoothManager) activity.getSystemService(activity.BLUETOOTH_SERVICE);

            if (manager.getAdapter() != null) {
                bluetoothAdapter = manager.getAdapter();
            } else {
                bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            }
        } else {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        return bluetoothAdapter;
    }

    public void apagarBluetooth () {
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.cancelDiscovery();
            bluetoothAdapter.disable();
        }
    }

    public void prenderBluetooth () {
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    public void buscarLow() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                    .build();
        }
    }

    public void findDevices() {
        //Comenzamos la busqueda de dispositivos.
        bluetoothAdapter.startDiscovery();
        TimerTask timerTask = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            final BluetoothLeScanner scanner = bluetoothAdapter.getBluetoothLeScanner();

            timerTask = new TimerTask() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    scanner.stopScan(callback);
                }
            };

            scanner.startScan(callback);
        } else {

            timerTask = new TimerTask() {
                @Override
                public void run() {
                    bluetoothAdapter.stopLeScan(leScanCallback);
                }
            };

            bluetoothAdapter.startLeScan(leScanCallback);
        }

        new Timer().schedule(timerTask, STOP_TIME_SCAN);
    }

    public List<BluetoothDevice> listaDispositivosVistos() {
        List<BluetoothDevice> devices = new ArrayList<>();
        if ( ! bluetoothAdapter.getBondedDevices().isEmpty() && bluetoothAdapter.isEnabled()) {
            for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
                devices.add(device);

                Log.e(TAG, "Nombre: " + device.getName() +
                        " Direccion: " + device.getAddress() + " state: " + device.getBondState());
            }
        }
        return devices;
    }

    public void mostrarDialog () {
        Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);

        activity.startActivityForResult(enableBluetoothIntent, BLUETOOTH_REQUEST);
    }

    public BroadcastReceiver getBluetoothReceiver() {
        return bluetoothReceiver;
    }

    /************             CallBacks            ************/
    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action != null) {
                switch (action) {
                    case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                        Log.e(TAG, action);

                        break;
                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        Log.e(TAG, action);
                        break;
                    case BluetoothDevice.ACTION_FOUND:

                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                        Log.e(TAG, action);
                        if (device.getBondState() != BluetoothDevice.BOND_BONDED) {

                            Log.e(TAG, "name: " + device.getName() + " address: " + device.getAddress());

                            listaDispositivos.add(device);
                        }
                        break;
                }
            }
        }
    };

    private ScanCallback callback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

        }
    };

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {

        }
    };
}
