package com.example.logger.bluetoothlogger.communications;

import android.bluetooth.BluetoothDevice;

public interface ICommunicationFragment {
    public void deviceBluetoothSeleccionado(BluetoothDevice device);
}
