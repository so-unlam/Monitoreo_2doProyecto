package com.example.logger.bluetoothlogger.components;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.logger.bluetoothlogger.R;

import java.util.List;

public class DeviceAdapter extends BaseAdapter {

    private static String TAG = DeviceAdapter.class.getSimpleName();
    private List<BluetoothDevice> devices;
    private LayoutInflater inflater;

    public DeviceAdapter(Context context, List<BluetoothDevice> devices) {
        this.devices = devices;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        if (convertView == null) {
            view = inflater.inflate(R.layout.item_device_finder, null);
        } else {
            view = convertView;
        }

        TextView viewTitulo = (TextView) view.findViewById(R.id.item_listView_titulo);
        TextView viewSubTitulo = (TextView) view.findViewById(R.id.item_listView_subTitulo);

        BluetoothDevice device = devices.get(position);
        viewTitulo.setText(device.getName());
        viewSubTitulo.setText(device.getAddress());

        return view;
    }
}
