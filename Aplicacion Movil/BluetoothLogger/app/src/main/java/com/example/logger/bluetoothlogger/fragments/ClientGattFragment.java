package com.example.logger.bluetoothlogger.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.logger.bluetoothlogger.R;
import com.example.logger.bluetoothlogger.communications.ICommunicationState;
import com.example.logger.bluetoothlogger.utils.BLEUtil;
import com.example.logger.bluetoothlogger.utils.FileUtil;
import com.example.logger.bluetoothlogger.communications.ICommunicationGatt;

import java.util.Timer;
import java.util.TimerTask;

public class ClientGattFragment extends Fragment implements View.OnClickListener, ICommunicationGatt, ICommunicationState {
    public static String TAG = ClientGattFragment.class.getSimpleName();
    private BluetoothDevice device;
    private TextView textView;
    private EditText editText;
    private BLEUtil bleUtil;

    public ClientGattFragment() {
        bleUtil = new BLEUtil();
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_client_mode, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        textView = (TextView) getActivity().findViewById(R.id.fragment_client_textMensaje);
        textView.append("\n");
        editText = (EditText) getActivity().findViewById(R.id.fragment_client_editText);
        ImageButton button = (ImageButton) getActivity().findViewById(R.id.fragment_client_button);
        button.setOnClickListener(this);

        bleUtil.setContext(getActivity());

        startCommunication(getActivity(),false, device);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_client_button:

                String mensaje = editText.getText().toString();
                bleUtil.sendMessage(mensaje);

                editText.setText("");
                break;
        }
    }

    @Override
    public void onMessageRead(String message) {

        textView.append(message);
        Log.e(TAG, "Llego mensaje: " + message);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        bleUtil.disconnect();
    }

    @Override
    public void startCommunication(Activity activity, boolean autoConnect, BluetoothDevice device) {
        bleUtil.setOnListener(this);
        bleUtil.startCommunication(activity, autoConnect, device);
    }

    @Override
    public void connect() {
        bleUtil.startCommunication(getActivity(), false, device);
    }

    @Override
    public void disconnect() {
        bleUtil.disconnect();
    }
}
