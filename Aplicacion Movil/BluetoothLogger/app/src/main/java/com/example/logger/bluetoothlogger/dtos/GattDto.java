package com.example.logger.bluetoothlogger.dtos;

import java.util.List;
import java.util.UUID;

public class GattDto {
    private UUID service;
    private List<UUID> uuids;

    public UUID getService() {
        return service;
    }

    public void setService(UUID service) {
        this.service = service;
    }

    public List<UUID> getUuids() {
        return uuids;
    }

    public void setUuids(List<UUID> uuids) {
        this.uuids = uuids;
    }

    public void addUuid(UUID uuidString) {
        this.uuids.add(uuidString);
    }
}
