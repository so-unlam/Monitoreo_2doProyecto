package com.example.logger.bluetoothlogger.components;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.example.logger.bluetoothlogger.R;
import com.example.logger.bluetoothlogger.utils.BluetoothUtil;

public class PopupComponent extends DialogFragment {

    private String titulo;
    private BluetoothUtil util;

    public void setBluetooth(BluetoothUtil util) {
        this.util = util;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(titulo)
                .setPositiveButton(R.string.popup_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        util.prenderBluetooth();
                    }
                })
                .setNegativeButton(R.string.popup_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        return builder.create();
    }
}
