package com.example.logger.bluetoothlogger.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.logger.bluetoothlogger.R;
import com.example.logger.bluetoothlogger.communications.ICommunicationGatt;
import com.example.logger.bluetoothlogger.communications.ICommunicationState;
import com.example.logger.bluetoothlogger.communications.ICommunicationFragment;
import com.example.logger.bluetoothlogger.utils.BLEUtil;
import com.example.logger.bluetoothlogger.utils.FileUtil;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class LoggerFragment extends Fragment implements ICommunicationGatt, ICommunicationState, View.OnClickListener {
    public static String TAG = LoggerFragment.class.getSimpleName();
    private ICommunicationFragment iCommunication;
    private BluetoothDevice device;
    private BLEUtil bleUtil;
    private FileUtil fileUtil;
    private Button btnIniciarLog;
    private Button btnFinalizarLog;


    private int VALOR_RETORNO = 9999;
    private String COMANDO_INICIAR = "inicio\r\n";
    private String COMANDO_FINALIZAR = "fin\r\n";

    public LoggerFragment() {
        bleUtil = new BLEUtil();
    }

    public void setCommunication(ICommunicationFragment iCommunication) {
        this.iCommunication = iCommunication;
    }

    public void setDevicetConnection(BluetoothDevice device) {
        this.device = device;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_logger, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnIniciarLog = (Button) getActivity().findViewById(R.id.btnIniciarLog);
        btnFinalizarLog = (Button) getActivity().findViewById(R.id.btnFinalizarLog);

        btnIniciarLog.setOnClickListener(this);
        btnFinalizarLog.setOnClickListener(this);

        btnFinalizarLog.setEnabled(false);
        btnIniciarLog.setEnabled(true);

        bleUtil.setContext(getActivity());
        startCommunication(getActivity(), false, device);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        bleUtil.disconnect();

    }

    @Override
    public void startCommunication(Activity activity, boolean autoConnect, BluetoothDevice device) {
        bleUtil.setOnListener(this);
        bleUtil.startCommunication(activity, autoConnect, device);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String [] path=null;

        if(resultCode!=-1)
            return;


        if(requestCode==VALOR_RETORNO)
        {
            Log.e("Test", "Result URI " + data.getData().getPath());
            path=data.getData().getPath().split(":");

            if(!(path[0].equals("/tree/primary")))
            {
                Toast.makeText(getActivity().getApplicationContext(), "ERROR NO SE SELECCIONO LA MEMORIA INTERNA ", Toast.LENGTH_SHORT).show();
                return;
            }

            if(fileUtil.createPath(path[1])) {
                btnFinalizarLog.setEnabled(true);
                btnIniciarLog.setEnabled(false);
                bleUtil.sendMessage(COMANDO_INICIAR);

                Toast.makeText(getActivity().getApplicationContext(), "Archivo Log ABIERTO", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getActivity().getApplicationContext(), "Error al crear el Archivo Log", Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void connect() {
        bleUtil.startCommunication(getActivity(), false, device);
    }

    @Override
    public void disconnect() {
        bleUtil.disconnect();
    }

    @Override
    public void onMessageRead(String message) {
        Log.e(TAG, message);
        fileUtil.escribir(message);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnIniciarLog:
                fileUtil = new FileUtil();
                Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                startActivityForResult(Intent.createChooser(i, "Choose directory"), VALOR_RETORNO);
                break;

            case R.id.btnFinalizarLog:
                if(fileUtil.cerrar())
                {
                    btnFinalizarLog.setEnabled(false);
                    btnIniciarLog.setEnabled(true);
                    bleUtil.sendMessage(COMANDO_FINALIZAR);

                    Toast.makeText(getActivity().getApplicationContext(), "Archivo Log CERRADO", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(getActivity().getApplicationContext(), "Error al cerrar el Archivo Log", Toast.LENGTH_SHORT).show();
                }


        }
    }
}
