package com.example.logger.bluetoothlogger.fragments;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.logger.bluetoothlogger.R;
import com.example.logger.bluetoothlogger.utils.BluetoothUtil;
import com.example.logger.bluetoothlogger.components.DeviceAdapter;
import com.example.logger.bluetoothlogger.communications.ICommunicationFragment;

import java.util.List;

public class DeviceFinderFragment extends Fragment {

    public static String TAG = DeviceFinderFragment.class.getSimpleName();
    private ICommunicationFragment iCommunication;
    private ListView listView;
    private List<BluetoothDevice> devices;
    private DeviceAdapter deviceAdapter;
    private BluetoothUtil util;

    public DeviceFinderFragment() {
    }

    public void setCommunication(ICommunicationFragment communication) {
        this.iCommunication = (ICommunicationFragment) communication;
    }

    public void setUtil (BluetoothUtil util) {
        this.util = util;
        this.devices = util.listaDispositivosVistos();
    }

    public void notifyDevices() {
        this.devices = this.util.listaDispositivosVistos();
        deviceAdapter = new DeviceAdapter(getActivity(), devices);
        listView.setAdapter(deviceAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_finder, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        deviceAdapter = new DeviceAdapter(getContext(), devices);

        listView = (ListView) getActivity().findViewById(R.id.fragment_device_finder_listview);
        listView.setAdapter(deviceAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = devices.get(position);

                iCommunication.deviceBluetoothSeleccionado(device);
            }
        });
    }
}
