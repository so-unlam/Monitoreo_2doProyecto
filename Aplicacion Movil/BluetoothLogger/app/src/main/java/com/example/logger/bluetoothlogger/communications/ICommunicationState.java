package com.example.logger.bluetoothlogger.communications;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;

public interface ICommunicationState {
    public void startCommunication(Activity activity, boolean autoConnect, BluetoothDevice device);
    public void connect();
    public void disconnect();
}
