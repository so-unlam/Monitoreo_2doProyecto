package com.example.logger.bluetoothlogger.utils;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.hardware.camera2.CameraCharacteristics;
import android.util.Log;
import android.widget.Toast;

import com.example.logger.bluetoothlogger.communications.ICommunicationGatt;
import com.example.logger.bluetoothlogger.dtos.GattDto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.example.logger.bluetoothlogger.protocols.PROTOCOL_BLUETOOTH.CHARACTERISTIC_READ_STRING;
import static com.example.logger.bluetoothlogger.protocols.PROTOCOL_BLUETOOTH.CHARACTERISTIC_WRITE_STRING;
import static com.example.logger.bluetoothlogger.protocols.PROTOCOL_BLUETOOTH.SERVICE_STRING;

public class BLEUtil {
    private final static String TAG = BLEUtil.class.getSimpleName();
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattCharacteristic characteristic;
    private List<GattDto> gattDtos;
    private UUID gattUUIDService;
    private UUID gattUUIDWrite;
    private Context context;

    private ICommunicationGatt communicationGatt;

    public BLEUtil() {
        gattDtos = new ArrayList<>();
    }

    public void setOnListener (ICommunicationGatt listener) {
        communicationGatt = listener;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void setService (String uuidService, String uuidRead) {
        gattUUIDService = UUID.fromString(uuidService);
        UUID uuid = UUID.fromString(uuidRead);
        characteristic = bluetoothGatt.getService(gattUUIDService).getCharacteristic(uuid);
        bluetoothGatt.setCharacteristicNotification(characteristic, true);
    }

    public void setUUIDService(UUID service, UUID read) {
        gattUUIDService = service;
        characteristic = bluetoothGatt.getService(gattUUIDService).getCharacteristic(read);
        bluetoothGatt.setCharacteristicNotification(characteristic, true);
    }

    public void setCharacteristicRead(String uuidCharacteristic) {
        gattUUIDWrite = UUID.fromString(uuidCharacteristic);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(gattUUIDWrite);

        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        bluetoothGatt.writeDescriptor(descriptor);
    }

    public void setCharacteristicUUIDRead(UUID uuidCharacteristic) {
        gattUUIDWrite = uuidCharacteristic;
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(gattUUIDWrite);

        if(descriptor!=null) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            bluetoothGatt.writeDescriptor(descriptor);
        }
    }

    public boolean sendMessage(String message) {

        byte[] mensajeBytes = StringUtil.bytesFromString(message);
        if (mensajeBytes.length == 0) {
            Log.e(TAG, "No se puede convertir el String en un array de bytes");
            return false;
        }

        if (bluetoothGatt == null ) {
            Log.e(TAG,"No se encuentra en servicio.");
            toastError();
            return false;
        }

        BluetoothGattService gattService = bluetoothGatt.getService(gattUUIDService);
        if (gattService == null) {
            Log.e(TAG,"No se encuentra en servicio.");
            toastError();
            return false;
        }
        BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(gattUUIDWrite);

        if (characteristic == null) {
            toastError();
            bluetoothGatt.disconnect();
            return false;
        }

        characteristic.setValue(mensajeBytes);

        return bluetoothGatt.writeCharacteristic(characteristic);
    }

    public void startCommunication(Activity activity, boolean autoConnect, BluetoothDevice device) {
        device.connectGatt(activity, autoConnect, new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);

                switch (newState) {
                    case BluetoothProfile.STATE_CONNECTED:
                        Log.e(TAG + "::connectGatt", "STATE_CONNECTED");
                        gatt.discoverServices();
                        break;
                    case BluetoothProfile.STATE_DISCONNECTED:
                        Log.e(TAG + "::connectGatt", "STATE_DISCONNECTED");
                        toastError();
                        break;
                    default:
                        Log.e(TAG + "::connectGatt", "STATE_OTHER");
                }

                Log.e(TAG + "::connectGatt", "status: " + status + " - " + gatt.getDevice().getName() + " - " + gatt.getDevice().getAddress());
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                super.onServicesDiscovered(gatt, status);
                Log.e(TAG, "onServicesDiscovered: " + status);
                if (status != BluetoothGatt.GATT_SUCCESS) {
                    return;
                }

                bluetoothGatt = gatt;

                gattDtos.clear();
                GattDto gattDto = null;
                Log.e(TAG, "Inicio - Estas son los Servicios y Caracteristicas");
                for (BluetoothGattService service : bluetoothGatt.getServices()) {
                    Log.e(TAG + "::Service", service.getUuid().toString() +
                            " type: " + service.getType() +
                            " instance: " + service.getInstanceId());
                    gattDto = new GattDto();
                    gattDto.setService(service.getUuid());

                    List<UUID> uuids = new ArrayList<>();
                    for (BluetoothGattCharacteristic characteristic: service.getCharacteristics()) {
                        Log.e(TAG + "::Characteristic", characteristic.getUuid().toString()
                                + " permission: " + characteristic.getPermissions() + " type: " + service.getType()
                                + " property: " + characteristic.getProperties() + " instance: " + characteristic.getInstanceId());
                        uuids.add(characteristic.getUuid());
                    }

                    gattDto.setUuids(uuids);
                    gattDtos.add(gattDto);
                }
                Log.e(TAG, "Fin - Servicios y caracteristicas");

                if (! gattDtos.isEmpty()) {
                    int serviceSize = gattDtos.size() - 1;
                    int characteristicSize = gattDtos.get(serviceSize).getUuids().size() - 1;

                    UUID service = gattDtos.get(serviceSize).getService();
                    UUID characteristicRead = gattDtos.get(serviceSize).getUuids().get(0);
                    UUID characteristicWrite = gattDtos.get(serviceSize)
                                    .getUuids().get(characteristicSize);

                    Log.e(TAG, "Permiso de Servicio: " + service.toString());
                    Log.e(TAG, "Permiso de Lectura: " + characteristicRead.toString());
                    Log.e(TAG, "Permiso de Escritura: " + characteristicWrite.toString());

                    setUUIDService(service, characteristicRead);
                    setCharacteristicUUIDRead(characteristicWrite);

                    gatt.setCharacteristicNotification(characteristic, true);
                } else {
                    setService(SERVICE_STRING, CHARACTERISTIC_READ_STRING);
                    setCharacteristicRead(CHARACTERISTIC_WRITE_STRING);
                }

            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                String mensaje = characteristic.getStringValue(0);
                if (communicationGatt != null) {
                    communicationGatt.onMessageRead(mensaje);

                }
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                super.onCharacteristicRead(gatt, characteristic, status);

                Log.e("MSG READ", "Inicio");
                Log.e("MSG READ", characteristic.getStringValue(0));
                Log.e("MSG READ", "Fin");
            }

            private String readCharacteristic(BluetoothGattCharacteristic characteristic) {
//                byte[] messageBytes = characteristic.getValue();
//                String message = StringUtil.stringFromBytes(messageBytes);
                String message =characteristic.getStringValue(0);
                return message;
            }
        });
    }

    public void connect() {
        if (bluetoothGatt != null) {
            bluetoothGatt.connect();
        }
    }

    public void disconnect() {
        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
        }
    }

    private void toastError() {
        Toast.makeText(context, "Se perdio la conexion", Toast.LENGTH_LONG).show();
    }
}
