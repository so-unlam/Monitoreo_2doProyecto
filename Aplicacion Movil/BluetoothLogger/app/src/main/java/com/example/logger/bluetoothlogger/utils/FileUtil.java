package com.example.logger.bluetoothlogger.utils;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FileUtil {
    private static String TAG = FileUtil.class.getSimpleName();
    private File path;
    private File file;
    private FileOutputStream fileOutputStream;
    private OutputStreamWriter outputStreamWriter;

    public FileUtil() {
    }

    public boolean createPath(String directory) {
        path = Environment.getExternalStoragePublicDirectory(directory);

        //si no existe la carpeta la creamos.
        if(! path.exists()) {
            Log.e(TAG, "Se pudo crear directorio: " + path.mkdirs());
        }

        Date date = new Date();

        DateFormat format = new SimpleDateFormat("yyyyMMddddHHmm", Locale.US);


        file = new File(path, "AbuCaida_" + format.format(date) + ".txt");
        try {
            file.createNewFile();
            fileOutputStream = new FileOutputStream(file);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream);

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void escribir(String data) {
        try {

            outputStreamWriter.append(data);

        } catch (IOException e) {
            Log.e(TAG, "File write failed: " + e.toString(), e);
        }
    }

    public boolean cerrar() {
        try {

            if(outputStreamWriter==null)
                return false;

            outputStreamWriter.close();
            //limpiamos el buffer antes de liberar el puntero
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;

        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
    }
}
