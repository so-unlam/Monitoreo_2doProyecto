package com.example.logger.bluetoothlogger.utils;

import com.jjoe64.graphview.series.DataPoint;

public class GraphicUtil {
    private static String TAG = GraphicUtil.class.getSimpleName();

    public GraphicUtil() {
    }

    public void armarGrafico() {

    }

    public void actualizarGrafico() {

    }

    public void cargarGrafico() {

    }


    public static class Node {
        private String type;
        private DataPoint time;
        private DataPoint temp;
        private DataPoint accelerometerX;
        private DataPoint accelerometerY;
        private DataPoint accelerometerZ;
        private DataPoint gyroscopeX;
        private DataPoint gyroscopeY;
        private DataPoint gyroscopeZ;
        private DataPoint pitch;
        private DataPoint roll;
        private DataPoint angule;
        private DataPoint normalAcel;
        private DataPoint normalGyro;


        public Node(String data[]) {
            this.type = data[0];
            Double time = Double.parseDouble(data[1]);
            this.time = new DataPoint(time, Double.parseDouble(data[1]));
            this.temp = new DataPoint(time, Double.parseDouble(data[2]));
            this.accelerometerX = new DataPoint(time, Double.parseDouble(data[3]));
            this.accelerometerY = new DataPoint(time, Double.parseDouble(data[4]));
            this.accelerometerZ = new DataPoint(time, Double.parseDouble(data[5]));
            this.gyroscopeX = new DataPoint(time, Double.parseDouble(data[6]));
            this.gyroscopeY = new DataPoint(time, Double.parseDouble(data[7]));
            this.gyroscopeZ = new DataPoint(time, Double.parseDouble(data[8]));
            this.pitch = new DataPoint(time, Double.parseDouble(data[9]));
            this.roll = new DataPoint(time, Double.parseDouble(data[10]));
            this.angule = new DataPoint(time, Double.parseDouble(data[11]));
            this.normalAcel = new DataPoint(time, Double.parseDouble(data[12]));
            this.normalGyro = new DataPoint(time, Double.parseDouble(data[13].replaceAll("(\\\\r\\\\n|\\\\n)", "").trim()));
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public DataPoint getTime() {
            return time;
        }

        public void setTime(DataPoint time) {
            this.time = time;
        }

        public DataPoint getTemp() {
            return temp;
        }

        public void setTemp(DataPoint temp) {
            this.temp = temp;
        }

        public DataPoint getAccelerometerX() {
            return accelerometerX;
        }

        public void setAccelerometerX(DataPoint accelerometerX) {
            this.accelerometerX = accelerometerX;
        }

        public DataPoint getAccelerometerY() {
            return accelerometerY;
        }

        public void setAccelerometerY(DataPoint accelerometerY) {
            this.accelerometerY = accelerometerY;
        }

        public DataPoint getAccelerometerZ() {
            return accelerometerZ;
        }

        public void setAccelerometerZ(DataPoint accelerometerZ) {
            this.accelerometerZ = accelerometerZ;
        }

        public DataPoint getGyroscopeX() {
            return gyroscopeX;
        }

        public void setGyroscopeX(DataPoint gyroscopeX) {
            this.gyroscopeX = gyroscopeX;
        }

        public DataPoint getGyroscopeY() {
            return gyroscopeY;
        }

        public void setGyroscopeY(DataPoint gyroscopeY) {
            this.gyroscopeY = gyroscopeY;
        }

        public DataPoint getGyroscopeZ() {
            return gyroscopeZ;
        }

        public void setGyroscopeZ(DataPoint gyroscopeZ) {
            this.gyroscopeZ = gyroscopeZ;
        }

        public DataPoint getPitch() {
            return pitch;
        }

        public void setPitch(DataPoint pitch) {
            this.pitch = pitch;
        }

        public DataPoint getRoll() {
            return roll;
        }

        public void setRoll(DataPoint roll) {
            this.roll = roll;
        }

        public DataPoint getAngule() {
            return angule;
        }

        public void setAngule(DataPoint angule) {
            this.angule = angule;
        }

        public DataPoint getNormalAcel() {
            return normalAcel;
        }

        public void setNormalAcel(DataPoint normalAcel) {
            this.normalAcel = normalAcel;
        }

        public DataPoint getNormalGyro() {
            return normalGyro;
        }

        public void setNormalGyro(DataPoint normalGyro) {
            this.normalGyro = normalGyro;
        }
    }
}
