package com.example.logger.bluetoothlogger.communications;

public interface ICommunicationGatt {
    public void onMessageRead(String message);
}
