package com.example.logger.bluetoothlogger.fragments;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.logger.bluetoothlogger.R;
import com.example.logger.bluetoothlogger.communications.ICommunicationGatt;
import com.example.logger.bluetoothlogger.communications.ICommunicationState;
import com.example.logger.bluetoothlogger.utils.BLEUtil;
import com.example.logger.bluetoothlogger.utils.GraphicUtil;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

public class GraphFragment extends Fragment implements ICommunicationGatt, ICommunicationState {
    public static String TAG = GraphFragment.class.getSimpleName();
    private GraphView graphView;
    private BluetoothDevice device;
    private BLEUtil bleUtil;
    private LineGraphSeries<DataPoint> accelerometerSeries;
    private String bufferString = "";
    private Double axisX;

    public GraphFragment() {
        bufferString = "";
        bleUtil = new BLEUtil();
        accelerometerSeries = new LineGraphSeries<DataPoint>(new DataPoint[] {
                    new DataPoint(0, 0)
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistical_graph, container, false);
    }

    public void setDevice(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        graphView = getActivity().findViewById(R.id.fragment_graph);
        graphView.addSeries(accelerometerSeries);
        graphView.getViewport().setMinX(0);
        graphView.getViewport().setMaxX(100);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setScalable(true);

        startCommunication(getActivity(),false, device);

    }

    @Override
    public void onMessageRead(String message) {

        //Armar nodo para graficar
        Log.e(TAG, "Llego mensaje: " + message);

        bufferString = bufferString + message;

        if (bufferString.contains("\r\n")) {
            String nuevoPaquete[] = bufferString.split("\r\n");
            String datos[] = nuevoPaquete[0].split(",");

            GraphicUtil.Node node = new GraphicUtil.Node(datos);

            accelerometerSeries.appendData(node.getAccelerometerX(), true, 2000);

            if (nuevoPaquete.length > 1) {
                bufferString = nuevoPaquete[1];
            } else {
                bufferString = "";
            }
        }

    }



    @Override
    public void startCommunication(Activity activity, boolean autoConnect, BluetoothDevice device) {
        bleUtil.setOnListener(this);
        bleUtil.startCommunication(activity, autoConnect, device);
    }

    @Override
    public void connect() {
        bleUtil.startCommunication(getActivity(), false, device);
    }

    @Override
    public void disconnect() {
        bleUtil.disconnect();
    }
}
