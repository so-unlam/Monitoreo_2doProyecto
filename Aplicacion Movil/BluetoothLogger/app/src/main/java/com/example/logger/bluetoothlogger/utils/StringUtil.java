package com.example.logger.bluetoothlogger.utils;

import android.support.annotation.Nullable;
import android.util.Log;

import java.io.UnsupportedEncodingException;

public class StringUtil {
    private static final String TAG = StringUtil.class.getSimpleName();

    public static byte[] bytesFromString(String string) {
        byte[] stringBytes = new byte[0];
        try {
            stringBytes = string.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Error al convertir el array en UTF-8");
        }

        return stringBytes;
    }

    @Nullable
    public static String stringFromBytes(byte[] bytes) {
        String byteString = null;
        try {
            byteString = new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "No se puede convertir el array de bytes en String");
        }
        return byteString;
    }
}
