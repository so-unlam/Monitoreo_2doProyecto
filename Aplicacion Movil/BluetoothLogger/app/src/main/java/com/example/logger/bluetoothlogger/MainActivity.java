package com.example.logger.bluetoothlogger;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.logger.bluetoothlogger.communications.ICommunicationState;
import com.example.logger.bluetoothlogger.components.PopupComponent;
import com.example.logger.bluetoothlogger.fragments.ClientGattFragment;
import com.example.logger.bluetoothlogger.fragments.DeviceFinderFragment;
import com.example.logger.bluetoothlogger.fragments.GraphFragment;
import com.example.logger.bluetoothlogger.fragments.LoggerFragment;
import com.example.logger.bluetoothlogger.utils.BluetoothUtil;
import com.example.logger.bluetoothlogger.communications.ICommunicationFragment;

import static com.example.logger.bluetoothlogger.utils.BluetoothUtil.REQUEST_ENABLE_BT;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ICommunicationFragment {

    private static String TAG = MainActivity.class.getSimpleName();

    private LoggerFragment loggerFragment;
    private DeviceFinderFragment deviceFinderFragment;
    private ClientGattFragment clientGattFragment;
    private GraphFragment graphFragment;
    private ICommunicationState fragmentState;

    private BluetoothUtil bluetoothUtil;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice deviceSelected;

    private static final int REQUEST_CODE = 10;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        deviceSelected = null;
        bluetoothUtil = new BluetoothUtil(this);

        if (bluetoothAdapter == null) {
            bluetoothAdapter = bluetoothUtil.habilitarBluetooth();

            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_FOUND);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);

            registerReceiver(bluetoothUtil.getBluetoothReceiver(), filter);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        bluetoothUtil.apagarBluetooth();
        unregisterReceiver(bluetoothUtil.getBluetoothReceiver());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            menu.findItem(R.id.action_connect_bluetooth).setVisible(false);
            menu.findItem(R.id.action_disconnect_bluetooth).setVisible(false);
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_turn_on_bluetooth) {
            bluetoothUtil.prenderBluetooth();
            return true;
        } else if (id == R.id.action_turn_off_bluetooth) {
            bluetoothUtil.apagarBluetooth();
        } else if (id == R.id.action_connect_bluetooth) {
            fragmentState.connect();
        } else if (id == R.id.action_disconnect_bluetooth) {
            fragmentState.disconnect();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        boolean resultado = true;

        Fragment fragment = null;
        String tag = "";

        switch (id) {
            case R.id.nav_fragment_graph:

                if (deviceValidate(deviceSelected)) {
                    graphFragment = new GraphFragment();
                    graphFragment.setDevice(deviceSelected);
                    fragment = graphFragment;
                    tag = GraphFragment.TAG;

                    fragmentState = graphFragment;
                } else {
                    resultado = false;
                }

                break;
            case R.id.nav_fragment_client:

                if (deviceValidate(deviceSelected)) {
                    clientGattFragment = new ClientGattFragment();
                    clientGattFragment.setDevice(deviceSelected);
                    fragment = clientGattFragment;
                    tag = ClientGattFragment.TAG;

                    fragmentState = clientGattFragment;
                } else {
                    resultado = false;
                }

                break;
            case R.id.nav_fragment_logger:

                menu.findItem(R.id.action_connect_bluetooth).setVisible(true);
                menu.findItem(R.id.action_disconnect_bluetooth).setVisible(true);

                if (deviceValidate(deviceSelected)) {
                    loggerFragment = new LoggerFragment();
                    loggerFragment.setCommunication(this);
                    loggerFragment.setDevicetConnection(deviceSelected);

                    fragment = loggerFragment;
                    tag = LoggerFragment.TAG;

                    fragmentState = loggerFragment;
                } else {
                    resultado = false;
                }
                break;
            case R.id.nav_fragment_finder:

                deviceFinderFragment = new DeviceFinderFragment();
                deviceFinderFragment.setCommunication(this);
                deviceFinderFragment.setUtil(bluetoothUtil);

                fragment = deviceFinderFragment;
                tag = DeviceFinderFragment.TAG;

                break;
            case R.id.nav_buscar_bluetooth:

                if (bluetoothAdapter.isEnabled()) {
                    bluetoothUtil.findDevices();
                } else {
                    PopupComponent dialog = new PopupComponent();
                    dialog.setBluetooth(bluetoothUtil);
                    dialog.setTitulo("Para comenzar la busqueda debe prender su bluetooth. Desea prenderlo ahora?");
                    dialog.show(getSupportFragmentManager(), "Advertencia");
                }

                resultado = false;

                break;
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .addToBackStack(tag)
                    .replace(R.id.contain_main, fragment, tag)
                    .commit();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return resultado;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "OPCIONES " + requestCode + " " + requestCode);

        if (REQUEST_ENABLE_BT == requestCode && bluetoothAdapter.isEnabled()){
            bluetoothUtil.findDevices();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                }
            }
        }
    }

    @Override
    public void deviceBluetoothSeleccionado(BluetoothDevice device) {

        menu.findItem(R.id.action_connect_bluetooth).setVisible(true);
        menu.findItem(R.id.action_disconnect_bluetooth).setVisible(true);

        this.deviceSelected = device;

        clientGattFragment = new ClientGattFragment();
        clientGattFragment.setDevice(device);

        getSupportFragmentManager().beginTransaction()
                .addToBackStack(LoggerFragment.TAG)
                .add(R.id.contain_main, clientGattFragment, ClientGattFragment.TAG)
                .commit();

        fragmentState = clientGattFragment;
    }

    public boolean deviceValidate(BluetoothDevice device) {
        if (device != null) {
            return true;
        } else {
            PopupComponent dialog = new PopupComponent();
            dialog.setBluetooth(bluetoothUtil);
            dialog.setTitulo("El dispositivo no se encuentra conectado a ningun dispositivo bluetooth servidor.");
            dialog.show(getSupportFragmentManager(), "Advertencia");
            return false;
        }
    }
}
