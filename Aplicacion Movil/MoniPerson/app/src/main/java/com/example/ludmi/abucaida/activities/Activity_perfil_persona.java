package com.example.ludmi.abucaida.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.fragments.Frg_list_contactos_persona;
import com.example.ludmi.abucaida.model.Persona;
import com.example.ludmi.abucaida.model.Contacto;
import com.example.ludmi.abucaida.notificaciones.Utilidades;
import com.example.ludmi.abucaida.redes.Registrar_persona;

import java.util.List;

import static com.example.ludmi.abucaida.activities.Activity_Main.activity_main;
import static com.example.ludmi.abucaida.notificaciones.Utilidades.mostrarDialogoAlerta;

public class Activity_perfil_persona extends AppCompatActivity implements  Frg_list_contactos_persona.OnFragmentInteractionListener {

    private static String TAG = Activity_perfil_persona.class.getName();

    private TextInputEditText editTokenSe;
    private TextInputEditText editNombre;
    private TextInputEditText editFechaNac;
    private TextInputEditText editTelefono;
    private TextInputEditText editDireccion;
    private Button btn_mapa;

    private int usuario_id;
    private String usuario_token;
    private Persona abuelo_actual;
    private String mensaje_id;
    private Boolean esCaida;
    private Menu currentMenu;

    private List<Contacto> filasContactos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_abuelo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTokenSe = (TextInputEditText) findViewById(R.id.activity_perfil_abuelo_edit_token);
        editNombre = (TextInputEditText) findViewById(R.id.activity_perfil_abuelo_edit_nombre);
        editFechaNac = (TextInputEditText) findViewById(R.id.activity_perfil_abuelo_edit_fecha);
        editTelefono = (TextInputEditText) findViewById(R.id.activity_perfil_abuelo_edit_tel);
        editDireccion = (TextInputEditText) findViewById(R.id.activity_perfil_abuelo_edit_direccion);
        btn_mapa = (Button) findViewById(R.id.activity_perfil_abuelo_ver_mapa);
        //cargando informacion del item seleccionado
        abuelo_actual = (Persona) getIntent().getSerializableExtra("abu");
        mensaje_id = (String) getIntent().getSerializableExtra("msj_id");

        usuario_id = abuelo_actual.getUsuario_id();
        usuario_token = abuelo_actual.getPersona_token_se();
        editTokenSe.setText(abuelo_actual.getPersona_token_se());
        editNombre.setText(abuelo_actual.getPersona_nombre());
        editFechaNac.setText(abuelo_actual.getPersona_fecha_nac());
        editTelefono.setText(abuelo_actual.getPersona_telefono());
        editDireccion.setText(abuelo_actual.getPersona_direccion());

        editTokenSe.setOnClickListener(listenerEditText);
        btn_mapa.setOnClickListener(onViewMap);
        onCreateButtonMap();
    }

    public void onCreateButtonMap(){
        LinearLayout.LayoutParams  linearButton=(LinearLayout.LayoutParams)btn_mapa.getLayoutParams();

        if(!mensaje_id.equals(Utilidades.CAIDA_DETECTADA)){
            btn_mapa.setText("Ver Mapa de Seguimiento");
            linearButton.gravity = Gravity.RIGHT;
        }else{
            btn_mapa.setText("Ver Ubicación caída");
            linearButton.gravity = Gravity.LEFT;
        }
    }
    private  void inhabilitarComponentes(){

        editNombre.setFocusable(false);
        editFechaNac.setFocusable(false);
        editTelefono.setFocusable(false);
        editDireccion.setFocusable(false);

        editNombre.setOnClickListener(listenerEditText);
        editFechaNac.setOnClickListener(listenerEditText);
        editTelefono.setOnClickListener(listenerEditText);
        editDireccion.setOnClickListener(listenerEditText);

        currentMenu.findItem(R.id.action_agregar_contacto).setVisible(false);
    }

    public void onBackPressed() {
        if (checkearCambios())
            mostrarDialogo();
        else finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.abu_caida_main, menu);
        currentMenu=menu;

        if(!mensaje_id.equals(Utilidades.ACTUALIZACION_COORDENADAS))
            inhabilitarComponentes();

        return true;
    }

    public View.OnClickListener onViewMap = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.activity_perfil_abuelo_ver_mapa:
                        Intent intent = new Intent(Activity_perfil_persona.this, Activity_Mapa.class);
                        intent.putExtra("abu", abuelo_actual);
                        intent.putExtra("msj_id", mensaje_id);
                        startActivity(intent);
                        finish();
                    break;
            }
        }
    };
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        Intent intent = new Intent(this, Activity_Contactos_Asignados.class);
        intent.putExtra("idAbuelo", usuario_token);
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (checkearCambios()) mostrarDialogo();
                else finish();
                break;
            case R.id.action_contactos:
                intent.putExtra("msj_id", mensaje_id);
                startActivity(intent);
                Log.e(TAG, "listar contactos");
                break;
            case R.id.action_agregar_contacto:

                intent.putExtra("msj_id", mensaje_id);
                intent.putExtra("isAgregado", true);
                startActivity(intent);
                Log.e(TAG, "listar contactos");
                break;
        }

        return true;
    }



    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void mostrarDialogo() {
        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:

                        new Registrar_persona(activity_main).ejecutarActualizarAbuelo(
                                usuario_id + "",
                                editNombre.getText() + "",
                                editDireccion.getText() + "",
                                editFechaNac.getText() + "",
                                editTelefono.getText() + "",
                                editTokenSe.getText() + "");

                        finish();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        mostrarDialogoAlerta(this, "Confirmar", "¿Desea guardar los cambios realizados?", dialogClickListener);
    }

    /***
     * devolvemos true si hubo algun cambio, caso contrario false
     * @return
     */
    public boolean checkearCambios() {
        if (!abuelo_actual.getPersona_token_se().equals(editTokenSe.getText() + "")) {
            abuelo_actual.setPersona_token_se(editTokenSe.getText() + "");
            return true;
        }

        if (!abuelo_actual.getPersona_nombre().equals(editNombre.getText() + "")) return true;

        if (!abuelo_actual.getPersona_fecha_nac().equals(editFechaNac.getText() + "")) return true;

        if (!abuelo_actual.getPersona_telefono().equals(editTelefono.getText() + "")) return true;

        if (!abuelo_actual.getPersona_direccion().equals(editDireccion.getText() + "")) return true;

        return false;
    }

    private View.OnClickListener listenerEditText = new View.OnClickListener() {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "Este campo no se podra editar", Toast.LENGTH_SHORT).show();
        }
    };
}
