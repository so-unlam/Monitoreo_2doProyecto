package com.example.ludmi.abucaida.redes;

import com.example.ludmi.abucaida.model.Usuario;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Respuesta_Eliminar implements Serializable{

    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
