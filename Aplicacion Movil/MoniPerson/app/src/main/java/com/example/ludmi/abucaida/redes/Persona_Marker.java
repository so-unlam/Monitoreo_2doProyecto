package com.example.ludmi.abucaida.redes;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Persona_Marker {
    public static Integer LAST_POSITIONS = 1;
    public static Integer LAST_FALL_DOWN = 2;

    @FormUrlEncoded
    @POST("/laravel/mobile/watch_se")
    public Call<Respuesta_Marker> getPosition(@Field("token") String token,
                            @Field("mensaje_id") String mensaje_id);

    @GET("/maps/api/directions/json")
    public Call<Respuesta_Direction> getWayPoints(@Query("origin") String origin,
                             @Query("destination") String destination,
                             @Query("key") String key);
}
