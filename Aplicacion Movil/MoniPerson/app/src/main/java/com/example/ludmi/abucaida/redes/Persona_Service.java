package com.example.ludmi.abucaida.redes;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by raulvillca on 28/10/17.
 */

public interface Persona_Service {

    @GET("/laravel/mobile/list_all")
    public Call<Respuesta_Abuelos> listarAbuelos(@Query("id") String idUsuario);

    @GET("/laravel/mobile/verify_token")
    public Call<Respuesta_Abuelos> verificarExitenciaToken(@Query("token") String token_se);

}
