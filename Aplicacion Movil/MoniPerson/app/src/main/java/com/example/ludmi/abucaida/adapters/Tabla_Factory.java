package com.example.ludmi.abucaida.adapters;

import android.support.v4.app.Fragment;

import com.example.ludmi.abucaida.fragments.Frg_login;
import com.example.ludmi.abucaida.fragments.Frg_registrar_usuario;

/**
 * Created by raulvillca on 9/11/17.
 */

public class Tabla_Factory {
    public static Fragment getInstance(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Frg_login();
                break;
            case 1:
                fragment = new Frg_registrar_usuario();
                break;
        }

        return fragment;
    }

    public static String getInstanceTitle(int position) {
        String name = "";
        switch (position) {
            case 0:
                name = "Iniciar Sesión";
                break;
            case 1:
                name = "Registrar";
                break;
        }

        return name;
    }
}
