package com.example.ludmi.abucaida.redes;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.example.ludmi.abucaida.activities.Activity_perfil_persona;
import com.example.ludmi.abucaida.fragments.Callback_Fragmento;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Registrar_Marker {
    private static String TAG = Registrar_Marker.class.getName();
    private Callback_Fragmento<Respuesta_Marker> callback;
    private Retrofit adapter;
    private Activity activity;

    public Registrar_Marker(Activity activity, Retrofit adapter) {
        this.callback = (Callback_Fragmento<Respuesta_Marker>) activity;
        this.adapter = adapter;
        this.activity=activity;
    }

    public void getMarkers(String token, String option, String originPosition, String server, String key) {
        Persona_Marker service = adapter.create(Persona_Marker.class);

        Call<Respuesta_Marker> response = service.getPosition(token, option);
        response.enqueue(new Callback<Respuesta_Marker>() {
            @Override
            public void onResponse(Call<Respuesta_Marker> call, Response<Respuesta_Marker> response) {
                Log.e(TAG, response.body() + "");

                if(response.body().isSuccess()) {
                    if(response.body().getEvents().size()!=0) {
                        //Pasamos por parametros latitud y longitud de origen, destino, el tipo de transporte y clave de google
                        getWayPoints(
                                originPosition,
                                response.body().getEvents().get(0).getSuceso_gps_latitud() + "," +
                                        response.body().getEvents().get(0).getSuceso_gps_longitud(),
                                server, key,
                                response.body());
                    }else{
                        Toast.makeText(activity,"No hay coordenadas de ubicación registradas para esta Persona",Toast.LENGTH_LONG).show();
                    }
                }
            }


            @Override
            public void onFailure(Call<Respuesta_Marker> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
        /*
        service.getPosition(token, option, new Callback<Respuesta_Marker>() {
            @Override
            public void success(Respuesta_Marker respuesta_marker, Response response) {


            }

            @Override
            public void failure(RetrofitError error) {

            }
        });*/
    }

    public void getWayPoints(String origin, String destination, String server, String key, Respuesta_Marker marker) {
        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(server)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Persona_Marker service = restAdapter.create(Persona_Marker.class);

        Call<Respuesta_Direction> response = service.getWayPoints(origin, destination, key);
        response.enqueue(new Callback<Respuesta_Direction>() {
            @Override
            public void onResponse(Call<Respuesta_Direction> call, Response<Respuesta_Direction> response) {
                if (! response.body().routes.isEmpty()) {
                    marker.setWayPoints(response.body().getRoutes().get(0).getOverview_polyline().getPoints());
                }
                callback.mensajeFragmento(marker);
            }

            @Override
            public void onFailure(Call<Respuesta_Direction> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
                callback.mensajeFragmento(marker);
            }
        });
    }
}

