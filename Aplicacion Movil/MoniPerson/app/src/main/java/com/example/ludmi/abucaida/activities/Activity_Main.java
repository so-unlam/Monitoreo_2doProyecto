package com.example.ludmi.abucaida.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;
import com.example.ludmi.abucaida.fragments.Callback_Fragmento;
import com.example.ludmi.abucaida.fragments.Frg_list_persona;
import com.example.ludmi.abucaida.fragments.Frg_perfil_usuario;
import com.example.ludmi.abucaida.fragments.Frg_registrar_persona;
import com.example.ludmi.abucaida.redes.Respuesta_Abuelos;

import java.util.Timer;
import java.util.TimerTask;


public class Activity_Main extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Callback_Fragmento<Respuesta_Abuelos> {

    private int clickCounter = 0;

    public static Activity activity_main;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.acivity_main_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Frg_list_persona frgListAbuelos = new Frg_list_persona();
        FragmentManager fragementManager = getSupportFragmentManager();
        fragementManager.beginTransaction()
                .replace(R.id.contenedor, frgListAbuelos, Frg_list_persona.TAG)
                .commit();

        activity_main=this;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager fragementManager = getSupportFragmentManager();
        if (id == R.id.menuMiPerfil) {
            Frg_perfil_usuario frgPerfilUsuario = (Frg_perfil_usuario) getSupportFragmentManager()
                    .findFragmentByTag(Frg_perfil_usuario.TAG);

            //ocultamos el fragmento de lista de abuelos para que no se escuche ningun evento.
            getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager()
                    .findFragmentByTag(Frg_list_persona.TAG))
                    .commit();

            if ( frgPerfilUsuario == null ) {
                fragementManager.beginTransaction()
                        .addToBackStack(null)
                        .add(R.id.contenedor,new Frg_perfil_usuario(), Frg_perfil_usuario.TAG)
                        .commit();
            } else if (frgPerfilUsuario != null && ! frgPerfilUsuario.isVisible()) {
                fragementManager.beginTransaction()
                        .addToBackStack(null)
                        .add(R.id.contenedor,new Frg_perfil_usuario(), Frg_perfil_usuario.TAG)
                        .commit();
            } else
                Log.e("Activity_Main", "No instanciar fragment");

        } else if (id == R.id.menuCerrarSesion) {
            startActivity(new Intent(this, Activity_Login.class));
            Bd_Sesion_Usuario.deslogear(this);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /****
     * Sobre escribimos onBackPressed para no cerrar la app, cuando tocamos hacia atras..
     * onBackPressed es escuchando por el menu lateral (DrawerLayour)
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Frg_list_persona frgListAbuelos =  (Frg_list_persona) getSupportFragmentManager()
                .findFragmentByTag(Frg_list_persona.TAG);

        getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager()
                .findFragmentByTag(Frg_list_persona.TAG))
                .commit();

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else if (frgListAbuelos != null && frgListAbuelos.isVisible()) {
            //incrementamos +1 a clickCounter, si en 1,5seg no volvimos a tocar atras
            //entonces clickCounter vuelve a 0
            clickCounter++;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    SystemClock.sleep(1500);
                    clickCounter = 0;
                }
            }, 100);

            if (clickCounter == 2) {
                super.onBackPressed();
                finish();
            } else {
                Toast.makeText(this, getString(R.string.message_exit), Toast.LENGTH_LONG).show();
            }
        }

        cerrarFragmentos();
    }

    public void cerrarFragmentos() {

        Frg_perfil_usuario frgPerfilUsuario = (Frg_perfil_usuario) getSupportFragmentManager()
                .findFragmentByTag(Frg_perfil_usuario.TAG);

        if (frgPerfilUsuario != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(getSupportFragmentManager().findFragmentByTag(Frg_perfil_usuario.TAG))
                    .commit();
        }

        Frg_registrar_persona frgRegistrarAbuelo = (Frg_registrar_persona) getSupportFragmentManager()
                .findFragmentByTag(Frg_registrar_persona.TAG);

        if (frgRegistrarAbuelo != null) {
            getSupportFragmentManager().beginTransaction()
                    .remove(getSupportFragmentManager().findFragmentByTag(Frg_registrar_persona.TAG))
                    .commit();
        }
    }

    @Override
    public void mensajeFragmento(Respuesta_Abuelos respuesta) {

        Frg_list_persona frgListAbuelos =  (Frg_list_persona) getSupportFragmentManager()
                .findFragmentByTag(Frg_list_persona.TAG);

        if (frgListAbuelos != null) {
            getSupportFragmentManager().beginTransaction().show(getSupportFragmentManager()
                    .findFragmentByTag(Frg_list_persona.TAG))
                    .commit();

            //enviamos la respuesta de la consulta
            frgListAbuelos.consultarAbuelos(respuesta.getPersonas());
        }

        cerrarFragmentos();
    }
}

