package com.example.ludmi.abucaida.redes;

import com.example.ludmi.abucaida.model.Usuario;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Respuesta_Registrar implements Serializable{

    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    private Usuario usuario;

    public Usuario getUsuario() {  return usuario;    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
