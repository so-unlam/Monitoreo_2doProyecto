package com.example.ludmi.abucaida.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.notificaciones.Utilidades;


public class Frg_list_contactos_persona_bottom extends BottomSheetDialogFragment {
    private TextView txtDesasignar;
    private TextView txtNotificarContacto;
    private String nombreContacto;
    private String contactoId;
    private String mensaje_id;

    @Override
    public void setupDialog(Dialog dialog, int style) {

        View contentView = View.inflate(getContext(), R.layout.fragment_list_contactos_abuelo_bottom, null);
        dialog.setContentView(contentView);

        BottomSheetBehavior<View> mBottomSheetBehavior = BottomSheetBehavior.from(((View) contentView.getParent()));

        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetBehaviorCallback);
            mBottomSheetBehavior.setPeekHeight(1200);
        }

        txtDesasignar=(TextView)contentView.findViewById(R.id.fragment_list_contactos_abuelo_bottom_txt_desasignar);
        txtNotificarContacto=(TextView)contentView.findViewById(R.id.fragment_list_contactos_abuelo_bottom_txt_notificar_contacto);

        txtDesasignar.setOnClickListener(listenerTxtDesasignar);

        txtNotificarContacto.setOnClickListener(listenerTxtNotificarContacto);
        nombreContacto= getArguments().getString("nombreContacto");
        contactoId=getArguments().getString("contactoId");
        mensaje_id=getArguments().getString("msj_id");

        if(mensaje_id.equals(Utilidades.ACTUALIZACION_COORDENADAS))
            inhabilitarControles();

    }



    private final BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new
            BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(View bottomSheet, int newState) {
                    String state = null;

                    switch (newState) {
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            state = "STATE_COLLAPSED";
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            state = "STATE_DRAGGING";
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            state = "STATE_EXPANDED";
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            state = "STATE_SETTLING";
                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:
                            state = "STATE_HIDDEN";
                            //call ALWAYS dismiss to hide the modal background
                            dismiss();
                            break;
                    }

                    Log.d(Frg_list_contactos_persona_bottom.class.getSimpleName(), state);
                }

                @Override
                public void onSlide( View bottomSheet, float slideOffset) {
                    Log.d(Frg_list_contactos_persona_bottom.class.getSimpleName(), String.valueOf(slideOffset));
                }
            };



    private View.OnClickListener listenerTxtDesasignar = new View.OnClickListener() {
        public void onClick(View view) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, null);
            dismiss();
        }

    };



    private View.OnClickListener listenerTxtNotificarContacto = new View.OnClickListener() {
        public void onClick(View view) {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactoId));
            intent.setData(uri);
            startActivity(intent);

            dismiss();
        }

    };

    public void inhabilitarControles(){
        txtDesasignar.setVisibility(View.INVISIBLE);
    }
}

