package com.example.ludmi.abucaida.bd_android;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ludmi.abucaida.model.Contacto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Esteban on 27/10/2017.
 */

public class Bd_lista_contactos {

    private static String NOMBRE_PREFERENCES = "Bd_lista_contactos";
    /***
     * agregamos un contacto a la lista, y la guardamos en un archivo preferencial
     * @param c
     * @param contacto
     */
    public static void agregarContacto(Context c, Contacto contacto) {

        List<Contacto> contactos = getListaContactosPorId(c, contacto.getPersona_token_se());
        contactos.add(contacto);

        SharedPreferences preferences = c.getSharedPreferences(NOMBRE_PREFERENCES, c.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("contactos", new Gson().toJson(contactos));
        editor.commit();
    }

    /***
     * se guarda la lista de contactos por token de abuelo
     * @param c
     * @param id_lista
     * @param contactos
     */
    public static void agregarContactos(Context c, String id_lista, List<Contacto> contactos) {

        SharedPreferences preferences = c.getSharedPreferences(NOMBRE_PREFERENCES, c.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(id_lista, new Gson().toJson(contactos));
        editor.commit();
    }

    /***
     *
     * Obtenemos lista de contactos deacuerdo al token del abuelo
     * @param context
     * @param idAbu
     * @return
     */
    public static List<Contacto> getListaContactosPorId(Context context, String idAbu) {
        SharedPreferences preferences = context.getSharedPreferences(NOMBRE_PREFERENCES, context.MODE_PRIVATE);

        String jsonContactos = preferences.getString("lista_" + idAbu, null);
        if (jsonContactos == null)
            return new ArrayList<>();

        List<Contacto> contactos = new Gson().fromJson(jsonContactos, new TypeToken<List<Contacto>>(){}.getType());
        return contactos;
    }
}
