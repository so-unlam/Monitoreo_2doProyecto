package com.example.ludmi.abucaida.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.ludmi.abucaida.activities.Activity_perfil_persona;
import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.model.Persona;
import com.example.ludmi.abucaida.redes.Registrar_persona;


public class Frg_list_personas_bottom extends BottomSheetDialogFragment {
    private TextView txtVerPerfil;
    private TextView txtEliminar;

    private String nombreAbuelo;
    private Persona abu;

    @Override
    public void setupDialog(Dialog dialog, int style) {

        View contentView = View.inflate(getContext(), R.layout.fragment_list_abuelos_bottom, null);
        dialog.setContentView(contentView);

        BottomSheetBehavior<View> mBottomSheetBehavior = BottomSheetBehavior.from(((View) contentView
                .getParent()));
        if (mBottomSheetBehavior != null) {
            mBottomSheetBehavior.setBottomSheetCallback(mBottomSheetBehaviorCallback);
            mBottomSheetBehavior.setPeekHeight(1200);
        }

        txtVerPerfil=(TextView)contentView.findViewById(R.id.fragment_list_abuelos_bottom_txt_ver_perfil);
        txtEliminar=(TextView)contentView.findViewById(R.id.fragment_list_abuelos_bottom_txt_eliminar);

        txtVerPerfil.setOnClickListener(listenerTxtVerPerfil);
        txtEliminar.setOnClickListener(listenerTxtEliminar);

        nombreAbuelo= getArguments().getString("nombreAbuelo");

        abu = (Persona) getArguments().getSerializable("abu");

    }


    private final BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new
            BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(View bottomSheet, int newState) {
                    String state = null;

                    switch (newState) {
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            state = "STATE_COLLAPSED";
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                            state = "STATE_DRAGGING";
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            state = "STATE_EXPANDED";
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            state = "STATE_SETTLING";
                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:
                            state = "STATE_HIDDEN";
                            //call ALWAYS dismiss to hide the modal background
                            dismiss();
                            break;
                    }

                    Log.d(Frg_list_personas_bottom.class.getSimpleName(), state);
                }

                @Override
                public void onSlide( View bottomSheet, float slideOffset) {
                    Log.d(Frg_list_personas_bottom.class.getSimpleName(), String.valueOf(slideOffset));
                }
            };




    private View.OnClickListener listenerTxtVerPerfil = new View.OnClickListener() {
        public void onClick(View view) {
            Intent intent= new Intent(getActivity(), Activity_perfil_persona.class);
            intent.putExtra("abu", abu);
            intent.putExtra("msj_id","1");
            startActivity(intent);

            dismiss();
        }

    };

    private View.OnClickListener listenerTxtEliminar = new View.OnClickListener() {
        public void onClick(View view) {

            new Registrar_persona(getActivity()).eliminarAbuelo(abu.getPersona_token_se()+"", abu.getUsuario_id());

            dismiss();
        }

    };

}

