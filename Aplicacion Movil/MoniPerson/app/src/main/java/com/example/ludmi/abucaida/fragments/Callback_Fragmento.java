package com.example.ludmi.abucaida.fragments;

/**
 * Created by raulvillca on 11/11/17.
 */

public interface Callback_Fragmento <T> {
    void mensajeFragmento(T respuesta);
}
