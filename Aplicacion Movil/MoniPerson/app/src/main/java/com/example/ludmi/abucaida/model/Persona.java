package com.example.ludmi.abucaida.model;

import java.io.Serializable;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    private int usuario_id;
    private String persona_nombre;
    private String persona_direccion;
    private String persona_telefono;
    private String persona_fecha_nac;
    private String persona_token_se;
    private int persona_imagen;

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getPersona_nombre() {
        return persona_nombre;
    }

    public void setPersona_nombre(String persona_nombre) {
        this.persona_nombre = persona_nombre;
    }

    public String getPersona_direccion() {
        return persona_direccion;
    }

    public void setPersona_direccion(String persona_direccion) {
        this.persona_direccion = persona_direccion;
    }

    public String getPersona_telefono() {
        return persona_telefono;
    }

    public void setPersona_telefono(String persona_telefono) {
        this.persona_telefono = persona_telefono;
    }

    public String getPersona_fecha_nac() {
        return persona_fecha_nac;
    }

    public void setPersona_fecha_nac(String persona_fecha_nac) {
        this.persona_fecha_nac = persona_fecha_nac;
    }

    public String getPersona_token_se() {
        return persona_token_se;
    }

    public void setPersona_token_se(String persona_token_se) {
        this.persona_token_se = persona_token_se;
    }

    public int getPersona_imagen() {
        return persona_imagen;
    }

    public void setPersona_imagen(int persona_imagen) {
        this.persona_imagen = persona_imagen;
    }
}
