package com.example.ludmi.abucaida.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.notificaciones.Utilidades;
import com.example.ludmi.abucaida.redes.Registrar_Inicio;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Frg_registrar_usuario extends Fragment {
    public Frg_registrar_usuario() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registrar_usuario, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        Button button = (Button) getActivity().findViewById(R.id.fragment_registrar_btn_registrar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editNombre = (EditText) getActivity().findViewById(R.id.fragment_registrar_usuario_edit_nombre);
                EditText editDireccion = (EditText) getActivity().findViewById(R.id.fragment_registrar_usuario_edit_direccion);
                EditText editTelefono = (EditText) getActivity().findViewById(R.id.fragment_registrar_usuario_edit_telefono);
                EditText editMail = (EditText) getActivity().findViewById(R.id.fragment_registrar_usuario_edit_mail);
                EditText editPassword = (EditText) getActivity().findViewById(R.id.fragment_registrar_usuario_edit_password);

                String nombre    = editNombre.getText().toString();
                String direccion = editDireccion.getText().toString();
                String telefono  = editTelefono.getText().toString();
                String mail      = editMail.getText().toString();
                String password  = editPassword.getText().toString();

                if (nombre.isEmpty()||direccion.isEmpty()||telefono.isEmpty()||mail.isEmpty()||password.isEmpty())
                {
                    Toast.makeText(getContext(),"ERROR: se deben completar todos los campos para realizar el registro",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!Utilidades.esMailValido(mail)) {
                    Toast.makeText(getContext(), "ERROR: Por favor ingrese un mail valido", Toast.LENGTH_LONG).show();
                    return;
                }
                new Registrar_Inicio(getActivity()).ejecutarRegistro(
                        nombre +"",
                        mail+"",
                        direccion+"",
                        telefono+"",
                        password+"",
                        FirebaseInstanceId.getInstance().getToken());

            }
        });
    }
}
