package com.example.ludmi.abucaida.fragments;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.activities.Activity_Login;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class FacebookUtil {
    public static String TAG = "FacebookUtil";
    private ProgressDialog progressDialog;
    private Callback_Facebook callback;
    private Frg_login frgLogin;

    private Map<String,String> datosPerfilFacebook=new HashMap<String,String>();

    public FacebookUtil(Frg_login frgLogin) {
        this.callback = (Callback_Facebook) frgLogin;
        this.frgLogin = frgLogin;
    }

    //Inicio de sesion desde el button de facebook. El callback debe estar construido
    public void buttonSession(final LoginButton loginButton, CallbackManager callbackManager) {
        loginButton.setFragment(frgLogin);
        //ponemos el permiso para ver estos datos del contacto
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                progress(frgLogin.getString(R.string.progress_facebook));
                getInfoContact(loginResult);
                //SystemClock.sleep(2000);

                //callback.loginFacebook(true);


                LoginManager.getInstance().logOut();
            }

            @Override
            public void onCancel() {
                //callback.loginFacebook(false);
            }

            @Override
            public void onError(FacebookException error) {
                //callback.loginFacebook(false);
            }
        });
    }

    private void getInfoContact(final LoginResult loginResult) {
        datosPerfilFacebook.clear();
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code
                        try {
                            String name = object.getString("name");
                            String email = object.getString("email");
                            //TODO Crear el objeto usuario

                            datosPerfilFacebook.put("email",email);
                            datosPerfilFacebook.put("nombre",name);

                            callback.loginFacebook(true,datosPerfilFacebook);

                            Log.i(TAG + "::getInfoContact", loginResult.getAccessToken().getUserId());
                        } catch (JSONException e) {
                            Log.e(TAG + "::getInfoContact ", e.getMessage());
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void progress(String title) {
        progressDialog = new ProgressDialog(frgLogin.getActivity());
        progressDialog.setMessage(title);
        progressDialog.show();
    }

    public void close() {
        if (progressDialog != null)
            progressDialog.cancel();
    }

    public void accessToken () {
        FacebookSdk.sdkInitialize(frgLogin.getActivity(), new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                if(AccessToken.getCurrentAccessToken() == null){
                    callback.loginFacebook(true,datosPerfilFacebook);
                }
            }
        });
    }

    // TODO Metodo para obtener la key, se debe generar siempre que se cambia de PC
    public void getKeyHash () {

        try {
            PackageInfo info = frgLogin.getActivity().getPackageManager().getPackageInfo(
                    frgLogin.getActivity().getString(R.string.package_name),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                System.out.println("La Key que esta en el server es " + Base64.encodeToString(md.digest(), Base64.DEFAULT));

                Log.d(TAG + "::getKeyHash::KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.i(TAG, e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            Log.i(TAG, e.getMessage());
        }
    }
}
