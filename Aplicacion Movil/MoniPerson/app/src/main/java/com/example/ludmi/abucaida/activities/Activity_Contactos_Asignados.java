package com.example.ludmi.abucaida.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.bd_android.Bd_lista_contactos;
import com.example.ludmi.abucaida.fragments.Frg_list_contactos_persona;
import com.example.ludmi.abucaida.model.Contacto;

import java.util.List;

/**
 * Created by raulvillca on 25/11/17.
 */

public class Activity_Contactos_Asignados extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contactos_asignados);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String mensaje_id = getIntent().getStringExtra("msj_id");
        String idAbuelo = getIntent().getStringExtra("idAbuelo");
        boolean isAgregar = getIntent().getBooleanExtra("isAgregado", false);

        if (isAgregar == true) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);


            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, Frg_list_contactos_persona.REQUEST_SELECCIONAR_CONTACTO);
            }
        }


        List<Contacto> listaContactosPorId = Bd_lista_contactos.getListaContactosPorId(this, idAbuelo);

        Frg_list_contactos_persona frg = new Frg_list_contactos_persona(idAbuelo, listaContactosPorId );
        Bundle args = new Bundle();

        args.putString("msj_id", mensaje_id);
        frg.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.id_contactos_asignados, frg , Frg_list_contactos_persona.TAG)
                .commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Frg_list_contactos_persona frgListContactosAbuelo = (Frg_list_contactos_persona) getSupportFragmentManager().findFragmentByTag(Frg_list_contactos_persona.TAG);

        if (requestCode == Frg_list_contactos_persona.REQUEST_SELECCIONAR_CONTACTO && resultCode == RESULT_OK) {
            if (frgListContactosAbuelo != null && data != null && data.getData() != null) {
                frgListContactosAbuelo.resultadoSeleccionarContacto(data);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
