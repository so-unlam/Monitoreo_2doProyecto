package com.example.ludmi.abucaida.notificaciones;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.activities.Activity_perfil_persona;
import com.example.ludmi.abucaida.model.Persona;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by raulvillca on 16/5/17.
 */

public class Notification_Service extends FirebaseMessagingService {
    private static String TAG = Notification_Service.class.getSimpleName();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        Map<String,String> notificacion=new HashMap<String,String>();

        notificacion.put("titulo", remoteMessage.getData().get("titulo"));
        notificacion.put("mensaje", remoteMessage.getData().get("mensaje"));
        notificacion.put("fechaEvento", remoteMessage.getData().get("fecha_evento"));
        notificacion.put("mensajeId", remoteMessage.getData().get("mensaje_id"));

        Persona abuelo = new Persona();

        abuelo.setPersona_token_se(remoteMessage.getData().get("persona_token_se"));
        abuelo.setPersona_nombre(remoteMessage.getData().get("persona_nombre"));
        abuelo.setPersona_direccion(remoteMessage.getData().get("persona_direccion"));
        abuelo.setPersona_telefono(remoteMessage.getData().get("persona_telefono"));
        abuelo.setPersona_fecha_nac(remoteMessage.getData().get("persona_fecha_nac"));
        abuelo.setUsuario_id(Integer.parseInt(remoteMessage.getData().get("usuario_id")));

        createNotification(notificacion,abuelo, remoteMessage.getData().get("mensaje_id").toString());
    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT_WATCH)
    public void createNotification(Map<String, String> notificacion, Persona abuelo, String mensaje_id) {

        Intent notificacionIntent = new Intent(getApplicationContext(), Activity_perfil_persona.class);
        notificacionIntent.putExtra("abu", abuelo);
        notificacionIntent.putExtra("msj_id",mensaje_id);

        PendingIntent notificacionPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificacionIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        @SuppressLint("ResourceAsColor")
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_hospital)
                .setContentTitle(notificacion.get("titulo"))
                .setTicker(notificacion.get("titulo"))
                .setSmallIcon(R.mipmap.ic_hospital)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_ambulancia))
                .setContentText(abuelo.getPersona_nombre())
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE))
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .setContentIntent(notificacionPendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(getResources().getColor(R.color.colorIconoNotificacion));
        }

        Notification notification = new NotificationCompat.InboxStyle(notificationBuilder)
                .addLine(notificacion.get("mensaje"))
                .addLine("Persona: " + abuelo.getPersona_nombre())
                .addLine("Fecha del suceso: " + notificacion.get("fechaEvento"))
                .build();

        //el atributo flags de la notificación nos permite ciertas opciones
        notification.flags |= Notification.FLAG_AUTO_CANCEL;//oculta la notificación una vez pulsada
        notification.defaults |= Notification.DEFAULT_SOUND; //sonido
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
        int notificationId = notificacion.get("mensajeId") != null ? Integer.parseInt(notificacion.get("mensajeId")) : 0;
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
