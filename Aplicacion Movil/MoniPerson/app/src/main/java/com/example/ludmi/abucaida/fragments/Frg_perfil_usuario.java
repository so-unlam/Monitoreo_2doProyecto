package com.example.ludmi.abucaida.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;
import com.example.ludmi.abucaida.model.Usuario;
import com.example.ludmi.abucaida.redes.Registrar_Inicio;
import com.google.firebase.iid.FirebaseInstanceId;


public class Frg_perfil_usuario extends Fragment implements View.OnClickListener {

    public static String TAG = Frg_perfil_usuario.class.getName();
    private EditText nombre;
    private EditText mail;
    private EditText direccion;
    private EditText telefono;
    private EditText clave;
    private Button  botonGuardar;

    private String foto;
    private String mailViejo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_perfil_usuario, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Usuario usuario = Bd_Sesion_Usuario.getUsuario(getActivity());

        boolean enabled=true;

        nombre = (EditText) getActivity().findViewById(R.id.fragment_perfil_usuario_edit_nombre);
        mail = (EditText) getActivity().findViewById(R.id.fragment_perfil_usuario_edit_mail);
        direccion = (EditText) getActivity().findViewById(R.id.fragment_perfil_usuario_edit_direccion);
        telefono = (EditText) getActivity().findViewById(R.id.fragment_perfil_usuario_edit_telefono);
        clave = (EditText) getActivity().findViewById(R.id.fragment_perfil_usuario_edit_clave);
        botonGuardar=(Button) getActivity().findViewById(R.id.fragment_regis_perfil_usuario_btn_guardar);
        foto="";

        mailViejo=usuario.getUsuario_mail();

        nombre.setText(usuario.getUsuario_nombre());
        mail.setText(mailViejo);
        direccion.setText(usuario.getUsuario_direccion());
        telefono.setText(usuario.getUsuario_telefono());
        clave.setText(usuario.getUsuario_clave());

        habilitarControles(usuario.getCuenta_facebook());

        Button button = (Button) getActivity().findViewById(R.id.fragment_regis_perfil_usuario_btn_guardar);
        button.setOnClickListener(this);

    }

    private void habilitarControles(Byte cuenta_facebook)
    {
        //si es cuenta facebook se inabilitan los controles
        if(cuenta_facebook==1)
        {
            direccion.setVisibility(View.GONE);
            telefono.setVisibility(View.GONE);
            clave.setVisibility(View.GONE);
            botonGuardar.setVisibility(View.GONE);

            nombre.setFocusable(false);
            mail.setFocusable(false);

        }
        else
        {
            direccion.setVisibility(View.VISIBLE);
            telefono.setVisibility(View.VISIBLE);
            clave.setVisibility(View.VISIBLE);

            nombre.setFocusableInTouchMode(true);
            mail.setFocusableInTouchMode(true);

        }


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_regis_perfil_usuario_btn_guardar:
                new Registrar_Inicio(getActivity()).ejecutarActualizacionUsuario(
                        nombre.getText().toString() +"",
                        direccion.getText().toString()+"",
                        telefono.getText().toString()+"",
                        clave.getText().toString()+"",
                        foto+"",
                        mailViejo+"",
                         mail.getText().toString(),
                         FirebaseInstanceId.getInstance().getToken());
                break;
        }
    }


}
