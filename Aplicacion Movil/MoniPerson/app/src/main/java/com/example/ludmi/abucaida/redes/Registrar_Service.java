package com.example.ludmi.abucaida.redes;


import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by raulvillca on 28/10/17.
 */

public interface Registrar_Service {

    @POST("/laravel/mobile/register")
    Call<ResponseBody> registrarUsuario(@Body RequestBody encrypted);

    @FormUrlEncoded
    @POST("/laravel/mobile/add_elder")
    public Call<Respuesta_Registrar> registrarAbuelo(@Field("usuario_id") int usuario_id,
                                @Field("nombre") String nombre,
                                @Field("direccion") String direccion,
                                @Field("telefono") String telefono,
                                @Field("fecha_nac") String fecha_nac,
                                @Field("token") String token,
                                @Field("imagen") String imagen);

    @FormUrlEncoded
    @POST("/laravel/mobile/update")
    public Call<Respuesta_Login_Usuario> actualizarAbuelo(@Field("tipo")  String tipoConsulta,
                             @Field("nombre") String nombre,
                             @Field("direccion") String direccion,
                             @Field("fecha")  String fecha,
                             @Field("telefono") String telefono,
                             @Field("token") String token);

    @FormUrlEncoded
    @POST("/laravel/mobile/update")
    public Call<Respuesta_Login_Usuario> actualizarUsuario(@Field("tipo")  String tipoConsulta,
                                  @Field("nombre") String nombre,
                                  @Field("direccion") String direccion,
                                  @Field("telefono") String telefono,
                                  @Field("clave") String clave,
                                  @Field("imagen")  String imagen,
                                  @Field("mail") String mailViejo,
                                  @Field("mail_nuevo") String mailNuevo,
                                  @Field("token") String token);

    @FormUrlEncoded
    @POST("/laravel/mobile/delete_elder")
    public Call<Respuesta_Eliminar> eliminarAbuelo(@Field("token") String token,
                               @Field("usuario_id") int usuario_id);
}
