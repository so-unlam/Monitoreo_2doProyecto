package com.example.ludmi.abucaida.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.adapters.Contactos_Adapter;
import com.example.ludmi.abucaida.bd_android.Bd_lista_contactos;
import com.example.ludmi.abucaida.model.Contacto;
import com.example.ludmi.abucaida.notificaciones.Utilidades;


import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ludmi on 2/9/2017.
 */

@SuppressLint("ValidFragment")
public class Frg_list_contactos_persona extends Fragment{

    public static String TAG = Frg_list_contactos_persona.class.getName();
    private List<Contacto> filasContactos;
    private ListView lstContactosAbuelos;
    private FloatingActionButton btnAgregar;

    private int posicionSeleccionada=-1;
    private Contactos_Adapter contactoAdapter;

    private String mensaje_id;
    private String tokenAbuelo;
    public static final int REQUEST_SELECCIONAR_CONTACTO = 1;
    public static final int FRG_LIST_CONTACTO_ABUELO_BOTTOM=2;

    private Frg_list_contactos_persona context;

    public Frg_list_contactos_persona(String tokenAbuelo, List<Contacto> filasContactos) {
        this.tokenAbuelo = tokenAbuelo;
        this.filasContactos=filasContactos;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_list_contacto_abuelo,container,false);
        btnAgregar = (FloatingActionButton) view.findViewById(R.id.fragment_list_contactos_abuelo_btn_agregar);
        lstContactosAbuelos = (ListView) view.findViewById(R.id.fragment_list_contactos_abuelo_lst_contactos_asignados);

        contactoAdapter = new Contactos_Adapter  (getContext(),filasContactos);
        lstContactosAbuelos.setAdapter(contactoAdapter);
        btnAgregar.setOnClickListener(listenerBotonAgregarAbuelo);
        lstContactosAbuelos.setOnItemClickListener(listenerTocarListView);

        context=this;

        mensaje_id=getArguments().getString("msj_id");

        if(!mensaje_id.equals(Utilidades.ACTUALIZACION_COORDENADAS))
            inhabilitarControles();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkearLista(filasContactos);
    }

    public void checkearLista(List<Contacto> contactoList) {
        if (contactoList.isEmpty()) {
            getActivity().findViewById(R.id.fragment_list_contactos_abuelo_lst_contactos_asignados).setVisibility(View.GONE);
            getActivity().findViewById(R.id.fragment_list_contactos_lista_vacia).setVisibility(View.VISIBLE);
        } else {
            getActivity().findViewById(R.id.fragment_list_contactos_abuelo_lst_contactos_asignados).setVisibility(View.VISIBLE);
            getActivity().findViewById(R.id.fragment_list_contactos_lista_vacia).setVisibility(View.GONE);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FRG_LIST_CONTACTO_ABUELO_BOTTOM:
                resultadoSeleccionarBotoomSheet(data);
                break;
            case REQUEST_SELECCIONAR_CONTACTO:
                if (requestCode == Frg_list_contactos_persona.REQUEST_SELECCIONAR_CONTACTO && resultCode == RESULT_OK) {
                    resultadoSeleccionarContacto(data);
                }
                break;
        }
    }

    public  void resultadoSeleccionarBotoomSheet(Intent data){

        filasContactos.remove(posicionSeleccionada);
        actualizarContactoListview();

        Toast.makeText(getContext(), "Contacto desasignado",Toast.LENGTH_LONG).show();
    }


     public void resultadoSeleccionarContacto(Intent data) {
        // Get the URI and query the content provider for the phone number
        Uri contactUri = data.getData();
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                                           ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                           ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                           ContactsContract.CommonDataKinds.Phone.PHOTO_URI};

        Cursor cursor = getActivity().getContentResolver().query(contactUri, projection, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int indiceContactoId = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
            int indiceTelefono = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int indiceNombre  = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

            String contactoId = cursor.getString(indiceContactoId);
            String nombre = cursor.getString(indiceNombre);
            String telefono = cursor.getString(indiceTelefono);

            Uri my_contact_Uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI,contactoId);
            InputStream photo_stream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(), my_contact_Uri);
            BufferedInputStream buf = new BufferedInputStream(photo_stream);
            Bitmap bitmapFoto = BitmapFactory.decodeStream(buf);

            agregarContactoListview(contactoId,nombre,telefono,bitmapFoto);
        }
    }

    public void agregarContactoListview(String contactoId,String nombre,String telefono,Bitmap foto){

        Contacto item = new Contacto();

        item.setContacto_id(contactoId);
        item.setContacto_nombre(nombre);
        item.setContacto_telefono(telefono);
        item.setContacto_imagen(foto);
        item.setPersona_token_se(tokenAbuelo);

        filasContactos.add(item);
        actualizarContactoListview();
    }

    public void actualizarContactoListview(){
        checkearLista(filasContactos);

        contactoAdapter.notifyDataSetChanged();
    }


    public void mostrarContactosAgenda() {
        // Start an activity for the user to pick a phone number from contacts
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);

        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_SELECCIONAR_CONTACTO);
        }


    }

    private View.OnClickListener listenerBotonAgregarAbuelo = new View.OnClickListener() {
        public void onClick(View view) {
            mostrarContactosAgenda();
        }

    };

    private AdapterView.OnItemClickListener listenerTocarListView;

    {
        listenerTocarListView = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nombre = filasContactos.get(position).getContacto_nombre();

                Frg_list_contactos_persona_bottom frag = new Frg_list_contactos_persona_bottom();

                Bundle args = new Bundle();
                args.putString("nombreContacto", nombre);
                args.putString("contactoId", filasContactos.get(position).getContacto_id());
                args.putString("msj_id", mensaje_id);
                frag.setArguments(args);

                posicionSeleccionada=position;

                frag.setTargetFragment(context, FRG_LIST_CONTACTO_ABUELO_BOTTOM);
                frag.show(getActivity().getSupportFragmentManager(), Frg_list_contactos_persona_bottom.class.getSimpleName());
            }
        };
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Bd_lista_contactos.agregarContactos(getActivity(), "lista_" + tokenAbuelo, filasContactos);
    }

    public void inhabilitarControles(){
        btnAgregar.setVisibility(View.INVISIBLE);

    }
}
