package com.example.ludmi.abucaida.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;

import java.util.Timer;
import java.util.TimerTask;

public class Activity_Splash extends AppCompatActivity {
    protected Animation animation;
    private ImageView imageView_car;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        animation = AnimationUtils.loadAnimation(this, R.anim.from_top_to_bottom);
        imageView_car = (ImageView) findViewById(R.id.activity_splash_image);
        imageView_car.setVisibility(View.VISIBLE);
        imageView_car.startAnimation(animation);

        new PrimerIndicadorTransicion().execute();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                SystemClock.sleep(2500);

                SharedPreferences sharedPreferences = getSharedPreferences(Bd_Sesion_Usuario.CONTROL_SESION, MODE_PRIVATE);
                if (sharedPreferences.getBoolean("login", false))
                    startActivity(new Intent(Activity_Splash.this, Activity_Main.class));
                else
                    startActivity(new Intent(Activity_Splash.this, Activity_Login.class));

                finish();
            }
        };
        new Timer().schedule(timerTask, 100);
    }

    public class PrimerIndicadorTransicion extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            SystemClock.sleep(1300);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
            imageView_car.startAnimation(animation);
            new SegundoIndicadorTransicion().execute();
        }
    }

    public class SegundoIndicadorTransicion extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            SystemClock.sleep(600);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
            imageView_car.startAnimation(animation);
        }
    }
}
