package com.example.ludmi.abucaida.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.model.Contacto;

import java.util.List;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Contactos_Adapter extends BaseAdapter {

    private List<Contacto> contactos;
    private Context context;

    public Contactos_Adapter(Context context,List<Contacto> contactos ) {
        this.contactos = contactos;
        this.context = context;
    }

    @Override
    public int getCount() {
        return contactos.size();
    }

    @Override
    public Object getItem(int position) {
        return contactos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if (convertView == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_perfil, parent, false);
        else
            view = convertView;

        //Usar glide para descargar la imagen

        TextView textView_titulo = (TextView) view.findViewById(R.id.item_perfil_txt_titulo);
        TextView textView_subTitulo = (TextView) view.findViewById(R.id.item_perfil_txt_subtitulo);
        ImageView img_Foto= (ImageView) view.findViewById(R.id.item_perfil_img_foto);

        Contacto contacto = contactos.get(position);
        textView_titulo.setText(contacto.getContacto_nombre());
        textView_subTitulo.setText(contacto.getContacto_telefono());
        img_Foto.setImageBitmap(contacto.getContacto_imagen());



        return view;
    }
}
