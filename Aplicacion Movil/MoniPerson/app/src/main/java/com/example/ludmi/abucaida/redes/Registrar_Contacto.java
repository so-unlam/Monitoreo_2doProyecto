package com.example.ludmi.abucaida.redes;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.example.ludmi.abucaida.bd_android.Bd_lista_contactos;
import com.example.ludmi.abucaida.fragments.Callback_Fragmento;
import com.example.ludmi.abucaida.model.Contacto;

import java.util.List;

/**
 * Created by raulvillca on 28/10/17.
 */

public class Registrar_Contacto {
    private AppCompatActivity activity;

    public Registrar_Contacto(AppCompatActivity activity) {
        this.activity = activity;
    }

    public boolean ejecutarRegistrarContacto(Contacto contacto) {

        Bd_lista_contactos.agregarContacto(activity, contacto);
        return true;
    }

    public List<Contacto> ejecutarListaContactos(String token) {
        List<Contacto> contactos = Bd_lista_contactos.getListaContactosPorId(activity, token);

        return contactos;
    }
}
