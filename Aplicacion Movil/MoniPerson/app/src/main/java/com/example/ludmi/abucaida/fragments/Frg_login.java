package com.example.ludmi.abucaida.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.activities.Activity_Main;
import com.example.ludmi.abucaida.model.Usuario;
import com.example.ludmi.abucaida.notificaciones.Utilidades;
import com.example.ludmi.abucaida.redes.Login_Service;
import com.example.ludmi.abucaida.redes.Registrar_Inicio;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import java.util.Map;

/**
 * Created by Ludmi on 21/10/2017.
 */

public class Frg_login extends Fragment implements Callback_Facebook {

    private static String TAG = Frg_login.class.getSimpleName();
    private CallbackManager callbackManager;
    private Callback_Facebook callback;
    private FacebookUtil util;
    public Frg_login() {

    }

    public Frg_login setCallback (Callback_Facebook callback) {
        this.callback = callback;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getContext().getApplicationContext());
        AppEventsLogger.activateApp(getActivity());
        callbackManager = CallbackManager.Factory.create();
        return inflater.inflate(R.layout.fragment_login,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button btnIniciar = (Button) getActivity().findViewById(R.id.fragment_login_btn_iniciar);
        LoginButton button_login_facebook = (LoginButton) getActivity().findViewById(R.id.login_button);

        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editMail = (EditText) getActivity().findViewById(R.id.fragment_login_edit_mail);
                EditText editPassword = (EditText) getActivity().findViewById(R.id.fragment_login_edit_password);

                String mail      = editMail.getText().toString();
                String password  = editPassword.getText().toString();

                if (mail.isEmpty()||password.isEmpty())
                {
                    Toast.makeText(getContext(),"ERROR: se deben completar todos los campos para poder iniciar sesion",Toast.LENGTH_LONG).show();
                    return;
                }
                if(!Utilidades.esMailValido(mail)) {
                    Toast.makeText(getContext(), "ERROR: Por favor ingrese un mail valido", Toast.LENGTH_LONG).show();
                    return;
                }

                new Registrar_Inicio(getActivity()).ejecutarLoginComun(
                        mail+"",
                        password +"",
                        FirebaseInstanceId.getInstance().getToken()
                );

            }
        });

        util = new FacebookUtil(this);
        util.buttonSession(button_login_facebook, callbackManager);


        /***
         * Peticion de prueba para enviar datos sin formato y encriptados.
         */
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_SERVER))
                .build();
        Login_Service login = adapter.create(Login_Service.class);
        JSONObject json = new JSONObject();
        Call<ResponseBody> call=null;
        try {
            json.put("nombre",new String("Esteban Carnuccio"));
            json.put("mail",new String("esteban_c23@yahoo.com.ar"));
        //    json.put("token",new String(FirebaseInstanceId.getInstance().getToken()));
            RequestBody requestBody= Utilidades.encryptRequestBody(json);

            call = login.requestTest(requestBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    Log.e("Prueba","hola");

                    String decrypt= Utilidades.decryptResponseBody(response);
                    Log.e("Ini Login DECRYPT:",decrypt);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error");
            }
        });
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        util.close();
    }

    @Override
    public void loginFacebook(boolean state,Map<String,String> datosPerfilFacebook) {
        if (state) {
            new Registrar_Inicio(getActivity()).ejecutarLoginFacebook(
                                datosPerfilFacebook.get("email").toString()+"",
                                datosPerfilFacebook.get("nombre").toString() +"",
                                FirebaseInstanceId.getInstance().getToken()
                                );

        } else {
            Log.i(TAG, "No se inicia sesion");
        }

    }
}

