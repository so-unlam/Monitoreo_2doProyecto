package com.example.ludmi.abucaida.redes;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.activities.Activity_Main;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;
import com.example.ludmi.abucaida.notificaciones.Utilidades;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by raulvillca on 9/11/17.
 */

public class Registrar_Inicio {
    private static String TAG = Registrar_Inicio.class.getName();
    private Activity activity;
    private Retrofit adapter;

    public Registrar_Inicio(Activity activity) {
        this.activity = activity;

        adapter = new Retrofit.Builder()
                .baseUrl(activity.getString(R.string.API_SERVER))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void ejecutarLoginComun(String mail, String password, String token) {
        JSONObject json = new JSONObject();
        Call<ResponseBody> call = null;
        Login_Service login = adapter.create(Login_Service.class);

        try {
            json.put("mail",new String(mail));
            json.put("clave",new String(password));
            json.put("token",new String(token));

            RequestBody requestBody= Utilidades.encryptRequestBody(json);
            call = login.loginUsuarioComun(requestBody);
            ejecutarRequestLogin(call);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

    }

    public void ejecutarLoginFacebook(String mail, String nombre, String token) {

        JSONObject json = new JSONObject();
        Call<ResponseBody> call = null;
        Login_Service login = adapter.create(Login_Service.class);

        try {
            json.put("nombre",new String(nombre));
            json.put("mail",new String(mail));
            json.put("token",new String(token));

            RequestBody requestBody= Utilidades.encryptRequestBody(json);
            call = login.loginUsuarioFacebook(requestBody);
            ejecutarRequestLogin(call);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }


    }

    public void ejecutarRegistro(String nombre, String mail, String dir, String tel, String clave, String token) {

        JSONObject json = new JSONObject();
        Call<ResponseBody> call = null;
        Registrar_Service service = adapter.create(Registrar_Service.class);

        try {
            json.put("nombre",new String(nombre));
            json.put("mail",new String(mail));
            json.put("direccion",new String(dir));
            json.put("telefono",new String(tel));
            json.put("clave",new String(clave));
            json.put("token",new String(token));

            RequestBody requestBody= Utilidades.encryptRequestBody(json);
            call = service.registrarUsuario(requestBody);
            ejecutarRequestRegistro(call);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }


    }


    private void ejecutarRequestLogin(Call<ResponseBody> call)
    {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> resp) {
                try {
                    String decryptString= Utilidades.decryptResponseBody(resp);
                    Log.e("IN FAcebook DECRYPT :",decryptString);
                    Gson aux= new Gson();
                    Respuesta_Login_Usuario response= aux.fromJson(decryptString,Respuesta_Login_Usuario.class);

                    if(response.isSuccess()) {

                        //guardamos los datos del usuario y cambiamos el estado de la sesion a activa
                        Bd_Sesion_Usuario.logear(activity, response.getUsuario());
                        activity.startActivity(new Intent(activity,Activity_Main.class));
                        activity.finish();
                    }

                    Toast.makeText(activity, response.getMessage(), Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error");
            }
        });
    }


    private void ejecutarRequestRegistro(Call<ResponseBody> call)
    {
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> resp) {
                try {
                    String decryptString= Utilidades.decryptResponseBody(resp);
                    Log.e("IN FAcebook DECRYPT :",decryptString);
                    Gson aux= new Gson();
                    Respuesta_Login_Usuario response= aux.fromJson(decryptString,Respuesta_Login_Usuario.class);

                    if(response.isSuccess()) {
                        //guardamos los datos del usuario y cambiamos el estado de la sesion a activa
                        Toast.makeText(activity, "Usuario creado", Toast.LENGTH_SHORT).show();

                        Bd_Sesion_Usuario.logear(activity, response.getUsuario());
                        activity.startActivity(new Intent(activity, Activity_Main.class));
                        activity.finish();
                    }

                    Toast.makeText(activity, response.getMessage(), Toast.LENGTH_LONG).show();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Error");
            }
        });
    }

    public void ejecutarActualizacionUsuario(String nombre, String dir, String tel, String clave, String foto, String mailViejo,String mailNuevo,String token) {
        Registrar_Service service = adapter.create(Registrar_Service.class);

        Call<Respuesta_Login_Usuario> response = service.actualizarUsuario("usuario", nombre, dir, tel, clave, foto, mailViejo, mailNuevo, token);
        response.enqueue(new Callback<Respuesta_Login_Usuario>() {
            @Override
            public void onResponse(Call<Respuesta_Login_Usuario> call, Response<Respuesta_Login_Usuario> response) {
                if(response.body().isSuccess()) {

                    //guardamos los datos del usuario y cambiamos el estado de la sesion a activa
                    Bd_Sesion_Usuario.logear(activity, response.body().getUsuario());

                    //Completar - si es fragmento (notificar), si es activida (finalizar)


                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Respuesta_Login_Usuario> call, Throwable t) {
                Toast.makeText(activity, "Error al registrar el usuario", Toast.LENGTH_LONG).show();
                Log.e(TAG, "error " + t.getMessage());
            }
        });
    }
}
