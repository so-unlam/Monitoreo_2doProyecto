package com.example.ludmi.abucaida.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.activities.Activity_QR_captura;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;
import com.example.ludmi.abucaida.model.Persona;
import com.example.ludmi.abucaida.redes.Registrar_persona;
import com.example.ludmi.abucaida.redes.Respuesta_Abuelos;

import static com.example.ludmi.abucaida.notificaciones.Utilidades.mostrarDialogoAlerta;

/**
 * Created by raulvillca on 9/11/17.
 */

public class Frg_registrar_persona extends Fragment implements Callback_Fragmento<Respuesta_Abuelos> {
    public static String TAG = Frg_registrar_persona.class.getName();

    private EditText editNombre   = null;
    private EditText editTelefono  = null;
    private EditText editDireccion = null;
    private EditText editToken = null;
    private EditText editFechaNac = null;
    private Frg_registrar_persona frg;
    private Button btnRegistrar;
    private ImageButton btnEscanearQR;

    private static int BARCODE_READER_REQUEST_CODE = 10;

    public Frg_registrar_persona() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_registrar_abuelo, container, false);

        frg= this;


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        editNombre = (EditText) getActivity().findViewById(R.id.fragment_registrar_abuelo_edit_nombre);
        editTelefono = (EditText) getActivity().findViewById(R.id.fragment_registrar_abuelo_edit_telefono);
        editDireccion = (EditText) getActivity().findViewById(R.id.fragment_registrar_abuelo_edit_direccion);
        editToken = (EditText) getActivity().findViewById(R.id.fragment_registrar_abuelo_edit_token);
        editFechaNac = (EditText) getActivity().findViewById(R.id.fragment_registrar_abuelo_edit_fecha_nac);

        btnRegistrar= (Button) getActivity().findViewById(R.id.fragment_registrar_abuelo_btn_registrar);
        btnEscanearQR = (ImageButton) getActivity().findViewById(R.id.fragment_registrar_abuelo_btn_escanear);

        btnEscanearQR.setOnClickListener(listenerBotonEscanearQR);
        btnRegistrar.setOnClickListener(listenerBotonRegistrar);


    }

    @Override
    public void mensajeFragmento(final Respuesta_Abuelos repuesta_abuelo) {

        Persona abuelo=repuesta_abuelo.getPersonas().get(0);
        final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //click en el boton SI

                        if (editToken.getText().toString().isEmpty())
                        {
                            Toast.makeText(getContext(),"ERROR: Se debe ingresar el Codigo QR correspondiente a la persona a monitorear",
                                    Toast.LENGTH_LONG).show();
                            return;
                        }

                        SharedPreferences preferences = getActivity().getSharedPreferences(Bd_Sesion_Usuario.CONTROL_SESION, getActivity().MODE_PRIVATE);
                        new Registrar_persona(getActivity()).ejecutarRegistrarAbuelo(
                                preferences.getInt("usuario_id", 0),
                                editNombre.getText()+"",
                                editDireccion.getText()+"",
                                editTelefono.getText()+"",
                                editFechaNac.getText()+"",
                                editToken.getText()+"",
                                ""
                        );

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //click en el boton NO
                        editToken.setText(null);
                        Toast.makeText(getActivity(), "Por favor seleccione otro Codigo QR para este persona", Toast.LENGTH_LONG).show();

                        break;
                }
            }
        };
        mostrarDialogoAlerta(this.getActivity(),"Codigo QR ya asignado a la persona "+abuelo.getPersona_nombre(),"¿Desea asignar este abuelo a su usuario?",dialogClickListener);

    }


    private View.OnClickListener listenerBotonEscanearQR = new View.OnClickListener() {
        public void onClick(View v) {
            //Hay que crear el layout para la camara, y terminar el activity Activity_QR_captura
            Intent intent = new Intent(getActivity(), Activity_QR_captura.class);
            startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
        }
    };


    private View.OnClickListener listenerBotonRegistrar = new View.OnClickListener() {
        public void onClick(View v) {
            if (editToken.getText().toString().isEmpty())
            {
                Toast.makeText(getContext(),"ERROR: Se debe ingresar el Codigo QR correspondiente a la persona a monitorear",
                        Toast.LENGTH_LONG).show();
                return;
            }

            SharedPreferences preferences = getActivity().getSharedPreferences(Bd_Sesion_Usuario.CONTROL_SESION, getActivity().MODE_PRIVATE);
            new Registrar_persona(getActivity()).ejecutarVerificarExistenciaAbuelo(
                    frg,
                    preferences.getInt("usuario_id", 0),
                    editNombre.getText()+"",
                    editDireccion.getText()+"",
                    editTelefono.getText()+"",
                    editFechaNac.getText()+"",
                    editToken.getText()+"",
                    ""
            );
        }

    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BARCODE_READER_REQUEST_CODE) {
            //obtenemos el resultado de QRCapturarActivity

            String codigoQr=data.getStringExtra("CodigoQR");
            if(!codigoQr.isEmpty()) {
                editToken.setText(codigoQr);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
