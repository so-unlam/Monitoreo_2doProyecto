package com.example.ludmi.abucaida.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Usuario  implements Serializable {
    private int usuario_id;
    @SerializedName("cuenta_facebook")
    private Byte cuenta_facebook;
    private String usuario_nombre;
    private String usuario_mail;
    private String usuario_direccion;
    private String usuario_telefono;
    private String usuario_clave;
    private String usuario_token_gcm;
    private String usuario_imagen;

    public int getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(int usuario_id) {
        this.usuario_id = usuario_id;
    }

    public void setCuenta_facebook(Byte cuenta_facebook){this.cuenta_facebook=cuenta_facebook;}

    public Byte getCuenta_facebook(){return this.cuenta_facebook;}

    public String getUsuario_nombre() {
        return usuario_nombre;
    }

    public void setUsuario_nombre(String usuario_nombre) {
        this.usuario_nombre = usuario_nombre;
    }

    public String getUsuario_mail() {
        return usuario_mail;
    }

    public void setUsuario_mail(String usuario_mail) {
        this.usuario_mail = usuario_mail;
    }

    public String getUsuario_direccion() {
        return usuario_direccion;
    }

    public void setUsuario_direccion(String usuario_direccion) {
        this.usuario_direccion = usuario_direccion;
    }

    public String getUsuario_telefono() {
        return usuario_telefono;
    }

    public void setUsuario_telefono(String usuario_telefono) {
        this.usuario_telefono = usuario_telefono;
    }

    public String getUsuario_clave() {
        return usuario_clave;
    }

    public void setUsuario_clave(String usuario_clave) {
        this.usuario_clave = usuario_clave;
    }

    public String getUsuario_token_gcm() {
        return usuario_token_gcm;
    }

    public void setUsuario_token_gcm(String usuario_token_gcm) {
        this.usuario_token_gcm = usuario_token_gcm;
    }

    public String getUsuario_imagen() {
        return usuario_imagen;
    }

    public void setUsuario_imagen(String usuario_imagen) {
        this.usuario_imagen = usuario_imagen;
    }
}
