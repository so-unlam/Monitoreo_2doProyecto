package com.example.ludmi.abucaida.redes;

import com.example.ludmi.abucaida.model.Evento;

import java.util.List;

public class Respuesta_Marker {
    private boolean success;
    private String message;
    private List<Evento> events;
    private String wayPoints;

    public void setEvents(List<Evento> events) {
        this.events = events;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Evento> getEvents() {
        return events;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getWayPoints() {
        return wayPoints;
    }

    public void setWayPoints(String wayPoints) {
        this.wayPoints = wayPoints;
    }
}