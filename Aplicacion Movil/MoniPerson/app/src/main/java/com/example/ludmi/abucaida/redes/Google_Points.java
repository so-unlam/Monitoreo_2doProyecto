package com.example.ludmi.abucaida.redes;

import com.google.gson.annotations.SerializedName;

public class Google_Points {
    @SerializedName("points")
    private String points;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }
}
