package com.example.ludmi.abucaida.model;

public class Evento {
    private int suceso_id;
    private String suceso_fecha_hora;
    private String suceso_notificacion;
    private Double suceso_gps_longitud;
    private Double suceso_gps_latitud;
    private String persona_token_se;

    public String getSuceso_fecha_hora() {
        return suceso_fecha_hora;
    }

    public void setSuceso_fecha_hora(String suceso_fecha_hora) {
        this.suceso_fecha_hora = suceso_fecha_hora;
    }

    public void setPersona_token_se(String persona_token_se) {
        this.persona_token_se = persona_token_se;
    }


    public void setSuceso_gps_latitud(Double suceso_gps_latitud) {
        this.suceso_gps_latitud = suceso_gps_latitud;
    }

    public void setSuceso_gps_longitud(Double suceso_gps_longitud) {
        this.suceso_gps_longitud = suceso_gps_longitud;
    }

    public void setSuceso_id(int suceso_id) {
        this.suceso_id = suceso_id;
    }

    public void setSuceso_notificacion(String suceso_notificacion) {
        this.suceso_notificacion = suceso_notificacion;
    }

    public Double getSuceso_gps_latitud() {
        return suceso_gps_latitud;
    }

    public Double getSuceso_gps_longitud() {
        return suceso_gps_longitud;
    }

    public int getSuceso_id() {
        return suceso_id;
    }

    public String getPersona_token_se() {
        return persona_token_se;
    }

    public String getSuceso_notificacion() {
        return suceso_notificacion;
    }
}