package com.example.ludmi.abucaida.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.adapters.TablaLogin_Adapter;

import static com.example.ludmi.abucaida.notificaciones.Utilidades.mostrarTokenFirebase;

public class Activity_Login extends AppCompatActivity {

    private ViewPager tablaPagina;
    private TablaLogin_Adapter mPagerAdapter;

    public  static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.activity_login_tab_titulos);

        FragmentManager manager = getSupportFragmentManager();
        mPagerAdapter = new TablaLogin_Adapter(manager, 2);
        tablaPagina = (ViewPager) findViewById(R.id.activity_login_vista_tabla_pagina);
        tablaPagina.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(tablaPagina);

        tablaPagina.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        //se solicita durante la ejecucion permisos de lecturas de la agenda de contactos y camara esto
        // se realiza para version posteriores a Android 6.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkearPermisos();
        }
        mostrarTokenFirebase();
    }

    public void checkearPermisos() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) +
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {

                    mostrarSnackBar();

                } else {
                        solicitarPermisos();
                }
            } else {
                // write your logic code if permission already granted
            }
        }



    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_MULTIPLE_REQUEST:
                if (grantResults.length > 0) {
                    boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean readContacts = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if(cameraPermission && readContacts)
                    {
                        // write your logic here
                    } else {
                        mostrarSnackBar();
                    }
                }
                break;
        }
    }

    private void mostrarSnackBar(){
        Snackbar.make(findViewById(android.R.id.content),
                     "Otorgue los permisos para que el sistema funcione correctamente",
                      Snackbar.LENGTH_INDEFINITE).setAction("HABILITAR",

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            solicitarPermisos();
                        }
                    }).show();
    }

    public void solicitarPermisos(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.CAMERA}, PERMISSIONS_MULTIPLE_REQUEST);
        }

    }
}

