package com.example.ludmi.abucaida.activities;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.fragments.Callback_Fragmento;
import com.example.ludmi.abucaida.model.Persona;
import com.example.ludmi.abucaida.model.PersonMarker;
import com.example.ludmi.abucaida.model.Evento;
import com.example.ludmi.abucaida.redes.Persona_Marker;
import com.example.ludmi.abucaida.redes.PolylineDecoder;
import com.example.ludmi.abucaida.redes.Registrar_Marker;
import com.example.ludmi.abucaida.redes.Respuesta_Marker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Activity_Mapa extends FragmentActivity implements OnMapReadyCallback,LocationListener, Callback_Fragmento<Respuesta_Marker> {
    private GoogleMap mMap;
    private Location location;
    private LocationManager locationManager;
    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 30;
    public static final long MIN_TIME_BW_UPDATES = 1000 * 45;
    public static String TAG = "Activity_Mapa";
    public List<PersonMarker> posicion;
    private Marker marker;
    private Persona abuelo_actual;
    private Retrofit adapter;
    private String option;
    private Integer icon;

    private Boolean isActived = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        option = getIntent().getStringExtra("msj_id");

        if(option.equals("2"))
            icon = R.mipmap.ic_abu_red;
        else
            icon =R.mipmap.ic_abu_green;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        abuelo_actual = (Persona) getIntent().getSerializableExtra("abu");
        adapter = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_SERVER))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        10).show();
            } else {
                Log.i(TAG, "Este dispositivo no soporta esta version del mapa.");
            }
        }

        // pedimos permisos para usar la geolocalizacion del dispositivo
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //A partir de la api 23, se tiene que pedir permisos al dispositivo
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET}, 10);

            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                if (ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                    return;
        }

        //TODO preseteamos algunas herramientos que el mapa nos puede proveer
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);

        getLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 10) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
            //Si se aceptaron los permisos entonces debemos mostrar nuestra ubicacion
            //volviendo a pedir a los servicios de ubicacion nuestra posicion
            getLocation();
        }
    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) getApplicationContext()
                    .getSystemService(getApplicationContext().LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // Si no hay proveedor habilitado
                //obtengo mi posicion
                LatLng latLng = new LatLng(-34.670367, -58.563585);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
                mMap.animateCamera(cameraUpdate);
            } else {
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return null;
                    }
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            positionUpdate(location);
                        }
                    }
                    // if GPS Enabled get lat/long using GPS Services
                } else if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                positionUpdate(location);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(Activity_Mapa.TAG + "::getLocation", e.getMessage());
        }
        return location;
    }

    private void positionUpdate(Location location) {

        if (location != null) {
            //obtengo mi posicion
            this.location = location;
            float zoomLevel = 16.0f; //This goes up to 21
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
            //mMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel));
            //TODO si quieren que se enfoquen siempre el punto
            //mMap.animateCamera(cameraUpdate);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void setAbuMarker(List<Evento> list, String polylineEncoding, int zoom) {
        if (mMap != null) {
            mMap.clear();
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            list.stream().forEach(evento -> {
                if (evento.getSuceso_gps_latitud() != null && evento.getSuceso_gps_longitud() != null) {
                    LatLng latLngParking = new LatLng(evento.getSuceso_gps_latitud(), evento.getSuceso_gps_longitud());
                    builder.include(latLngParking);

                    //mMap.setOnMarkerClickListener(this);
                    marker = mMap.addMarker(new MarkerOptions()
                            .position(latLngParking)
                            .title(evento.getSuceso_fecha_hora() + ""));
                    //.snippet(list.get(i).getDescripcion())
                    //.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher)));
                    marker.setIcon(BitmapDescriptorFactory.fromResource(icon));
                }
            });

            if (location != null) {
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                builder.include(latLng);

                Log.e(Activity_Mapa.TAG, "Establecer zoom");

                mMap.addPolyline(new PolylineOptions().addAll(PolylineDecoder.decode(polylineEncoding)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 500, 500, zoom));
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f));
                //mMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
            }
        } else {
            Log.e(Activity_Mapa.TAG, "No hay mapa");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        positionUpdate(location);
        //obtenemos la posicion pidiendo informacion a los
        //servicios de posicionamiento
        Log.i(TAG, abuelo_actual.getPersona_token_se().toString());
        new Registrar_Marker(Activity_Mapa.this, adapter).getMarkers(abuelo_actual.getPersona_token_se(), option,
                location.getLatitude() + "," + location.getLongitude(),
                getResources().getString(R.string.API_GOOGLEDIRECTION),
                getResources().getString(R.string.TOKEN_GOOGLE));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

        Log.i(TAG, "Mostrar");
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void mensajeFragmento(Respuesta_Marker respuesta) {

        //getLocation();
        if (!respuesta.getEvents().isEmpty())
            setAbuMarker(respuesta.getEvents(), respuesta.getWayPoints(),16);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActived = false;
    }
}
