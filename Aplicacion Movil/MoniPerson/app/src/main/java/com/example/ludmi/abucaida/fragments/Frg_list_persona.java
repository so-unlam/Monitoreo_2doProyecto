package com.example.ludmi.abucaida.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.adapters.Persona_Adapter;
import com.example.ludmi.abucaida.bd_android.Bd_Sesion_Usuario;
import com.example.ludmi.abucaida.model.Persona;
import com.example.ludmi.abucaida.redes.Registrar_persona;
import com.yalantis.phoenix.PullToRefreshView;

import java.util.ArrayList;
import java.util.List;

import static com.example.ludmi.abucaida.notificaciones.Utilidades.getDensidad;

/**
 * Created by Ludmi on 2/9/2017.
 */

public class Frg_list_persona extends Fragment {
    public static String TAG = Frg_list_persona.class.getName();
    private List<Persona> filasAbuelos;
    private FloatingActionButton btnAgregar;
    private ListView lstAbuelos;
    private static final int REFRESH_DELAY = 2000;
    private int usuario_id;
    private PullToRefreshView pullToRefreshView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_list_abuelos,container,false);

        lstAbuelos = (ListView) view.findViewById(R.id.fragment_list_abuelos_lst_abuelos_asignados);
        btnAgregar = (FloatingActionButton) view.findViewById(R.id.fragment_list_abuelos_btn_agregar);
        btnAgregar.setOnClickListener(listenerBotonAgregarAbuelo);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        filasAbuelos = new ArrayList<Persona>();
        SharedPreferences preferences = getActivity().getSharedPreferences(Bd_Sesion_Usuario.CONTROL_SESION, getActivity().MODE_PRIVATE);

        usuario_id = preferences.getInt("usuario_id", 0);

        new Registrar_persona(getActivity()).ejecutarListarAbuelos( usuario_id + "");

        pullToRefreshView = (PullToRefreshView) getActivity().findViewById(R.id.fragment_list_abuelos_swiperefresh);
        pullToRefreshView.setOnRefreshListener(refreshListener);

        //le envio una lista vacia, luego se cargara desde una request
        lstAbuelos.setAdapter(new Persona_Adapter(getContext(), filasAbuelos));
    }

    private PullToRefreshView.OnRefreshListener refreshListener = new PullToRefreshView.OnRefreshListener() {
        @Override
        public void onRefresh() {
            pullToRefreshView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getActivity(),getDensidad(getActivity().getApplicationContext()),Toast.LENGTH_LONG).show();
                    new Registrar_persona(getActivity()).ejecutarListarAbuelos( usuario_id + "");
                    pullToRefreshView.setRefreshing(false);

                }
            }, REFRESH_DELAY);
        }
    };

    private View.OnClickListener listenerBotonAgregarAbuelo = new View.OnClickListener() {
        public void onClick(View view) {

            //ocultamos el fragmento de lista de abuelos para que no se escuche ningun evento.
            getActivity().getSupportFragmentManager().beginTransaction()
                    .hide(getActivity().getSupportFragmentManager().findFragmentByTag(Frg_list_persona.TAG))
                    .commit();

            getActivity().getSupportFragmentManager().beginTransaction()
                    .addToBackStack(null)
                    .add(R.id.contenedor, new Frg_registrar_persona(), Frg_registrar_persona.TAG)
                    .commit();
        }

    };


    private AdapterView.OnItemClickListener listenerTocarListView = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String nombre = filasAbuelos.get(position).getPersona_nombre();

            Frg_list_personas_bottom frag = new Frg_list_personas_bottom();

            Bundle args = new Bundle();
            args.putString("nombreAbuelo", nombre);
            args.putSerializable("abu", filasAbuelos.get(position));
            frag.setArguments(args);

            frag.show(getActivity().getSupportFragmentManager(), Frg_list_personas_bottom.class.getSimpleName());
        }
    };

    public void consultarAbuelos(List<Persona> abuelos) {

        if (abuelos != null) {

            filasAbuelos = abuelos;
            if (abuelos.isEmpty()) {
                Log.e(Frg_list_persona.class.getName(), "Entra Fragmento Vacio");
                (getActivity().findViewById(R.id.fragment_list_abuelos_lista_vacia)).setVisibility(View.VISIBLE);
                (getActivity().findViewById(R.id.fragment_list_abuelos_lista_no_vacia)).setVisibility(View.GONE);
            } else {
                Log.e(Frg_list_persona.class.getName(), "Entra Fragmento No Vacio");
                (getActivity().findViewById(R.id.fragment_list_abuelos_lista_vacia)).setVisibility(View.GONE);
                (getActivity().findViewById(R.id.fragment_list_abuelos_lista_no_vacia)).setVisibility(View.VISIBLE);
            }
            lstAbuelos.setAdapter(new Persona_Adapter(getContext(), filasAbuelos));
            lstAbuelos.setOnItemClickListener(listenerTocarListView);
        }
    }

}
