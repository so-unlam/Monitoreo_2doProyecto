package com.example.ludmi.abucaida.redes;

import com.example.ludmi.abucaida.model.Persona;

import java.util.List;

/**
 * Created by raulvillca on 10/11/17.
 */

public class Respuesta_Abuelos {
    private List<Persona> personas;
    private boolean success;
    private String message;

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setMessage(String message) { this.message=message;}

    public String getMessage(){ return message;}
}
