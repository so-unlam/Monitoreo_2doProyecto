package com.example.ludmi.abucaida.bd_android;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ludmi.abucaida.model.Usuario;
import com.google.gson.Gson;

/**
 * Created by raulvillca on 9/11/17.
 */

public class Bd_Sesion_Usuario {
    public static String CONTROL_SESION = "Control_Sesion";
    public static void deslogear(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(CONTROL_SESION, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean("login", false);
        editor.commit();
    }

    /****
     * Mantenemos la sesion activa, para no cargar el login, y guardamos el usuario, para futuras consultas
     * @param context
     * @param usuario
     */
    public static void logear(Context context, Usuario usuario) {
        SharedPreferences preferences = context.getSharedPreferences(CONTROL_SESION, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt("usuario_id", usuario.getUsuario_id());
        editor.putString("usuario", new Gson().toJson(usuario));
        editor.putBoolean("login", true);
        editor.commit();
    }

    public static Usuario getUsuario(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(CONTROL_SESION, context.MODE_PRIVATE);

        String jsonUsuario = preferences.getString("usuario", null);
        Usuario usuario = null;
        if (jsonUsuario != null)
            usuario = new Gson().fromJson(jsonUsuario, Usuario.class);

        return usuario;
    }
}
