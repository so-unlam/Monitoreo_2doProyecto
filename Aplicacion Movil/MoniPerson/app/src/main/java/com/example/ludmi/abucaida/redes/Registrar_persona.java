package com.example.ludmi.abucaida.redes;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.fragments.Callback_Fragmento;
import com.example.ludmi.abucaida.fragments.Frg_registrar_persona;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by raulvillca on 28/10/17.
 */

public class Registrar_persona {
    private static String TAG = Registrar_persona.class.getName();
    private Callback_Fragmento<Respuesta_Abuelos> callback;
    private Activity activity;
    private Retrofit adapter;

    public Registrar_persona(Activity activity) {
        this.activity = activity;
        callback = (Callback_Fragmento<Respuesta_Abuelos>) activity;

        adapter = new Retrofit.Builder()
                .baseUrl(activity.getString(R.string.API_SERVER))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public void ejecutarRegistrarAbuelo(final int usuario_id, String nombre, String dir, String tel, String fecha_nac, String token, String imagen) {
        Registrar_Service service = adapter.create(Registrar_Service.class);

        Call<Respuesta_Registrar> response = service.registrarAbuelo(usuario_id, nombre, dir, tel, fecha_nac, token, imagen);
        response.enqueue(new Callback<Respuesta_Registrar>() {
            @Override
            public void onResponse(Call<Respuesta_Registrar> call, Response<Respuesta_Registrar> response) {
                Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, response.body().getMessage());

                if(response.body().isSuccess()) {
                    ejecutarListarAbuelos(usuario_id + "");
                }
            }

            @Override
            public void onFailure(Call<Respuesta_Registrar> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
    }

    /***
     * Se realiza la consulta de todos los abuelos, segun al id del usuario.
     * Retorna un objeto Respuesta_Abuelos que se recargara en mensajeFragmento(Lista)
     * @param idUsuario
     */
    public void ejecutarListarAbuelos(String idUsuario) {

        Persona_Service service = adapter.create(Persona_Service.class);

        Call<Respuesta_Abuelos> response = service.listarAbuelos(idUsuario);
        response.enqueue(new Callback<Respuesta_Abuelos>() {
            @Override
            public void onResponse(Call<Respuesta_Abuelos> call, Response<Respuesta_Abuelos> response) {
                Log.e(TAG, "lista " + new Gson().toJson(response.body().getPersonas()));

                callback.mensajeFragmento(response.body());
            }

            @Override
            public void onFailure(Call<Respuesta_Abuelos> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
    }

    public void ejecutarActualizarAbuelo(final String usuario_id, String nombre, String dir, String fecha, String tel, String token) {
        Registrar_Service service = adapter.create(Registrar_Service.class);

        Call<Respuesta_Login_Usuario> response = service.actualizarAbuelo("abuelo", nombre, dir, fecha, tel, token);
        response.enqueue(new Callback<Respuesta_Login_Usuario>() {
            @Override
            public void onResponse(Call<Respuesta_Login_Usuario> call, Response<Respuesta_Login_Usuario> response) {
                if (response.body().isSuccess()) {
                    ejecutarListarAbuelos(usuario_id );
                    Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Respuesta_Login_Usuario> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
    }

    public void eliminarAbuelo(String token, final int usuario_id) {
        Registrar_Service service = adapter.create(Registrar_Service.class);

        Call<Respuesta_Eliminar> response = service.eliminarAbuelo(token, usuario_id);
        response.enqueue(new Callback<Respuesta_Eliminar>() {
            @Override
            public void onResponse(Call<Respuesta_Eliminar> call, Response<Respuesta_Eliminar> response) {
                Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_LONG).show();
                Log.e(TAG, response.body().getMessage());

                if(response.body().isSuccess()) {
                    ejecutarListarAbuelos(usuario_id + "");
                }
            }

            @Override
            public void onFailure(Call<Respuesta_Eliminar> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
    }

    public void ejecutarVerificarExistenciaAbuelo(Frg_registrar_persona fragment, final int usuario_id, final String nombre, final String dir, final String tel, final String fecha_nac, final String token, final String imagen) {
        Persona_Service service = adapter.create(Persona_Service.class);

        final Callback_Fragmento<Respuesta_Abuelos> callback_verficiacion;

        callback_verficiacion= (Callback_Fragmento<Respuesta_Abuelos>) fragment;

        Call<Respuesta_Abuelos> response = service.verificarExitenciaToken(token);
        response.enqueue(new Callback<Respuesta_Abuelos>() {
            @Override
            public void onResponse(Call<Respuesta_Abuelos> call, Response<Respuesta_Abuelos> response) {
                Log.e(TAG, response.body().getMessage());

                if(response.body().isSuccess()) {
                    callback_verficiacion.mensajeFragmento(response.body());
                }
                else{
                    if(response.body().getMessage().equals("Token disponible")){
                        ejecutarRegistrarAbuelo(usuario_id, nombre,dir,tel,fecha_nac,token,imagen);
                    }
                }
            }

            @Override
            public void onFailure(Call<Respuesta_Abuelos> call, Throwable t) {
                Log.e(TAG, "Error " + t.getMessage());
            }
        });
    }

}
