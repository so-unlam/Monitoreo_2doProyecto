package com.example.ludmi.abucaida.redes;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Respuesta_Direction {
    @SerializedName("routes")
    List<Google_Polyline> routes;

    public List<Google_Polyline> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Google_Polyline> routes) {
        this.routes = routes;
    }
}
