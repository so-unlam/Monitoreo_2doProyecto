package com.example.ludmi.abucaida.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Ludmi on 2/9/2017.
 */

public class TablaLogin_Adapter extends FragmentStatePagerAdapter {

    private int cantidadTitulos;
    public TablaLogin_Adapter(FragmentManager fm, int cantidadTitulos) {
        super(fm);
        this.cantidadTitulos = cantidadTitulos;
    }

    @Override
    public Fragment getItem(int position) {

        return Tabla_Factory.getInstance(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return Tabla_Factory.getInstanceTitle(position);
    }

    @Override
    public int getCount() {
        return cantidadTitulos;
    }
}
