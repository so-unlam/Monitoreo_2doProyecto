package com.example.ludmi.abucaida.redes;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Ludmi on 28/10/2017.
 */

public interface Login_Service {

    @POST("/laravel/mobile/login")
    Call<ResponseBody> loginUsuarioComun(@Body RequestBody encrypted);

    @POST("/laravel/mobile/request_test")
    Call<ResponseBody> requestTest(@Body RequestBody encrypted);

    @POST("/laravel/mobile/login_facebook")
    Call<ResponseBody> loginUsuarioFacebook(@Body RequestBody encrypted);
}
