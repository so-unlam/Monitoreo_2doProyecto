package com.example.ludmi.abucaida.notificaciones;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import android.view.WindowManager;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * Created by Esteban on 02/11/2017.
 */

public class Utilidades {

    public static  String keyGlobal                      = "A";

    public static final String ACTUALIZACION_COORDENADAS = "1";
    public static final String CAIDA_DETECTADA           = "2";
    public static final String CAIDA_CRITICA             = "3";
    public static final String RECUPERACION              = "4";
    public static final String BOTON_PANICO              = "5";

    @SuppressLint("LongLogTag")
    public static void mostrarTokenFirebase()
    {
        String tokenFirebase= FirebaseInstanceId.getInstance().getToken();

        if(tokenFirebase!=null)
            Log.e("Bd_lista_contactos Firebase", FirebaseInstanceId.getInstance().getToken());
        else
            Log.e("Bd_lista_contactos Firebase", "Token null");

    }

    public static boolean esMailValido(String email) {
        return !(email == null || TextUtils.isEmpty(email)) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static  void mostrarDialogoAlerta(Context context, String titulo, String mensaje, DialogInterface.OnClickListener dialogClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(titulo)
                .setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton("SI", dialogClickListener)
                .setNegativeButton("NO", dialogClickListener)
                .show();
    }

    public static  String getDensidad(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        String statusBarHeight = null;
        switch (displayMetrics.densityDpi) {
            case DisplayMetrics.DENSITY_XXXHIGH:
                statusBarHeight = "XXXHIGH_DPI";
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                statusBarHeight = "XXHIGH_DPI";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                statusBarHeight = "XHIGH_DPI";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                statusBarHeight = "HIGH_DPI";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                statusBarHeight = "MEDIUM_DPI";
                break;
            case DisplayMetrics.DENSITY_LOW:
                statusBarHeight = "LOW_DPI";
                break;
            default:
                statusBarHeight = "MEDIUM_DPI";
        }
        return statusBarHeight;
    }
    public static String encryptDecrypt(String input,String keyXor)
    {
        char[] key = keyXor.toCharArray();

        StringBuilder output = new StringBuilder();
        for(int i = 0; i < input.length(); i++)
        {
            output.append((char) (input.charAt(i) ^ key[i % key.length]));
        }
        return output.toString();
    }

    public static ResponseBody copyResponseBody(ResponseBody body, long limit) throws IOException {
        BufferedSource source = body.source();
        Buffer bufferedCopy;

        if (source.request(limit))
            throw new IOException("body too long!");

        bufferedCopy = source.buffer().clone();
        return ResponseBody.create(body.contentType(), body.contentLength(), bufferedCopy);
    }

    public static RequestBody encryptRequestBody(JSONObject json)
    {
        String resp;
        RequestBody requestBody=null;

        try
        {
            AES_Encryptation aes = new AES_Encryptation();

            resp=aes.encrypt(json.toString());
            requestBody = RequestBody.create(MediaType.parse("text/plain"), resp.toString());
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        } catch (NoSuchPaddingException e)
        {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e)
        {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e)
        {
            e.printStackTrace();
        } catch (BadPaddingException e)
        {
            e.printStackTrace();
        }

        return requestBody;
    }

    public static String decryptResponseBody(retrofit2.Response<ResponseBody> response) throws IOException
    {
        String resp;
        String decrypt=null;
        ResponseBody respuesta;

        try
        {

            AES_Encryptation aes = new AES_Encryptation();

            respuesta=Utilidades.copyResponseBody(response.body(),10000);
            resp=respuesta.string();

            decrypt= aes.decrypt(resp);
        }
        catch (GeneralSecurityException e)
        {
            e.printStackTrace();
        }

        return decrypt;
    }
}
