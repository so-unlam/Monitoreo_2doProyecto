package com.example.ludmi.abucaida.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ludmi.abucaida.R;
import com.example.ludmi.abucaida.model.Persona;

import java.util.List;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Persona_Adapter extends BaseAdapter {

    private Context context;
    private List<Persona> abus;

    public Persona_Adapter(Context context, List<Persona> abus) {
        this.abus = abus;
        this.context = context;
    }

    @Override
    public int getCount() {
        return abus.size();
    }

    @Override
    public Object getItem(int position) {
        return abus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;

        if (convertView == null) {
            v = LayoutInflater.from(context).inflate(R.layout.item_perfil, parent, false);
        } else {
            v = convertView;
        }

        ImageView imageView_Foto = (ImageView)  v.findViewById(R.id.item_perfil_img_foto);
        TextView textView_titulo = (TextView) v.findViewById(R.id.item_perfil_txt_titulo);
        TextView textView_subTitulo = (TextView) v.findViewById(R.id.item_perfil_txt_subtitulo);


        Persona abu = abus.get(position);
        textView_titulo.setText(abu.getPersona_nombre());
        textView_subTitulo.setText(abu.getPersona_fecha_nac());

        return v;
    }
}
