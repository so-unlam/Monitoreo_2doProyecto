package com.example.ludmi.abucaida.redes;

import com.google.gson.annotations.SerializedName;

public class Google_Polyline {
    @SerializedName("overview_polyline")
    private Google_Points overview_polyline;

    public Google_Points getOverview_polyline() {
        return overview_polyline;
    }

    public void setOverview_polyline(Google_Points overview_polyline) {
        this.overview_polyline = overview_polyline;
    }
}
