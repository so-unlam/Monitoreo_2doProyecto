package com.example.ludmi.abucaida.model;

import android.graphics.Bitmap;

/**
 * Created by raulvillca on 21/10/17.
 */

public class Contacto {
    private String persona_token_se;
    private String contacto_id;
    private String contacto_nombre;
    private String contacto_tipo;
    private String contacto_telefono;
    private String contacto_direccion;
    private Bitmap contacto_imagen;

    public String getPersona_token_se() {
        return persona_token_se;
    }

    public void setPersona_token_se(String persona_token_se) {
        this.persona_token_se = persona_token_se;
    }

    public String getContacto_id() {
        return contacto_id;
    }

    public void setContacto_id(String contacto_id) {
        this.contacto_id = contacto_id;
    }

    public String getContacto_nombre() {
        return contacto_nombre;
    }

    public void setContacto_nombre(String contacto_nombre) {
        this.contacto_nombre = contacto_nombre;
    }

    public String getContacto_tipo() {
        return contacto_tipo;
    }

    public void setContacto_tipo(String contacto_tipo) {
        this.contacto_tipo = contacto_tipo;
    }

    public String getContacto_telefono() {
        return contacto_telefono;
    }

    public void setContacto_telefono(String contacto_telefono) {
        this.contacto_telefono = contacto_telefono;
    }

    public String getContacto_direccion() {
        return contacto_direccion;
    }

    public void setContacto_direccion(String contacto_direccion) {
        this.contacto_direccion = contacto_direccion;
    }

    public Bitmap getContacto_imagen() {
        return contacto_imagen;
    }

    public void setContacto_imagen(Bitmap contacto_imagen) {
        this.contacto_imagen = contacto_imagen;
    }
}
