package com.example.ludmi.abucaida.model;

public class PersonMarker {

    private Double Latitud;
    private Double Longitud;
    private String Descripcion;

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setLatitud(Double latitud) {
        Latitud = latitud;
    }

    public void setLongitud(Double longitud) {
        Longitud = longitud;
    }

    public Double getLatitud() {
        return Latitud;
    }

    public Double getLongitud() {
        return Longitud;
    }

    public PersonMarker(){
    }
}
