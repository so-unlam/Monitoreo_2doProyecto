Branch MASTER
=========================================================
**Esta Rama contendrá la versión final del sistema a desarrollar**

Repostorio GIT con el desarrollo del Proyecto de Investigación denominado **Dispositivo de asistencia de personas mediante monitoreo y análisis de datos en la nube**,
que se esta llevando a cabo en la Universidad Nacional de La Matanza por los siguientes investigadores:

* Graciela De Luca
* Waldo Valiente
* Mariano Volker
* Esteban Carnuccio
* Gerardo García
* Raúl Villca
* Marcos Vittorio

El presente proyecto corresponde a la continuación de la investigación [Monitorización de Adultos Mayores](https://gitlab.com/soa-unlam/monitoreo) resalizada por este grupo de investigación
	

