<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('mobile/register', 'MobileController@registrarUsuario');

Route::post('mobile/login', 'MobileController@iniciarSesion');

Route::post('mobile/add_contact', 'MobileController@agregarContacto');

Route::post('mobile/add_elder', 'MobileController@agregarAnciano');

Route::post('mobile/add_elder_contact', 'MobileController@asignarContactoAbuelo');

Route::get('mobile/watch_se', 'MobileController@monitorearSE');

Route::post('mobile/send_notication', 'MobileController@enviarNotificacion');

Route::post('mobile/update', 'MobileController@actualizarDatos');

Route::post('mobile/reset_pass', 'MobileController@recuperarClave');

Route::get('mobile/list_all', 'MobileController@mostrar');

Route::get('mobile/elder_datail', 'MobileController@mostrarAnciano');

Route::get('mobile/contact_datail', 'MobileController@mostrarContacto');