<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suceso extends Model
{
    //
    protected $table = 'suceso';
    protected $fillable = [
        'suceso_id',
        'suceso_fecha',
        'suceso_notificacion',
        'suceso_gps_longitud',
        'suceso_gps_latitud',
        'persona_token_se'
    ];

    public function incidente()
    {
        return $this->belongsTo(Persona::class);
    }
}
