CREATE TABLE `mensaje` (
  `mensaje_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mensaje_titulo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mensaje_descripcion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mensaje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `usuario` (
  `usuario_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `usuario_nombre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_direccion` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_telefono` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_clave` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuario_token_gcm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `persona` (
  `persona_nombre` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_direccion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_telefono` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_fecha_nac` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_imagen` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_token_se` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`persona_token_se`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `suceso` (
  `suceso_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `suceso_fecha` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suceso_notificacion` varchar(230) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suceso_gps_latitud` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `suceso_gps_longitud` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `persona_token_se` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`suceso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci

CREATE TABLE `usuario_persona` (
  `persona_token_se` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci