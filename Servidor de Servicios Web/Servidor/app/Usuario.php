<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $fillable = [
        'usuario_id',
        'usuario_nombre',
        'usuario_mail',
        'usuario_direccion',
        'usuario_telefono',
        'usuario_clave',
        'usuario_imagen',
        'usuario_token_gcm'
    ];

    public function incidente()
    {
        return $this->hasMany(Persona::class);
    }
}
