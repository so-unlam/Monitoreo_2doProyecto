<?php
namespace App\Http\Library;

use Illuminate\Database\Eloquent\Model;

//Clave XOR usada para encriptar los post que llegan desde Android
$GLOBALS['keyGlobal']="0123456789abcdef";

class ManagerEncrypt extends Model
{
	
	
	
	public static function encryptAes($data, $key) 
	{
		return base64_encode(openssl_encrypt($data, "aes-128-ecb", $key, OPENSSL_RAW_DATA));
	}

	public static  function decryptAes($data, $key) 
	{
		return openssl_decrypt(base64_decode($data), "aes-128-ecb", $key, OPENSSL_RAW_DATA);
	}	 
	
	public static function decryptXorHex( $key, $text )
	{
		$encrypt = '';
		$cnt     = 0;
		$char_ascii='';
		
		// Convierte el <string> en un <vector>, en forma eficiente.
		$vkey    = str_split($key);
		
		for($i=0;$i<strlen($text);$i+=2)
		{
			$char_ascii = chr(hexdec(substr($text,$i,2)));
			$encrypt .= $char_ascii ^ $vkey[ ($i/2) % (count( $vkey )) ]; 
		}  
		
	
		return $encrypt;
	}
	
	public static function convertAESToJson($texto,$key)
	{
		//decodifico el mensaje recibido
		$texto_decrypt= ManagerEncrypt::decryptAes($texto,$key);
	
		$resp=json_decode($texto_decrypt,true);
		
		return $resp;
	}
	
	public static function convertJsonToAES($json,$key)
	{
		$texto=json_encode($json);
		
		$resp=ManagerEncrypt::encryptAes($texto,$key);
		
		return $resp;
	}
	
	public static function  decryptRequest($requestPost)
	{
		//Descencripto el mensaje request del POST
		$texoRequest=$requestPost->getContent();
		return ManagerEncrypt::convertAESToJson($texoRequest,$GLOBALS['keyGlobal']);	
	}

	public static function encryptResponse($data)
	{
		return ManagerEncrypt::convertJsonToAES($data,$GLOBALS['keyGlobal']);	
	}
}