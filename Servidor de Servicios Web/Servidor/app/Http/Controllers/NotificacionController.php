<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensaje;
use App\Persona;
use App\Suceso;
use App\Usuario;

use App\Http\Library\ManagerEncrypt;

class NotificacionController extends Controller
{

    public $API_ACCESS_KEY = 'AAAALP03hfc:APA91bGk5XVIyqc8zpal3svFtSnrtOOQ9-Zdqo2KaihvfWirm_rBnIBUDDrHjSb9OXlfKBys-w9AxTrNcRXQ5_eFEEwj3EJsYlqoDNKLTjuSggIpTYRoCMQI5UHs7rKs9_L59IA_OrFz';
    public $URI_FIREBASE_NOTIFICATION = 'https://fcm.googleapis.com/fcm/send';

    public function enviarPosicionTest(Request $requestPost) {
        $data['success'] = false;

        if (!empty($requestPost->input('DATOS'))) {
            $data['message'] = 'Estructura de mensaje a enviar esta en Datos:';
	
            $data['datos']['titulo'] = '';
            $data['datos']['mensaje'] = '';
            $data['datos']['usuario_id'] = '';
            $data['datos']['usuario_token_gcm'] = '';
            $data['datos']['persona_token_se'] = '';
            $data['datos']['persona_nombre'] = '';
            $data['datos']['persona_direccion'] = '';
            $data['datos']['persona_telefono'] = '';
            $data['datos']['persona_fecha_nac'] = '';

            return $data;
        }

        $titulo = $requestPost->input('titulo');
        $mensaje = $requestPost->input('mensaje');
        $fechaEvento = date("Y-m-d H:i:s");

        $result = $this->notificarTest([
            'titulo'        =>  $titulo,
            'mensaje'       =>  $mensaje,
            'usuario_id'   	     => $requestPost->input('usuario_id'),
            'usuario_token_gcm'  => $requestPost->input('usuario_token_gcm'),
			'persona_token_se'   => $requestPost->input('persona_token_se'),
			'persona_nombre'     => $requestPost->input('persona_nombre'),
			'persona_direccion'  => $requestPost->input('persona_direccion'),
			'persona_telefono'   => $requestPost->input('persona_telefono'),
            'persona_fecha_nac'  => $requestPost->input('persona_fecha_nac'),
            'fecha_evento'	=>  $fechaEvento
        ]);

        $data['success'] = true;
        $data['message'] = 'Notificacion enviada al contacto';
        $data['persona_token_se'] = $requestPost->input('persona_token_se'); 

        return $data;
    }

    private function notificarTest($datos) {
        $headers = array
        (
            'Authorization: key=' . $this->API_ACCESS_KEY,
            'Content-Type: application/json'
        );
		

        $msg = array
        (
            'titulo'       	 	 => $datos['titulo'],
            'mensaje'     	  	 => $datos['mensaje'],
			'fecha_evento'  	 => $datos['fecha_evento'],
			'usuario_id'   	     => $datos['usuario_id'],
			'persona_token_se'   => $datos['persona_token_se'],
			'persona_nombre'     => $datos['persona_nombre'],
			'persona_direccion'  => $datos['persona_direccion'],
			'persona_telefono'   => $datos['persona_telefono'],
            'persona_fecha_nac'  => $datos['persona_fecha_nac']
        );

        $fields = array
        (
            'to' => $datos['usuario_token_gcm'],
            'data' => $msg
        );

        //Request para realizar peticion a la nube de google
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URI_FIREBASE_NOTIFICATION);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $resultado = curl_exec($ch);
        curl_close($ch);

        return $resultado;
    }

    public function enviarNotificacionNuevo(Request $requestPost) {

        $titulo = '';
        $mensaje = '';
		$usuarios ='';
        $result=null;
        
        $data['success'] = false;

        $tablaMensaje = $this->getMensaje($requestPost->input('C'));

        $fechaEvento = date("Y-m-d H:i:s");

        if (count($tablaMensaje) > 0) {
            $titulo = $tablaMensaje[0]['mensaje_titulo'];
            $mensaje = $tablaMensaje[0]['mensaje_descripcion'];
        }

        if ($requestPost->has('T')) {

            //Buscamos los sucesos de cada token.
            $sucesos = $this->getSuceso($requestPost->input('T'));
            
            if (count($sucesos) > 4) {
                Suceso::where('suceso_id', '=', $sucesos[4]['suceso_id'])->delete();
            }

			if (($requestPost->input('C') < 1)|| ($requestPost->input('C') > 5)){
				$data['message'] = 'Campo C del request incorrecto';
				return $data;
			}
			
            if ($requestPost->input('C') == 1) {

                $personas = Persona::where('persona_token_se', '=', $requestPost->input('T'))->get();
                if(count($personas) > 0) {
                    $this->registrarPosicion([
                        'fecha_evento'          => $fechaEvento,
                        'suceso_notificacion'   => 'Actualizando coordenadas',
                        'suceso_gps_latitud'    => $requestPost->input('A'),
                        'suceso_gps_longitud'   => $requestPost->input('O'),
                        'persona_token_se'      => $requestPost->input('T'),
                        'mensaje_id'            => $requestPost->input('C')
                    ]);
                    
                    $data['message'] = 'Suceso actualizado';
                    $data['success'] = true;
                } else {
                    $data['message'] = 'No existe token SE';
                }

                return $data;
            }

            $usuariosAbuelo = $this->consultarUsuariosAbuelo($requestPost->input('T'));

            if (count($usuariosAbuelo) > 0) {

				//se realiza la notificacion del evento al celular de cada usuario que se encuentra
				//asignado a dicho abuelo
                foreach ($usuariosAbuelo as &$usuarioAbuelo) {

                    $result = $this->notificar([
                        'mensaje_id'    =>  $requestPost->input('C'),
                        'titulo'        =>  $titulo,
                        'mensaje'       =>  $mensaje,
                        'usuario'       =>  $usuarioAbuelo,
                        'fecha_evento'	=>  $fechaEvento
                    ]);

                    if (empty($result)) {
                        $data['message'] = 'No se pudo enviar la notificacion a los contactos asociados';
                        $data['success'] = false;
                    } else {
                        $personas = Persona::where('persona_token_se', '=', $requestPost->input('T'))->get();
                        if(count($personas) > 0) {
                            $this->registrarPosicion([
                                'fecha_evento'          => $fechaEvento,
                                'suceso_notificacion'   => $result,
                                'suceso_gps_latitud'    => $requestPost->input('A'),
                                'suceso_gps_longitud'   => $requestPost->input('O'),
                                'persona_token_se'      => $requestPost->input('T'),
                                'mensaje_id'            => $requestPost->input('C')
                            ]);
                            
                            $data['success'] = true;
                            $data['message'] = $titulo;
                        } else {
                            $data['message'] = 'No existe token SE';
                        }
                    }
                }

            } else {
                $data['message'] = 'No hay usuarios a quien enviar la notificación';
            }
        } else {
            $data['message'] = 'No se recibio ningun token';
        }

        return $data;
    }


    private function getMensaje($codigo) {
        if (!empty($codigo)) {
            $tablaMensaje = Mensaje::where('mensaje_id', '=', $codigo)->get();
            return $tablaMensaje;
        }

        return null;
    }

    private function getSuceso($token) {
        $tablaSuceso = Suceso::where('persona_token_se', '=', $token)
            ->orderBy('suceso_id', 'Desc')    
            ->get();
        return $tablaSuceso;
    }

    private function notificar($datos) {
        $headers = array
        (
            'Authorization: key=' . $this->API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $msg = array
        (
            'mensaje_id'         => $datos['mensaje_id'],
            'titulo'       	 	 => $datos['titulo'],
            'mensaje'     	  	 => $datos['mensaje'],
			'fecha_evento'  	 => $datos['fecha_evento'],
			'usuario_id'   	     => $datos['usuario']['usuario_id'],
			'persona_token_se'   => $datos['usuario']['persona_token_se'],
			'persona_nombre'     => $datos['usuario']['persona_nombre'],
			'persona_direccion'  => $datos['usuario']['persona_direccion'],
			'persona_telefono'   => $datos['usuario']['persona_telefono'],
            'persona_fecha_nac'  => $datos['usuario']['persona_fecha_nac']
        );

        $fields = array
        (
            'to' => $datos['usuario']['usuario_token_gcm'],
            'data' => $msg
        );

        //Request para realizar peticion a la nube de google
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->URI_FIREBASE_NOTIFICATION);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $resultado = curl_exec($ch);
        curl_close($ch);

        return $resultado;
    }

    private function consultarUsuariosAbuelo($token) {
        $usuarios = Usuario::join('usuario_persona','usuario.usuario_id','=','usuario_persona.usuario_id')
                           ->join('persona','persona.persona_token_se','=','usuario_persona.persona_token_se')
						   ->where('usuario_persona.persona_token_se', '=', $token)
						   ->get();

        return $usuarios;
    }

    private function registrarNotificacion($datos) {
        $personas = Persona::where('persona.persona_token_se', '=', $datos['token'])
            ->get();

        $dato['success'] = true;

        //notificacion - si fue notificado
        if (count($personas) > 0) {

            $dato['message'] = 'Se envio la nofiticacion con exito';
        }

        return $dato;
    }

    private function registrarNotificacionNuevo($datos, $latitud, $longitud) {
        $personas = Persona::where('persona.persona_token_se', '=', $datos['token'])
            ->get();

        $dato['success'] = true;

        //notificacion - si fue notificado
        if (count($personas) > 0) {
            Suceso::insert([
                'suceso_fecha'          => $datos['fecha_evento'],
                'suceso_notificacion'   => $datos['resultado'],
                'suceso_gps_latitud'    => $latitud,
                'suceso_gps_longitud'   => $longitud,
                'persona_token_se'      => $personas[0]['persona_token_se']
            ]);

            $dato['message'] = 'Se envio la nofiticacion con exito';
        }
	
        return $dato;
    }

    private function registrarPosicion($datos) {
        Suceso::insert([
            'suceso_fecha_hora'    	=> $datos['fecha_evento'],
            'suceso_notificacion'   => $datos['suceso_notificacion'],
            'suceso_gps_latitud'    => $datos['suceso_gps_latitud'],
            'suceso_gps_longitud'   => $datos['suceso_gps_longitud'],
            'persona_token_se'      => $datos['persona_token_se'],
            'mensaje_id'            => $datos['mensaje_id']
        ]);
    }

    public function requestTest(Request $requestPost) {
		
		$respuesta="false";
		
		$post=ManagerEncrypt::decryptRequest($requestPost);
		
		//$texto_decrypt=($texo_recibido_encrypt,$GLOBALS['keyGlobal']);
	
		//$resp=json_decode($texto_decrypt,true);
		if($post['nombre']=='Esteban Carnuccio')
		{ 
			$respuesta=ManagerEncrypt::encryptResponse($post);
		}	
        return $respuesta;
		
    }
	
	public function enviarNotificacionTextoPlano(Request $requestPost) 
{
	$texto_plano=$requestPost->getContent();
	return $texto_plano;
}

//WAV: Prueba Encriptación algoritmo Esteban.xor.
/*********************************

Formato del mensaje formato json:

{
    "key":"98:4F:EE:01:0E:32",
    "text":"654321"
}

************************************/
    
    public function xorTest(Request $requestPost) 
    {
        $key             = '';
        $text            = '';
        $text_hexa       = '';
        $text_encrypt    = '' ;
        $data['success'] = false;

        $key  = $requestPost->input('key');
        $text = $requestPost->input('text');
        
        if(empty($key)) 
        {
            $data['message'] = 'Error: Parametro (key) no valido.';
            return $data;
        }

        if(empty($text)) 
        {
            $data['message'] = 'Error: Parametro (text) no valido.';
            return $data;
        }

        $data['message' ]  = 'Codificacion Estaban.xor.';
        $text_hexa     = bin2hex( $text );
        $text_encrypt  = ManagerEncrypt::decryptXorHex( $key, $text);
        $text_enc_hexa = bin2hex( $text_encrypt );        
        $data['key'     ] = $key;
        $data['text'    ] = $text;
        $data['texthexa'] = $text_hexa;	
        $data['encrypt' ] = $text_encrypt;
        $data['encryHX' ] = $text_enc_hexa;
        
        $data['success' ] = true;

        return $data;
    }

	
	  private function enviarNotificacion($requestPost) {

        $titulo = '';
        $mensaje = '';
		$usuarios ='';
        $result=null;
        
        $data['success'] = false;

        $tablaMensaje = $this->getMensaje($requestPost['C']);

        $fechaEvento = date("Y-m-d H:i:s");

        if (count($tablaMensaje) > 0) {
            $titulo = $tablaMensaje[0]['mensaje_titulo'];
            $mensaje = $tablaMensaje[0]['mensaje_descripcion'];
        }

        if (empty($requestPost['T'])!=true) {

            //Buscamos los sucesos de cada token.
            $sucesos = $this->getSuceso($requestPost['T']);
            
            if (count($sucesos) > 4) {
                Suceso::where('suceso_id', '=', $sucesos[4]['suceso_id'])->delete();
            }

			if (($requestPost['C'] < 1)|| ($requestPost['C'] > 5)){
				$data['message'] = 'Campo C del request incorrecto';
				return $data;
			}
			
            if ($requestPost['C'] == 1) {

                $personas = Persona::where('persona_token_se', '=', $requestPost['T'])->get();
                if(count($personas) > 0) {
                    $this->registrarPosicion([
                        'fecha_evento'          => $fechaEvento,
                        'suceso_notificacion'   => 'Actualizando coordenadas',
                        'suceso_gps_latitud'    => $requestPost['A'],
                        'suceso_gps_longitud'   => $requestPost['O'],
                        'persona_token_se'      => $requestPost['T'],
                        'mensaje_id'            => $requestPost['C']
                    ]);
                    
                    $data['message'] = 'Suceso actualizado';
                    $data['success'] = true;
                } else {
                    $data['message'] = 'No existe token SE';
                }

                return $data;
            }

            $usuariosAbuelo = $this->consultarUsuariosAbuelo($requestPost['T']);

            if (count($usuariosAbuelo) > 0) {

				//se realiza la notificacion del evento al celular de cada usuario que se encuentra
				//asignado a dicho abuelo
                foreach ($usuariosAbuelo as &$usuarioAbuelo) {

                    $result = $this->notificar([
                        'mensaje_id'    =>  $requestPost['C'],
                        'titulo'        =>  $titulo,
                        'mensaje'       =>  $mensaje,
                        'usuario'       =>  $usuarioAbuelo,
                        'fecha_evento'	=>  $fechaEvento
                    ]);

                    if (empty($result)) {
                        $data['message'] = 'No se pudo enviar la notificacion a los contactos asociados';
                        $data['success'] = false;
                    } else {
                        $personas = Persona::where('persona_token_se', '=', $requestPost['T'])->get();
                        if(count($personas) > 0) {
                            $this->registrarPosicion([
                                'fecha_evento'          => $fechaEvento,
                                'suceso_notificacion'   => $result,
                                'suceso_gps_latitud'    => $requestPost['A'],
                                'suceso_gps_longitud'   => $requestPost['O'],
                                'persona_token_se'      => $requestPost['T'],
                                'mensaje_id'            => $requestPost['C']
                            ]);
                            
                            $data['success'] = true;
                            $data['message'] = $titulo;
                        } else {
                            $data['message'] = 'No existe token SE';
                        }
                    }
                }

            } else {
                $data['message'] = 'No hay usuarios a quien enviar la notificación';
            }
        } else {
			$data['T']=$requestPost['T'];
            $data['message'] = 'No se recibio ningun token';
        }

        return $data;
    }

	
    public function enviarNotificacionFit(Request $requestPost) {
		$result=null;
        $data['success'] = false;
		$request='';
			
		// Subir a una base de datos real con 2 tablas.
        $tiny_db[0][1]['tipo'] = '0001';
        $tiny_db[0][1]['mac' ] = "00:21:6a:b0:2f:fa";
        $tiny_db[0][1]['md5' ] = "10e6b6819b4bb2c9c453f8b72c93c305";
        $tiny_db[0][2]['tipo'] = '0002';
        $tiny_db[0][2]['mac' ] = "00:21:6a:b0:2f:fa";
        $tiny_db[0][2]['md5' ] = "1020cf232aa3004663b45bc6d70715ba";
        $tiny_db[0][3]['tipo'] = '0003';
        $tiny_db[0][3]['mac' ] = "00:21:6a:b0:2f:fa";
        $tiny_db[0][3]['md5' ] = "10d97d832b607c92fdb2a6188b24dcc7";
        $tiny_db[0][4]['tipo'] = '0004';
        $tiny_db[0][4]['mac' ] = "00:21:6a:b0:2f:fa";
        $tiny_db[0][4]['md5' ] = "10d7dca932454be077bb7783cee6f739";
        $tiny_db[0][5]['tipo'] = '0005';
        $tiny_db[0][5]['mac' ] = "00:21:6a:b0:2f:fa";
        $tiny_db[0][5]['md5' ] = "1092a2cbc743745f1f6471d84464af83";

        $tiny_db[1][1]['tipo'] = '0001';
        $tiny_db[1][1]['mac' ] = "98:4F:EE:01:0E:32";
        $tiny_db[1][1]['md5' ] = "91e6b6819b4bb2c9c453f8b72c93c305";
        $tiny_db[1][2]['tipo'] = '0002';
        $tiny_db[1][2]['mac' ] = "98:4F:EE:01:0E:32";
        $tiny_db[1][2]['md5' ] = "7720cf232aa3004663b45bc6d70715ba";
        $tiny_db[1][3]['tipo'] = '0003';
        $tiny_db[1][3]['mac' ] = "98:4F:EE:01:0E:32";
        $tiny_db[1][3]['md5' ] = "90d97d832b607c92fdb2a6188b24dcc7";
        $tiny_db[1][4]['tipo'] = '0004';
        $tiny_db[1][4]['mac' ] = "98:4F:EE:01:0E:32";
        $tiny_db[1][4]['md5' ] = "f7d7dca932454be077bb7783cee6f739";
        $tiny_db[1][5]['tipo'] = '0005';
        $tiny_db[1][5]['mac' ] = "98:4F:EE:01:0E:32";
        $tiny_db[1][5]['md5' ] = "0c92a2cbc743745f1f6471d84464af83";

        $msg = $requestPost->getContent();
        
        if(empty($msg) )
        {
            $data['message'] = 'Error: mensaje vacio.';
            return $data;
		}
		
		$md5      = substr($msg, 0, 32);

		// Busco codigo en tiny_db:
		$id       = 0;
		$tipo_msg = 0;
		$mac      = 0;
		$coordenadasHex='';
		$coordenadasStr='';
		
		for( $i = 0;$i < 2;$i++)
		{
			for( $j = 1;$j < 6;$j++)
			{
				if( $tiny_db[$i][$j]['md5'] == $md5 )
				{
					$id       = $i;
					$tipo_msg = $j;
					$mac	  = $tiny_db[$i][$j]['mac'];
				}
			}
		}

		// Verifio MD5 encontrado.
		if( $tipo_msg == 0 )
		{
            $data['message'] = 'Error: MD5 no registrado en servidor (tiny_db).';
            return $data;
		}
		
		//se obtienen las coordenadas dels GPS
		$coordenadasHex=substr($msg, 32, 32);
		$coordenadasStr=ManagerEncrypt::decryptXorHex($mac,$coordenadasHex);
		
		$latitud  = substr($coordenadasStr, 0, 8);
		$longitud = substr($coordenadasStr, 8, 16);

		
		$request['T']  = $mac;
		$request['C']  = $tipo_msg;
		$request['A'] =  $latitud;
		$request['O']  = $longitud;
		       
		
        $data=$this->enviarNotificacion($request);
        return $data;
    }

}
