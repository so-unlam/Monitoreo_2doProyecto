<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Usuario;
use App\Suceso;
use App\UsuarioPersona;



class AbueloController extends Controller
{
    
	    
	 public function agregarAnciano() {
        if ( ! empty($_POST)) {
			$respuestaAbuelo=false;
			$respuestaAbueloUsuario=true;
			$respuestaAbuelo=$this->insertarPersona($_POST);
			$respuestaAbueloUsuario=$this->insertarUsuarioPersona($_POST);
			
			
			if($respuestaAbuelo==true && $respuestaAbueloUsuario==true){
				$data['success'] = true;
				$data['message'] = 'Se registro un nuevo abuelo y se lo asocio a este usuario';
				return $data;
			}
			
			
			if($respuestaAbuelo==false && $respuestaAbueloUsuario==false){
			
				$data['success'] = false;
				$data['message'] = 'Abuelo existente, ya se encuentra asociado a este usuario';
				return $data;
			}
			
			
			if($respuestaAbuelo==false && $respuestaAbueloUsuario==true){
				$data['success'] = true;
				$data['message'] = 'abuelo ya existente, se lo asocio a este usuario';
				return $data;
			}
			
			$data['success'] = false;
			$data['message'] = 'Error al agregar abuelo en la base de datos';
			return $data;
		}    
		$data['success'] = false;		
		$data['message'] = 'No se recibieron datos para agregar un abuelo';
		return $data;
	
    }

	
	public function insertarPersona($POST){
		$tablaPersona = $this->getAbuelo($POST['token']);
		$cant=count($tablaPersona);
		
		if($cant==0)
		{
			Persona::insert([
				'persona_nombre'    => $POST['nombre'],
				'persona_direccion' => $POST['direccion'],
				'persona_telefono'  => $POST['telefono'],
				'persona_fecha_nac' => $POST['fecha_nac'],
				'persona_token_se'  => $POST['token']
			]);
			return true;
		}
		return false;
	}
	
	public function insertarUsuarioPersona($POST){
		$tablaUsuarioPersona = $this->getUsuarioPersona($POST['usuario_id'],$POST['token']);
		$cant=count($tablaUsuarioPersona);
		
		if($cant==0)
		{	UsuarioPersona::insert([
							'usuario_id'        => $POST['usuario_id'],
							'persona_token_se'  => $POST['token']
						]);
			return true;
		}
		return false;
	}
	
	public function eliminarAbuelo() {
        $data['success'] = false;
        if ( ! empty($_POST)) {

			$tablaUsuarioPersona = $this->getUsuariosPersona($_POST['usuario_id'],$_POST['token']);
			$cant=count($tablaUsuarioPersona);
				
			$data=$this->eliminarDatosAbuelo($cant);
			
        } else {
            $data['message'] = 'No se recibio correctamente el POST';
        }

        return $data;
    }

	public function eliminarDatosAbuelo($cantUsuariosAbuelos){
		$data['success'] = false;
		
		if($cantUsuariosAbuelos>0){
	
			UsuarioPersona::where('usuario_id', '=', $_POST['usuario_id'])
						->where('persona_token_se', '=', $_POST['token'])
						->delete();
			
			if($cantUsuariosAbuelos==1){
				 Persona::where('persona_token_se', '=', $_POST['token'])
				 ->delete();
				
				$data['message'] = 'Atencion los datos del abuelo fueron eliminados del sistema';
			}
			else
			{
				$data['message'] = 'Usuario desasignado del abuelo correctamente';
			}
			$data['success'] = true;
		}
		else{	     
			$data['message'] = 'No se pudo eliminar el abuelo, datos incorrectos';
		}
		return $data;
	}
	
    public function monitorearSE() {
		$data['success'] = false;
		$records = 1;
		if (!empty($_POST['mensaje_id']) && $_POST['mensaje_id'] == 1) {
			$records = 5;
		}
		if (! empty($_POST)) {
			$events = $this->getSuceso($_POST['token']);
			if (count($events) > 0 ) {
				$events = Suceso::where('persona_token_se', '=', $_POST['token'])
					->where('mensaje_id', '=', $_POST['mensaje_id'],'AND')
					->orderBy('suceso_id', 'Desc')
					->take($records)    
					->get();

				$data['events'] = $events;
			} else {
				$data['message'] = 'No hay coordenadas ingresadas para este token.';
				$data['events'] = [];
			}
			$data['success'] = true;
		} else {
			$data['message'] = 'No se ha ingresado el parametro correcto.';
		}
			
		return $data;
    }

    public function mostrarAnciano() {

        //Esto puede no ser util, ya que la misma informacion se va a encontrar en la lista general
        $data['success'] = false;
        if ( ! empty($_GET) && ! empty($_GET['usuario_id']) && ! empty($_GET['token'])) {

            $result = Persona::where('usuario_id', '=', $_POST['usuario_id'])
                ->where('persona_token_se', '=', $_GET['token'])->get();

            if ( count($result) > 0) {
                $data['persona'] = $result[0];
                $data['success'] = true;
            }
        }

        return $data;
    }

    public function mostrarAbuelos() {

        $data['success'] = false;

        if ( ! empty($_GET)) {

            $result = Persona::join(
                'usuario_persona',
                'usuario_persona.persona_token_se',
                '=',
                'persona.persona_token_se'
                )
                ->where('usuario_persona.usuario_id', '=', $_GET['id'])
                ->get();

            $data['personas'] = $result;

            $data['success'] = true;
        }

        return $data;
    }
	
	
    private function getAbuelo($token_se) {
        if (!empty($token_se)) {
            $tablaPersona = Persona::where('persona_token_se', '=', $token_se)->get();
            return $tablaPersona;
        }

        return null;
    }
	
	private function getUsuarioPersona($usuario_id,$token_se) {
        if (!empty($token_se)) {
            $tablaUsuarioPersona = UsuarioPersona::where('usuario_id', '=', $usuario_id)
											->where('persona_token_se', '=', $token_se,'AND')
											->get();
            return $tablaUsuarioPersona;
        }
        return null;
    }

	
	private function getUsuariosPersona($usuario_id, $token_se) {
        if (!empty($token_se)) {
            $tablaUsuarioPersona = UsuarioPersona::where('persona_token_se', '=', $token_se)->get();
            return $tablaUsuarioPersona;
        }
        return null;
    }
	
	
	public function verificarExistenciaToken(){
		$data['success'] = false;
		$tablaPersona = $this->getAbuelo($_GET['token']);
		$cant=count($tablaPersona);
		
		if ( ! empty($_GET)){
		
			if($cant!=0)
			{
				$data['success'] = true;
				$data['personas'] = $tablaPersona;
				$data['message'] = "Token ya creado";
			}
			else
			{
				$data['message'] = "Token disponible";
			}
			return $data;
		}
		
		$data['message'] = "NO SE RECIBIO PARAMETROS GET";
		return $data;
		
	}

	private function getSuceso($token) {
        $tablaSuceso = Suceso::where('persona_token_se', '=', $token)
            ->orderBy('suceso_id', 'Desc')    
            ->get();
        return $tablaSuceso;
    }
}
