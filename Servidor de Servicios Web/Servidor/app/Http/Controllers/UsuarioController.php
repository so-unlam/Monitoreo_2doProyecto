<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Usuario;
use App\Http\Library\ManagerEncrypt;

class UsuarioController extends Controller
{
	//metodo para registrar usuarios sin usar json. Es como lo habian hecho raul y marcos
    public function registrarUsuarioNormal(Request $requestPost)
    {
		//Descencripto el request del POST
		$post=ManagerEncrypt::decryptRequest($requestPost);
        
		$data['success'] = false;
        if ( ! empty($post)) {

            $result = Usuario::where('usuario_mail', '=', $post['mail'])->get();

			//se registra al usuario en la BD
            if (count($result) == 0) {
                Usuario::insert([
                    'usuario_nombre'    => $post['nombre'],
					'cuenta_facebook'	=> '0',
                    'usuario_mail'      => $post['mail'],
                    'usuario_clave'     => $post['clave'],
                    'usuario_direccion' => $post['direccion'],
                    'usuario_telefono'  => $post['telefono'],
                    'usuario_imagen'    => '',
                    'usuario_token_gcm' => $post['token']
                ]);
				
				//se inicia la sesion normalmente para que quede registrada
				$requestPost['mail']=$post['mail'];
				$requestPost['token']=$post['token'];
				$requestPost['clave']=$post['clave'];
				
				$data=$this->registrarInicioSesionNormal($requestPost);
            }
			else{
				$data['message']="El mail del usuario ya se encuentra registrado en el sistema";
			}
        }
		else{
			$data['message']="ERROR no se recibio POST para registrar el usuario";
		}

		//se encripta el mensaje response del POST
		$respuesta=ManagerEncrypt::encryptResponse($data);
        return $respuesta;
    }

	public function iniciarSesionNormal(Request $requestPost)
    {
		//Descencripto el request del POST
		$post=ManagerEncrypt::decryptRequest($requestPost);
		
		$data=$this->registrarInicioSesionNormal($post);
		
		//se encripta el mensaje response del POST
		$respuesta=ManagerEncrypt::encryptResponse($data);
		return $respuesta;
    }

	private function registrarInicioSesionNormal($post)
	{
		$data['success'] = false;
		
        if ( ! empty($post) && ! empty($post['mail'])) {

			$result = Usuario::where('usuario_mail', '=', $post['mail'])->get();
            if (count($result) > 0 && strcmp($result[0]['usuario_clave'], $post['clave']) == 0) {
                $data['message'] = 'bienvenido';

                Usuario::where('usuario_mail', '=', $post['mail'])
                    ->update(['usuario_token_gcm'=> $post['token']]);

                $data['usuario'] = $result[0];
                $data['success'] = true;
            } else {
                $data['message'] = 'La clave no coincide con el mail ingresado';
            }
		}
		return $data;
	}
    public function recuperarClave() {
        $data['success'] = false;

        if ( ! empty($_POST) && ! empty($_POST['mail'])) {

            $result = Usuario::where('usuario_mail', '=', $_POST['mail'])->get();
            ///Enviar mail de restauracion de contraseña

            if ( count($result) > 0) {

                $data['usuario'] = $result[0];
                $data['message'] = 'Se acaba de enviar un mail de recuperación';
                $data['success'] = true;
            } else {
                $data['message'] = 'El usuario no existe en la base de datos';
            }
        }

        return $data;
    }

    public function actualizarDatos() {
        $data['success'] = false;

        if ( ! empty($_POST) && ! empty($_POST['tipo'])) {

            if (strcmp($_POST['tipo'], 'usuario') == 0) {
                Usuario::where('usuario_mail', '=', $_POST['mail'])
                    ->update([
                        'usuario_nombre'    =>   $_POST['nombre'],
                        'usuario_direccion' =>   $_POST['direccion'],
                        'usuario_telefono'  =>   $_POST['telefono'],
                        'usuario_clave'     =>   $_POST['clave'],
                        'usuario_imagen'    =>   $_POST['imagen'],
						'usuario_mail'      =>   $_POST['mail_nuevo']
                    ]);
					
					$_POST['mail']=$_POST['mail_nuevo'];
     				
					//se inicia la sesion normalmente para que quede registrada
					$requestPost['mail']=$_POST['mail'];
					$requestPost['token']=$_POST['token'];
					$requestPost['clave']=$_POST['clave'];
					
					$data=$this->registrarInicioSesionNormal($requestPost);

            } else if (strcmp($_POST['tipo'], 'abuelo') == 0) {
                Persona::where('persona_token_se', '=', $_POST['token'])
                       ->update([
                        'persona_nombre'    =>  $_POST['nombre'],
                        'persona_direccion' =>  $_POST['direccion'],
                        'persona_fecha_nac' =>  $_POST['fecha'],
                        'persona_telefono'  =>  $_POST['telefono']
                       ]);
		            

            }
			
			$data['message'] = 'Se realizo la actualizacion correctamente';
            $data['success'] = true;
        } else {
            $data['message'] = 'El ' . $_POST['tipo'] . ' no esta registrado';
        }

        return $data;
    }
	
	
	public function validarSesionFacebook(Request $requestPost)
    {
		$post=ManagerEncrypt::decryptRequest($requestPost);

		$inicio_sesion_face_realizado = false;
		$resp_registrar_usuario=false;
		$cant_vuelta_while=0;
		$data['success'] = false;
		$respuesta='';
		        
        if (empty($post) && empty($post['mail']) && empty($post['token']))
		{
			$data['message']='faltan parametros en POST';
			
			//se encripta el mensaje response del POST
			$respuesta=ManagerEncrypt::encryptResponse($data);
			return $respuesta;
		}
	
		while($inicio_sesion_face_realizado==false)
		{
            $result = Usuario::where('usuario_mail', '=', $post['mail'])->get();
			
			//se pregunta si se encuentra registrado con ese mail con cuenta normal o facebook
            if (count($result) > 0) 
			{
				//si se encuentra registrado con facebook, entonces se inicia la sesion
				if($result[0]['cuenta_facebook']=='1')
				{
					$data=$this->iniciarSesionFacebook($post);
					$data['usuario'] = $result[0];
					$data['success'] = true;
				}
				else
				{
					$data['message'] = 'El mail ya se encuentra registrado en la BD sin facebook';
				}
				$inicio_sesion_face_realizado=true;
            } 
			else 
			{	
				//si no se encuentra en registrado con ese mail, entonces lo registro en la BD con facebook
				$resp_registrar_usuario=$this->registrarUsuarioFacebook($post);
				
				if($resp_registrar_usuario!=true)
				{
					$data['message']=$resp_registrar_usuario;
					$inicio_sesion_face_realizado=true;
				}
					
	        }
        }
		
		//se encripta el mensaje response del POST
		$respuesta=ManagerEncrypt::encryptResponse($data);
        return $respuesta;
    }

	private function registrarUsuarioFacebook($post)
	{
		
		$resp=false;
		
		try
		{
			Usuario::insert([
				'usuario_nombre'    => $post['nombre'],
				'usuario_mail'      => $post['mail'],
				'usuario_token_gcm' => $post['token'],
				'cuenta_facebook'	=> '1',
				'usuario_clave'     => '',
				'usuario_direccion' => '',
				'usuario_telefono'  => '',
				'usuario_imagen'    => ''
				
			]);
			
			$resp=true;
		}
		catch(Exception $e)
		{
			$resp=$e->getMessage();
		}
		
		return $resp;
	}
	
	private function iniciarSesionFacebook($post)
	{
		$data['message'] = 'bienvenido';

		Usuario::where('usuario_mail', '=', $post['mail'])
				->update(['usuario_token_gcm'=> $post['token']]);

		return $data;
	}
}
