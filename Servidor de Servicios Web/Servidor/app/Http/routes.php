<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//route de la aplicacion

Route::post('mobile/register', 'UsuarioController@registrarUsuarioNormal');

Route::post('mobile/login', 'UsuarioController@iniciarSesionNormal');

Route::post('mobile/login_facebook', 'UsuarioController@validarSesionFacebook');

Route::post('mobile/add_elder', 'AbueloController@agregarAnciano');

Route::post('mobile/delete_elder', 'AbueloController@eliminarAbuelo');

Route::post('mobile/watch_se', 'AbueloController@monitorearSE');

Route::post('mobile/request_test', 'NotificacionController@requestTest');

Route::post('mobile/send_notication_test', 'NotificacionController@enviarPosicionTest');

Route::post('mobile/send_notication_nuevo', 'NotificacionController@enviarNotificacionNuevo');

Route::post('mobile/xor_test', 'NotificacionController@xorTest');

Route::post('mobile/send_notication_fit', 'NotificacionController@enviarNotificacionFit');

Route::post('mobile/update', 'UsuarioController@actualizarDatos');

Route::post('mobile/reset_pass', 'UsuarioController@recuperarClave');

Route::get('mobile/list_all', 'AbueloController@mostrarAbuelos');

Route::get('mobile/elder_datail', 'AbueloController@mostrarAnciano');

Route::get('mobile/verify_token', 'AbueloController@verificarExistenciaToken');
