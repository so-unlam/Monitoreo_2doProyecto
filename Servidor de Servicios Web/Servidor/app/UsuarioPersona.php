<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioPersona extends Model
{
    //
    protected $table = 'usuario_persona';
    protected $fillable = [
        'usuario_id',
        'persona_token_se'
    ];
}
