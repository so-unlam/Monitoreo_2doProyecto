<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    protected $table = 'persona';
    protected $fillable = [
        'persona_nombre',
        'persona_direccion',
        'persona_fecha_nac',
        'persona_telefono',
        'persona_token_se'
    ];

    public function usuarios()
    {
        return $this->hasMany(Usuario::class);
    }

    public function incidentes()
    {
        return $this->hasMany(Suceso::class);
    }
}
