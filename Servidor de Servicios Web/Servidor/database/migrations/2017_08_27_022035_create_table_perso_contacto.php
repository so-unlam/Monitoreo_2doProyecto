<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersoContacto extends Migration
{
    /**
     * Esta migracion genera la tabla perso_contacto en la db definida
     * en el archivo database.php
     *
     * @return void
     */
    public function up()
    {
        //se crea la tabla perso_contacto con claves foraneas
        Schema::create('perso_contacto', function (Blueprint $table) {
            $table->unsignedInteger('persona_dni')->references('persona_dni')->on('persona');
            $table->unsignedInteger('usuario_id')->references('usuario_id')->on('persona');
            $table->unsignedInteger('contacto_id')->references('contacto_id')->on('contacto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perso_contacto');
    }
}
