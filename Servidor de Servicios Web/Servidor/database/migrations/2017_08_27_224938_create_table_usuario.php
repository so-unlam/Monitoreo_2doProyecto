<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('usuario_id');
            $table->string('usuario_nombre')->nullable();
            $table->string('usuario_mail')->nullable(false);
            $table->string('usuario_direccion')->nullable();
            $table->string('usuario_telefono')->nullable();
            $table->string('usuario_clave')->nullable(false);
            $table->string('usuario_imagen')->nullable();
            $table->string('usuario_token_gcm')->nullable();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
