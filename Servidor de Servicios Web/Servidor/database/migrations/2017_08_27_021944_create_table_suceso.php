<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSuceso extends Migration
{
    /**
     * Esta migracion se encarga de generar la tabla suceso en la db definida
     * en el archivo database.php
     *
     * @return void
     */
    public function up()
    {
        //creamos la tabla suceso y creamos la clave foranea de persona
        Schema::create('suceso', function (Blueprint $table) {
            $table->increments('suceso_id');
            $table->string('suceso_fecha');
            $table->string('suceso_tipo')->nullable();
            $table->string('suceso_descripcion')->nullable();
            $table->string('suceso_notificacion')->nullable();
            $table->string('suceso_gps_latitud')->nullable();
            $table->string('suceso_gps_longitud')->nullable();
            $table->unsignedInteger('persona_dni')->refences('persona_dni')->on('persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suceso');
    }
}
