<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContacto extends Migration
{
    /**
     * Esta migracion se encarga de generar la tabla contacto en la DB definida
     * en el archivo database.php
     * @return void
     */
    public function up()
    {
        //creamos la tabla contacto y definimos algunos campos nulos
        Schema::create('contacto', function (Blueprint $table) {
            $table->increments('contacto_id');
            $table->string('contacto_nombre')->nullable();
            $table->string('contacto_mail')->nullable();
            $table->string('contacto_tipo')->nullable();
            $table->string('contacto_direccion')->nullable();
            $table->string('contacto_telefono')->nullable();
            $table->string('contacto_imagen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacto');
    }
}
