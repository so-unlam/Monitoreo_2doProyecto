<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersona extends Migration
{
    /**
     * Esta migracion se encarga de generar la tabla Persona en la db definida
     * en el archivo database.php
     *
     * @return void
     */
    public function up()
    {
        //creamos la tabla persona con atributos que pueden ser nulos
        Schema::create('persona', function (Blueprint $table) {
            $table->increments('persona_dni');
            $table->string('persona_nombre')->nullable(false);
            $table->string('persona_mail')->nullable();
            $table->string('persona_direccion')->nullable();
            $table->string('persona_telefono')->nullable();
            $table->string('persona_fecha_nac')->nullable();
            $table->string('persona_imagen')->nullable();
            $table->string('persona_token_se')->nullable();
            $table->timestamps();
            $table->unsignedInteger('usuario_id')
                ->references('usuario_id')
                ->on('usuario')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona');
    }
}
