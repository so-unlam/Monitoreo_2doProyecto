/*
* @file Controlador.h
 * @brief Implementacion del Controlador GSM
 * @date: Oct 29, 2018
 * @author: Esteban Carnuccio.
*/


#include "App/Drivers/Controlador_GSM.h"


extern Serial * usart_debug;

/***************************************************************************************************/
Controlador_GSM::Controlador_GSM()
{
	this->serial_GSM=NULL;

	this->num_comando_at=0;
	this->estado_comando_at=0;


	this->estado_op=Estado_Op_Gsm::INACTIVO;
	this->estado_senal=Estado_Senal_Gsm::ESPERANDO_RESPUESTA;

	this->tiempo_inicial_respuesta_at=0;
	this->tiempo_espera_respuesta_at =0;
	this->tiempo_inicial_ejecutar_at=HAL_GetTick();
	this->tiempo_espera_ejecutar_at=0;
	this->tiempo_total_ejecucion=0;
	this->tiempo_total_inicial_ejecucion=HAL_GetTick();
	this->tiempo_i_prender_led_gsm=HAL_GetTick();
	this->tiempo_a_prender_led_gsm=0;

	this->tiempo_en_estado=0;

	this->led_gsm_pin=0;
	this->led_gsm_puerto=NULL;

}

/***************************************************************************************************/
Controlador_GSM::~Controlador_GSM()
{

}

/***************************************************************************************************/
bool Controlador_GSM::controlador_init(Serial * SerialGSM)
{
	bool resp=true;

	this->serial_GSM=SerialGSM;

	//El led del gsm esta siempre prendido al iniciar
	//la aplicacion y parpadee cuando se empiezan a nviar comandos AT y no tenga se�al
	HAL_GPIO_WritePin(led_gsm_puerto,led_gsm_pin,GPIO_PIN_SET);

	return resp;
}

/***************************************************************************************************/
void Controlador_GSM::configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin)
{
	this->led_gsm_pin=led_pin;
	this->led_gsm_puerto=led_puerto;
}

/***************************************************************************************************/
bool Controlador_GSM::encolar_notificacion_gsm(Peticion_Operacion_Gsm *peticion)
{
	bool resp=false;

	//se agrega la peticion de operacion de GSM solicitida buffer
	//en el Buffer circular de peticiones
	resp=cola_circular_peticiones.fifo_push(peticion);

	if( resp == false )
	{
		LOG("**BUFFER CIRCULAR DE PETICION GSM LLENO!!\r\n");
	}
	else
	{
		LOG("**SE ENCOLO PETICION!!\r\n");
	}

	return resp;
}

/***************************************************************************************************/
bool Controlador_GSM::ejecutar_op_encoladas_gsm()
{
	bool resp=false;
	Estado_Senal_Gsm senal=Estado_Senal_Gsm::ESPERANDO_RESPUESTA;

	switch (estado_op)
	{
		case Estado_Op_Gsm::INACTIVO:
			//ACA DESENCOLARIA LAS PETICIONES DE OPERACION PARA EL GSM
			if(desencolar_notificacion_gsm(&peticion_activa)==true)
				proximo_estado(Estado_Op_Gsm::COMPROBAR_SENAL);

			else
				resp=true;//esto quiere decir que ya no hay mas peticiones pendientes
						  //que se encuentran encoladas
			break;

		case Estado_Op_Gsm::COMPROBAR_SENAL:

			senal=comprobar_funcinamiento_SIM800L();

			if(senal==Estado_Senal_Gsm::GSM_SIN_SENAL)
			{
				proximo_estado(Estado_Op_Gsm::NOTIFICAR_SIN_SENAL_GSM);
			}
			else if(senal==Estado_Senal_Gsm::GSM_CON_SENAL)
			{
				//al tener se�al el GSM se indica por medio del encencido del led del GSM
				HAL_GPIO_WritePin(led_gsm_puerto,led_gsm_pin,GPIO_PIN_SET);

				proximo_estado(Estado_Op_Gsm::REALIZAR_OPERACION);
			}
			break;

		case Estado_Op_Gsm::REALIZAR_OPERACION:
			if(determinar_tipo_op_gsm()==true)
			{
				proximo_estado(Estado_Op_Gsm::INACTIVO);
				//resp=true;//Esto es temporal hasta que no se haga la cola de peticiones
			}
			else
			{
				proximo_estado(Estado_Op_Gsm::REALIZAR_OPERACION,Estado_Op_Gsm::REINICIAR_OPERACION,TIMEOUT_OP_GSM);
			}
			break;

		case(Estado_Op_Gsm::NOTIFICAR_SIN_SENAL_GSM):
			//SI NO HAY SE�AL DE ACELEROMETRO
			tiempo_a_prender_led_gsm=HAL_GetTick()-tiempo_i_prender_led_gsm;

			if(tiempo_a_prender_led_gsm>TIEMPO_ESPERA_LED_GSM)
			{
				//el gsm no tiene se�al entonces parpadea el led del GSM
 				HAL_GPIO_TogglePin(led_gsm_puerto,led_gsm_pin);

				LOG("==GSM SIN SENAL==\r\n");

				tiempo_i_prender_led_gsm=HAL_GetTick();
			}
			proximo_estado(Estado_Op_Gsm::COMPROBAR_SENAL);
			break;

		case(Estado_Op_Gsm::REINICIAR_OPERACION):
				this->num_comando_at=0;
				this->estado_comando_at=0;

				this->tiempo_inicial_respuesta_at=0;
				this->tiempo_espera_respuesta_at =0;
				this->tiempo_inicial_ejecutar_at=HAL_GetTick();
				this->tiempo_espera_ejecutar_at=0;
				this->tiempo_total_ejecucion=0;
				this->tiempo_total_inicial_ejecucion=HAL_GetTick();
				this->tiempo_i_prender_led_gsm=HAL_GetTick();
				this->tiempo_a_prender_led_gsm=0;

				proximo_estado(Estado_Op_Gsm::COMPROBAR_SENAL);

				LOG("*********TIMEOUT OPERACION GSM. REINTENTANDO...\r\n");
				break;
		default:
			break;
	}

	return resp;
}


/***************************************************************************************************/
bool Controlador_GSM::determinar_tipo_op_gsm()
{
	bool resp=false;

	switch (peticion_activa.tipo_operacion)
	{
		case Tipos_Comandos::INICIAR_LLAMADA:
			if(realizar_llamada(peticion_activa.llamada.num_tel)==true)
				resp=true;
			break;

		case Tipos_Comandos::FINALIZAR_LLAMADA:
			if(finalizar_llamada()==true)
				resp=true;
			break;

		case Tipos_Comandos::ENVIAR_SMS:
			if(enviar_sms(peticion_activa.sms.num_tel,peticion_activa.sms.msj)==true)
				resp=true;

			break;

		case Tipos_Comandos::REALIZAR_POST:
			if(realizar_POST(peticion_activa.post.msj_json_post,
							 sizeof(peticion_activa.post.msj_json_post),
							 peticion_activa.post.resp_json_post,
							 sizeof(peticion_activa.post.resp_json_post))==true)
			{
				resp=true;
				LOG("POST FINALIZADO!!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
			}
			break;
		default:
			break;
	}
	return resp;
}

/***************************************************************************************************/
bool Controlador_GSM::desencolar_notificacion_gsm(Peticion_Operacion_Gsm * peticion)
{
	bool resp=false;

	//se desconla un peticion de operacion
	//solicitada que se encuentra pendiente de ejecucion
	resp=cola_circular_peticiones.fifo_pop(peticion);

	return resp;
}
/***************************************************************************************************/


Controlador_GSM::Estado_Senal_Gsm Controlador_GSM::comprobar_funcinamiento_SIM800L()
{
	volatile char * pos;

	char comando[100]={0};
	size_t tam_respuesta=0;

	/* Asi es la respuesta de AT+COPS?
	 *
	 * 1�Linea= \r\n
	 * 2�Linea= +COPS: 0....\r\n
	 * 3�Linea= OK\r\n
	 *
	 * La 2�Linea es la que nos dice si el GSM tiene o no se�al.
	 * Si nos devuelve +COPS: 0\r\n entonces quiere decir que no hay se�al.
	 */

	snprintf(comando,sizeof(comando),"AT+COPS?\r\n");
	tam_respuesta=this->ejecutar_comando_at(comando, CHARACTER_CUT_NEW_LINE);

	if(tam_respuesta==0)
		return Estado_Senal_Gsm::ESPERANDO_RESPUESTA;

	if(strcmp((char *)this->respuesta,"OK\r\n")==0)
	{
		this->estado_comando_at=0;
		//si detecto OK retorno el utlimo valor almacenado
		return estado_senal;
	}

	pos=strstr((char*)this->respuesta,"+COPS:");


	if (pos!=NULL)
	{
		pos+=strlen("+COPS:");
		if(strcmp((const char *)pos," 0\r\n")!=0)
		{
			estado_senal=Estado_Senal_Gsm::GSM_CON_SENAL;
		}
		else
		{
			estado_senal=Estado_Senal_Gsm::GSM_SIN_SENAL;

		}
	}


	return Estado_Senal_Gsm::ESPERANDO_RESPUESTA;

}


/***************************************************************************************************/
bool Controlador_GSM::realizar_llamada(byte_t * num_tel)
{
	char comando[30]={0};
	bool operacion_completa=false;
	bool resp_comando_at=false;

	switch(this->num_comando_at)
	{
		case 0:
			snprintf(comando,sizeof(comando),"AT+CMGF=0\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 1:
			snprintf(comando,sizeof(comando),"ATD+%s;\r\n",num_tel);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			if(resp_comando_at==true)
			{
				this->num_comando_at=0;
				operacion_completa=true;
			}
			break;
		default:
			break;

	}

	return operacion_completa;
}

/***************************************************************************************************/
bool Controlador_GSM::finalizar_llamada()
{

	char comando[50]={0};
	bool operacion_completa=false;
	bool resp_comando_at=false;

	switch(this->num_comando_at)
	{
		case 0:
			snprintf(comando,sizeof(comando),"ATH\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			if(resp_comando_at==true)
			{
				this->num_comando_at=0;
				operacion_completa=true;
			}


			break;
		default:
			break;
	}
	return operacion_completa;
}

/***************************************************************************************************/
bool Controlador_GSM::enviar_sms(byte_t * num_tel,byte_t *mensaje)
{
	char comando[50]={0};
	bool operacion_completa=false;
	bool resp_comando_at=false;
	size_t tam_respuesta=0;

	switch(this->num_comando_at)
	{
		case 0:
			tiempo_total_inicial_ejecucion=HAL_GetTick();
			snprintf(comando,sizeof(comando),"AT+CMGF=1\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 1:
			snprintf(comando,sizeof(comando),"AT+CMGS=\"%s\"\r\n",num_tel);
			tam_respuesta=this->ejecutar_comando_at(comando, '>');

			if(tam_respuesta==0)
				break;

			LOG((char *)this->respuesta);
			if(this->respuesta[tam_respuesta-1]=='>')
			{
				this->num_comando_at++; //paso al siguiente estado
				this->estado_comando_at=0;
				LOG(comando);
				memset(this->respuesta,0,sizeof(this->respuesta));

			}

			break;
		case 2:
			snprintf(comando,sizeof(comando),"%s\r\n %c at\r\n",mensaje,CTRL_Z);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			if(resp_comando_at==true)
			{
				operacion_completa=true;
				this->num_comando_at=0;

				tiempo_total_ejecucion=HAL_GetTick()-tiempo_total_inicial_ejecucion;
				LOG("Mensaje SMS Enviado: %s\r\n",comando);
				LOG("tiempo de envio SMS:%u\r\n",tiempo_total_ejecucion);
			}
			break;

		default:
			break;
	}
	return operacion_completa;
}
/***************************************************************************************************/
bool Controlador_GSM::realizar_POST(byte_t * msj_json_enviar,size_t tam_msj_enviar,byte_t * msj_json_resp,size_t tam_msj_resp)
{
	char comando[100]={0};
	bool operacion_completa=false;
	bool resp_comando_at=false;
	size_t tam_respuesta=0;


	switch(this->num_comando_at)
	{

		case 0:
			tiempo_total_inicial_ejecucion=HAL_GetTick();

			snprintf(comando,sizeof(comando),"ATE0\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 1:
			snprintf(comando,sizeof(comando),"AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 2:
			snprintf(comando,sizeof(comando),"AT+SAPBR=3,1,\"APN\",\"%s\"\r\n",APN_PERSONAL);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 3:
			snprintf(comando,sizeof(comando),"AT+SAPBR=3,1,\"USER\",\"%s\"\r\n",USER_PERSONAL);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 4:
			snprintf(comando,sizeof(comando),"AT+SAPBR=3,1,\"PWD\",\"%s\"\r\n",PWD_PERSONAL);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 5:
			snprintf(comando,sizeof(comando),"AT+SAPBR=1,1\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 6:
			snprintf(comando,sizeof(comando),"AT+HTTPINIT\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 7:
			snprintf(comando,sizeof(comando),"AT+HTTPPARA=\"CID\",1\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 8:
			snprintf(comando,sizeof(comando),"AT+HTTPPARA=\"URL\",\"%s\"\r\n",URL_SEND_NOTIFCATION);
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 9:
			snprintf(comando,sizeof(comando),"AT+HTTPPARA=\"REDIR\",1\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 10:
			//snprintf(comando,sizeof(comando),"AT+HTTPPARA=\"CONTENT\",\"application/json\"\r\n");
			snprintf(comando,sizeof(comando),"AT+HTTPPARA=\"CONTENT\",\"text/plain\"\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 11:
			snprintf(comando,sizeof(comando),"AT+HTTPSSL=0\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 12:
			snprintf(comando,sizeof(comando),"AT+HTTPDATA=%u,%d\r\n",tam_msj_enviar,TIEMPO_ESPERA_HTTPDATA);
			tam_respuesta=this->ejecutar_comando_at(comando, CHARACTER_CUT_NEW_LINE);

			if(tam_respuesta==0)
			break;

			if(strcmp((char *)this->respuesta,"DOWNLOAD\r\n")==0)
			{

				LOG(comando);
				LOG("Respuesta:%s\r\n",(char *)this->respuesta);
				memset(this->respuesta,0,sizeof(this->respuesta));

				//envio el JSON
				this->serial_GSM->send((const char *)msj_json_enviar);
				LOG("Msj Json:%s\r\n",(char *)msj_json_enviar);

				//memset(this->respuesta,0,sizeof(this->respuesta));
				break;
			}
			if(strstr((char *)this->respuesta,"OK\r\n")!=NULL)
			{
				this->num_comando_at++; //paso al siguiente estado
				this->estado_comando_at=0;
				LOG("HTTPACTION ENVIADO OK\r\n");
				memset(this->respuesta,0,sizeof(this->respuesta));
				break;
			}

			break;

		case 13:
			snprintf(comando,sizeof(comando),"AT+HTTPACTION=1\r\n");
			tam_respuesta=this->ejecutar_comando_at(comando, CHARACTER_CUT_NEW_LINE);

			if(tam_respuesta==0)
				break;

			if(strcmp((char *)this->respuesta,"OK\r\n")==0)
			{
				LOG(comando);
				LOG("Respuesta:%s\r\n",(char *)this->respuesta);

				memset(this->respuesta,0,sizeof(this->respuesta));
				break;
			}
			if(strstr((char *)this->respuesta,"200")!=NULL)
			{
				this->num_comando_at++; //paso al siguiente estado
				this->estado_comando_at=0;
				LOG("HTTPACTION ENVIADO OK\r\n");
				memset(this->respuesta,0,sizeof(this->respuesta));
				break;
			}
			if(strstr((char *)this->respuesta,"400")!=NULL)
			{
				//como se produjo un error en la transmicion se restablece el GSM
				//al envio de datos por GSM, descativando el GPRS, forzandole para eso
				// que se ejecute directamente a partir del comando 16
				this->num_comando_at=16;


				LOG("***ERROR AL ENVIAR HTTPACTION***\r\n");
				memset(this->respuesta,0,sizeof(this->respuesta));
			}
			break;
		case 14:
			snprintf(comando,sizeof(comando),"AT+HTTPREAD\r\n");
			tam_respuesta=this->ejecutar_comando_at(comando, CHARACTER_CUT_NEW_LINE);

			if(tam_respuesta==0)
				break;

			if(strcmp((char *)this->respuesta,"OK\r\n")==0)
			{
				this->num_comando_at++; //paso al siguiente estado
				this->estado_comando_at=0;

				LOG(comando);
				LOG("Respuesta:%s\r\n",(char *)this->respuesta);

				memset(this->respuesta,0,sizeof(this->respuesta));
				break;
			}

			LOG((char *)this->respuesta);
			//SI QUIERO SABER CUAL ES EL MENSAJE JSON DE RESPUESTA DEL SERVIDO
			//LO GUARDO EN LA VARIABLE QUE SE ENVIO DESDE CONTROLLER
			if((this->respuesta[0]=='{')&&(this->respuesta[tam_respuesta-3]=='}'))
			{
				strncpy((char *)msj_json_resp,(char *)this->respuesta,tam_msj_resp);
			}

			break;
		case 15:
			snprintf(comando,sizeof(comando),"AT+HTTPTERM\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			break;
		case 16:
			snprintf(comando,sizeof(comando),"AT+SAPBR=0,1\r\n");
			resp_comando_at=this->realizar_operacion_ok(comando, CHARACTER_CUT_NEW_LINE);

			if(resp_comando_at==true)
			{
				operacion_completa=true;
				this->num_comando_at=0;

				tiempo_total_ejecucion=HAL_GetTick()-tiempo_total_inicial_ejecucion;
				LOG("###tiempo de Post:%u\r\n",tiempo_total_ejecucion);
				break;

			}
			break;
		default:
			break;
	}
	return operacion_completa;
}


/***************************************************************************************************/

size_t Controlador_GSM::ejecutar_comando_at(const char * comando,byte_t  caracter_corte_respuesta)
{
	size_t resp=0;

	switch(this->estado_comando_at)
	{
		case 0:
			this->serial_GSM->flush();
			this->estado_comando_at++;
			break;

		case 1:
			//Se aplica un Delay no Bloqueante para ejecutar comando AT
			tiempo_espera_ejecutar_at=HAL_GetTick()-tiempo_inicial_ejecutar_at;
			if(tiempo_espera_ejecutar_at > TIEMPO_ESPERA_RESPUESTA_COMANDO_AT)
			{
				this->serial_GSM->send(comando);
				this->estado_comando_at++;
				tiempo_inicial_respuesta_at=HAL_GetTick();

			}
			break;

		case 2:
			//Se aplica un Delay no Bloqueante para recibir la respuesta de la ejecucion del
			//comando AT
			tiempo_espera_respuesta_at=HAL_GetTick()-tiempo_inicial_respuesta_at;
			if(tiempo_espera_respuesta_at > TIEMPO_ESPERA_RESPUESTA_COMANDO_AT)
			{
				resp=this->serial_GSM->read_until_character(this->respuesta,sizeof(this->respuesta),caracter_corte_respuesta);
				//resp=this->serial_GSM->read(this->respuesta,sizeof(this->respuesta));
				tiempo_inicial_respuesta_at=HAL_GetTick();
				tiempo_inicial_ejecutar_at=HAL_GetTick();
			}

			break;
		default:
			break;
	}
	return resp;
}

/***************************************************************************************************/

bool Controlador_GSM::realizar_operacion_ok(const char * comando,byte_t  caracter_corte_respuesta)
{
	size_t resp_comando_at=0;

	resp_comando_at=this->ejecutar_comando_at(comando, caracter_corte_respuesta);

	if(resp_comando_at==0)
		return false;

	if(strcmp((char *)this->respuesta,"OK\r\n")==0)
	{

		this->num_comando_at++; //paso al siguiente estado
		this->estado_comando_at=0;

		LOG(comando);
		LOG("Respuesta:%s\r\n",(char *)this->respuesta);

		memset(this->respuesta,0,sizeof(this->respuesta));
		return true;
	}
	else
	{
		LOG("Respuesta:%s\r\n",(char *)this->respuesta);
		return false;
	}



}

/***************************************************************************************************/
void Controlador_GSM::proximo_estado(Estado_Op_Gsm proximo_estado_ok, Estado_Op_Gsm proximo_estado_error, uint32_t timeout)
{
	//si se ingresa por primera vez a un estado
	if( tiempo_en_estado == 0 )
	{
		tiempo_en_estado = HAL_GetTick();
	}

	if( estado_op == proximo_estado_ok )
	{

		if( (timeout > 0) && ( (HAL_GetTick() - tiempo_en_estado) > timeout) )
		{
			estado_op = proximo_estado_error;
			tiempo_en_estado = 0;
		}
	}
	else
	{
		estado_op = proximo_estado_ok;
		tiempo_en_estado = 0;
	}
}

