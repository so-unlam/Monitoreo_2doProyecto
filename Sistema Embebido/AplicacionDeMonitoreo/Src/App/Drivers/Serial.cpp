/*
 * @file Serial.cpp
 * @brief Implementacion del driver puerto Serial.
 * @date: Oct 29, 2018
 * @author: Waldo A. Valiente, basado en codigo C de Esteban Carnuccio.
 */

#include <App/Drivers/Serial.h>
#include "main.h"

Serial * usart_debug = NULL;


/***************************************************************************************************/

Serial::Serial( )
{
	this->handler_uart  = NULL;

	this->num_character_read=0;
	this->buffer_rx_full=false;


}

/***************************************************************************************************/

Serial::~Serial()
{

}

/***************************************************************************************************/
void Serial::usart_init(UART_HandleTypeDef * huart)
{
	this->handler_uart=huart;

	//Se pone en cero al inicializar la UART el flag de rececpion en el registro SR
	__HAL_UART_CLEAR_FLAG(this->handler_uart,UART_FLAG_RXNE);
	/* enable UART RX interrupt. Disable the TX interrupt since the FIFO's empty right now */
	__HAL_UART_ENABLE_IT(this->handler_uart, UART_IT_RXNE);		/* receiver not empty */
	__HAL_UART_DISABLE_IT(this->handler_uart, UART_IT_TXE);		/* transmit empty */

}

/***************************************************************************************************/

void Serial::interrupt_handler()
{
	uint32_t tmp1, tmp2;
	bool err;
	err = false;

	/* UART parity error interrupt occurred ------------------------------------*/
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_PE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_PE);
	if ((tmp1 != RESET) && (tmp2 != RESET))
	{
		__HAL_UART_CLEAR_PEFLAG(this->handler_uart);
		err = true;

		LOG("Error en paridad\r\n");
	}

	/* UART frame error interrupt occurred -------------------------------------*/
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_FE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_ERR);
	if ((tmp1 != RESET) && (tmp2 != RESET))
	{
		__HAL_UART_CLEAR_FEFLAG(this->handler_uart);
		err = true;

		LOG("Error en Frame\r\n");
	}

	/* UART noise error interrupt occurred -------------------------------------*/
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_NE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_ERR);
	if ((tmp1 != RESET) && (tmp2 != RESET))
	{
		__HAL_UART_CLEAR_NEFLAG(this->handler_uart);
		err = true;

		LOG("Error en ruido UART\r\n");
	}

	/* UART Over-Run interrupt occurred ----------------------------------------*/
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_ORE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_ERR);
	if ((tmp1 != RESET) && (tmp2 != RESET))
	{
		__HAL_UART_CLEAR_OREFLAG(this->handler_uart);
		err = true;

		LOG("Error en Overrun\r\n");
	}

	if(err==true)
		return;

	//Si ocurrio una IRQ de recepcion de datos en el pin RX
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_RXNE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_RXNE);

	if((tmp1 != RESET) && (tmp2 != RESET))
	{

		this->save_data_fifo_rx();
		return;
	}

	//Si ocurrio una IRQ de transmicion de datos en el pin TX
	tmp1 = __HAL_UART_GET_FLAG(this->handler_uart, UART_FLAG_TXE);
	tmp2 = __HAL_UART_GET_IT_SOURCE(this->handler_uart, UART_IT_TXE);

	if((tmp1 != RESET) && (tmp2 != RESET))
	{
		this->get_data_fifo_tx_irq();
	}

}


/***************************************************************************************************/
size_t Serial::send(const char *fmt, ...)
{

	//lo declaro +1 para que tenga en cuenta el \0 al final de la cadena al hacer vsnprintf
	const int tam_msg=BUFFER_SIZE_SERIAL+1;
	//const int tam_msg=90;
	byte_t msg[tam_msg];
	size_t result_send=0;
	int n=0;

	memset(msg,0,sizeof(msg));

	va_list argp;
    va_start(argp, fmt);

    //con vsnprintf se arma el msg sin peligro de producir oveflow
    //debido a que se la cadena orignr es mas grande que el destino
    //trunca al mensaje.
    n = vsnprintf((char *)msg, sizeof(msg),fmt,argp);

    //como vsnprintf no agrega el \0 al final de la cadena, entonces me fijo que n sea menor a BUFFER_SIZE.
    //Un byte menos del maximo de msg para poder siempre agregar un \0
    if(n>-1 && n<(tam_msg-1))
    {
    	msg[n+1]='\0';
        result_send=this->send_mode_irq(msg,strlen((char*)msg));
        va_end(argp);
    }
    else
    {
    	//LOG("\n*******ERROR: se trunco mensaje en metodo SEND. Se pierden datos!!\r\n\n");
    }


    return result_send;
}




/***************************************************************************************************/
size_t Serial::read(byte_t *buf, size_t tam_buffer_destino)
{
	unsigned int nread=0;

	nread= read_mode_irq(buf,tam_buffer_destino);

	return nread;
}

/***************************************************************************************************/
size_t Serial::read_line(byte_t *buf, size_t tam_buffer_destino)
{
	size_t bytes_read= this->read_until_character(buf,tam_buffer_destino,CHARACTER_CUT_NEW_LINE);
	return bytes_read;
}

/***************************************************************************************************/

size_t Serial::read_until_character(byte_t *buffer_destino, size_t tam_buffer_destino,byte_t caracter_corte)
{

	size_t tam_aux;
	size_t resultado=0;
	size_t nRead=0;
	bool salir_while=false;
	byte_t caracter_recv=0;

	//se verifica que el tamano del buffer destino no sea mayor que el del buffer auxiliar(buffer_aux_rx)
	//donde se van a ir concatena temporalmente los caracteres recibidos a medida que se van leyendo
	//del buffer circular
	if(tam_buffer_destino>sizeof(this->buffer_aux_rx))
	{
		LOG("\n***ERROR:tamano de Buffer de lectura destino es menor que buffer aux****\r\n\n");
		return 0;
	}

	tam_aux=tam_buffer_destino-3;

	//se limpia el buffer destino antes de guardar en el los bytes recibidos de la USART
	memset(buffer_destino,0,tam_buffer_destino);

	while ((this->num_character_read<tam_aux)&&(salir_while!=true))
	{

		//================================== region critica============================
		this->dissable_irq_rx();
		resultado=this->rx.fifo_pop_byte(&caracter_recv);
		this->enable_irq_rx();
		//============================================================================

		if(buffer_rx_full==true)
		{
			//LOG("\n*********ERROR:BUFFER CIRCULAR RX LLENO\r\n\n");
			this->buffer_rx_full=false;
		}
		if (resultado==true)
		{
			//si se recibio un caracter en el buffer circular de rx, se lo concatena a un
			//buffer global.
			this->buffer_aux_rx[this->num_character_read]=caracter_recv;
			this->num_character_read++;
			//si el caracter recibido es un caracter de corte, por ejemplo \n
			if(caracter_recv==caracter_corte)
			{
				salir_while=true;
			}
		}
		else
		{
			//Si no hay caracteres recibidos en buffer circular, entonces se sale del while
			break;
		}
	}

	if (this->num_character_read==tam_aux)
		LOG("\n***********SE LLEGO AL TAMANO MAXIMO DE BUFFER LECTURA DESTINO*\r\n\n");

	//Una vez que se sale del while
	if((salir_while==true)||(this->num_character_read==tam_aux))
	{
		//si se recibio un caracter de corte o se recibio la cantidad maxima de carateres
		//que puede almacenar el buffer destino
		this->buffer_aux_rx[this->num_character_read]='\0';
		strncpy((char *)buffer_destino,(char *)this->buffer_aux_rx,this->num_character_read);
		nRead=this->num_character_read;
		this->num_character_read=0;
		memset(buffer_aux_rx,0,sizeof(buffer_aux_rx));
	}


	return nRead;
}

/***************************************************************************************************/
void Serial::flush()
{
	this->rx.flush();
}

/***************************************************************************************************/
void Serial::enable_irq_rx()
{
	__HAL_UART_ENABLE_IT(this->handler_uart, UART_IT_RXNE);
}
/***************************************************************************************************/
void Serial::dissable_irq_rx()
{
	__HAL_UART_DISABLE_IT(this->handler_uart, UART_IT_RXNE);		/* receiver not empty */
}


/***************************************************************************************************/

void Serial::get_data_fifo_tx_irq()
{
	byte_t val;

	if (this->tx.fifo_pop_byte(&val)==true)
	{
		this->handler_uart->Instance->DR = (uint32_t) val;
	}
	else
	{
		__HAL_UART_DISABLE_IT(this->handler_uart, UART_IT_TXE);
	}
}

/***************************************************************************************************/
void Serial::save_data_fifo_rx()
{

	volatile byte_t val;

	//================================== region critica============================
	val = (byte_t)(this->handler_uart->Instance->DR);
	if(this->rx.fifo_push_byte(val)==false)
	{
		//Esta lleno el buffer entonces deshabilito las interrupciones de RXNE
		//	__HAL_UART_DISABLE_IT(this->handler_uart, UART_IT_RXNE);
		this->buffer_rx_full=true;
	}
	//==============================================================================
}
/***************************************************************************************************/
size_t Serial::send_mode_irq(byte_t *buf, size_t len)
{
	size_t  nwritten;
	bool resp=false;
	nwritten = 0;
	while (len)
	{
		__HAL_UART_DISABLE_IT(this->handler_uart, UART_IT_TXE);
		resp=this->tx.fifo_push_byte(*buf);
		__HAL_UART_ENABLE_IT(this->handler_uart, UART_IT_TXE);

		if (resp==true)
		{
			// enable the transmitter empty interrupt to start things flowing
			++buf;
			++nwritten;
			--len;
		}
		else
		{
			//HAL_Delay(1);
			noop();
			noop();
		}

	};

	return nwritten;
}

/***************************************************************************************************/

size_t Serial::read_mode_irq(byte_t *buffer_destino, size_t tam_buffer_destino)
{
	size_t n_leidos=0;
	size_t resultado=0;
	byte_t caracter_recv=0;


	//se limpia el buffer destino antes de guardar en el los bytes recibidos de la USART
	memset(buffer_destino,0,tam_buffer_destino);


	while (n_leidos<tam_buffer_destino-TAM_BYTES_RESGUARDO)
	{
		//================================== region critica============================
		this->dissable_irq_rx();

		resultado=this->rx.fifo_pop_byte(&caracter_recv);
		if(this->rx.analyze_full_buffer_percentage()==true)
			__HAL_UART_ENABLE_IT(this->handler_uart, UART_IT_RXNE);

		this->enable_irq_rx();
		//============================================================================

		if (resultado==true)
		{
			buffer_destino[n_leidos]=caracter_recv;
			n_leidos++;
		}
		else
		{
			break;
		}
	}

	return n_leidos;
}
