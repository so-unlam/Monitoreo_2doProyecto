/*
    File:       nmea.cpp
    Version:    0.1.0
    Date:       Feb. 23, 2013
	License:	GPL v2

	Controlador_GPS GPS content parser

    ****************************************************************************
    Copyright (C) 2013 Radu Motisan  <radu.motisan@gmail.com>

	http://www.pocketmagic.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    ****************************************************************************
 */
#include <App/Drivers/Controlador_GPS.h>


extern Serial * usart_debug;


Controlador_GPS::Controlador_GPS()
{
	m_bFlagComputedCks		 = false;
	m_bFlagReceivedCks 		 = false;
	index_received_checksum  = 0;
	m_nWordIdx				 = 0;
	m_nPrevIdx				 = 0;
	m_nNowIdx				 = 0;
	m_nChecksum				 = 0;
	res_fLongitude			 = 0;
	res_fLatitude			 = 0;
	res_nUTCHour			 = 0;
	res_nUTCMin				 = 0;
	res_nUTCSec				 = 0;
	res_nUTCDay				 = 0;
	res_nUTCMonth			 = 0;
	res_nUTCYear			 = 0;
	res_nSatellitesUsed		 = 0;
	res_fAltitude			 = 0;
	res_fSpeed				 = 0;
	res_fBearing			 = 0;

	pos_analizada		     = 0;
	tam_cadena_gps			 = 0;

	serial_GPS				 = NULL;
	retorno_carro_detectado	 = false;
	irq_gps_habilitado		 = false;
	GRMC_leido				 = false;
	estado_op_gps 			 = Operacion_Gps::LEER_GPS;
	tiempo_f_duracion_leer_gps = 0;
	tiempo_i_duracion_leer_gps = HAL_GetTick();
	tiempo_i_prender_led_gps   = HAL_GetTick();
	tiempo_a_prender_led_gps   = 0;

	led_gps_pin=0;
	led_gps_puerto=NULL;

	kalman_latitud.setRQ( 0.1, 1) ;
	kalman_longitud.setRQ( 0.1, 1) ;


}

/***************************************************************************************************/
Controlador_GPS::~Controlador_GPS()
{

}
/***************************************************************************************************/
bool Controlador_GPS::controlador_init(Serial * serial_GPS)
{
	bool resp=true;

	this->serial_GPS=serial_GPS;

	//El led del gps este siempre prendido al iniciar
	//la aplicacion y parpadee cuando se empiezan a leer las coordenadas y no tenga se�al
	HAL_GPIO_WritePin(led_gps_puerto,led_gps_pin,GPIO_PIN_SET);

	return resp;
}
/***************************************************************************************************/
bool Controlador_GPS::leer_gps()
{
	bool resp=false;
	Estado_Gps estado_lectura_gps=Estado_Gps::DATO_NO_DISPONIBLE;
	Resultado_Analisis_Nmea resultado_nmea=Resultado_Analisis_Nmea::ANALIZANDO_CADENA;

	switch (estado_op_gps)
	{
		case Operacion_Gps::LEER_GPS:
			//SE LEEN LAS COORDENADAS DEL GPS
			//habilito las interrupciones de irq para poder recibir las coordenadas del GPS

			if(irq_gps_habilitado==false)
			{
				//limpio el buffer_circular de rx por si habian datos viejos que no se hayan leido
				//serial_GPS->flush();
				//habilito la irq de recepcion del buffer
				serial_GPS->enable_irq_rx();
				irq_gps_habilitado=true;
			}

			memset(cadena_gps,0,sizeof(cadena_gps));
			tam_cadena_gps=0;
			//leo a traves del puerto serial una linea del GPS que
			//supuestamentamente deberia tener el formato NMEA
			 tam_cadena_gps=serial_GPS->read_line(cadena_gps,sizeof(cadena_gps));

			 //si no se recibio algun string del GPS
			 if(tam_cadena_gps==0)
			 {
				 resp=false;
			 	 break;
			 }
			 //se loguean los datos leidos del GPS
	//		 LOG("!!!CADENA:%s",cadena_gps);

			 this->pos_analizada=0;
			 this->m_bFlagComputedCks = false;
			 reiniciar_variables();

			 //Como se se leyo una linea de datos del GPS entonces se pasa al siguiente estado
			 estado_op_gps=Operacion_Gps::DETECTAR_COORDENADAS;
			break;

		case Operacion_Gps::DETECTAR_COORDENADAS:

			//this->tiempo_i_duracion_leer_gps=HAL_GetTick();

			 //se valida que la linea recibida sea efectivmente una cadena NMEA
			 resultado_nmea=detectar_nmea();
			 //se muestra el tiempo que se tarda en detectar si es nmea
			 //this->tiempo_f_duracion_leer_gps=HAL_GetTick()-this->tiempo_i_duracion_leer_gps;
			 //LOG("tiempo gps:%u\r\n",this->tiempo_f_duracion_leer_gps);


			 if(resultado_nmea==Resultado_Analisis_Nmea::NO_ES_NMEA)
			 {
				 //si no es nmea se retorna false
				 estado_op_gps=Operacion_Gps::LEER_GPS;
				 resp=false;
				 break;
			 }
			 else if(resultado_nmea==Resultado_Analisis_Nmea::ANALIZANDO_CADENA)
			 {
				 //se retorna false, si todavia no se termino de analizar
				 //la linea recibida para determinar si es NMEA
				 resp=false;
				 break;
			 }

			 //si es NMea entonces se parsean los datos y se los guarda en los atributos
			// de esta clase.
			estado_lectura_gps=parsear_datos();

			if (estado_lectura_gps==Estado_Gps::DATO_DISPONIBLE)
			{
				//si el GPS tiene se�al y se obtuvieron correctamente las coordenadas
				//entonces se retorna true y se vuelve al estado leer_gps
				estado_op_gps=Operacion_Gps::LEER_GPS;
				resp=true;
				//Ademas si tiene se�al se notifica dicha suceso mantendido prendido el led
				//del GPS
				HAL_GPIO_WritePin(led_gps_puerto,led_gps_pin,GPIO_PIN_SET);
			}
			else if (estado_lectura_gps==Estado_Gps::DATO_NO_DISPONIBLE)
			{
				//si el GPS tiene se�al y no se pudo obtener las coordenadas de la cadena NMEA
				//entonces se retorna false y se vuelve al estado leer_gps
				estado_op_gps=Operacion_Gps::LEER_GPS;
				resp=false;
			}
			else
			{
				//si el GPS no tiene se�al, entonces se pasa al estado
				//para notificar que no hay se�al y se retona false
				estado_op_gps=Operacion_Gps::NOTIFICAR_SIN_SENAL;
				resp=false;
			}

			break;
		case Operacion_Gps::NOTIFICAR_SIN_SENAL:
			//SI NO HAY SE�AL DE GPS
			tiempo_a_prender_led_gps=HAL_GetTick()-tiempo_i_prender_led_gps;

			if(tiempo_a_prender_led_gps>TIEMPO_ESPERA_LED_GPS)
 			{
				//el gps no tiene se�al entonces parpadea el led del GPS
 				HAL_GPIO_TogglePin(led_gps_puerto,led_gps_pin);

 				//LOG("==GPS SIN SENAL==\r\n");

 				tiempo_i_prender_led_gps=HAL_GetTick();

				estado_op_gps=Operacion_Gps::LEER_GPS;
 			}

			break;
		default:
			break;
	}
	return resp;
}

/***************************************************************************************************/
int Controlador_GPS::get_hour()
{
	return res_nUTCHour;
}
int Controlador_GPS::get_minute()
{
	return res_nUTCMin;
}
int Controlador_GPS::get_second()
{
	return res_nUTCSec;
}
int Controlador_GPS::get_day()
{
	return res_nUTCDay;
}
int Controlador_GPS::get_month()
{
	return res_nUTCMonth;
}
int Controlador_GPS::get_year()
{
	return res_nUTCYear;
}

float Controlador_GPS::get_latitude()
{
	if (res_fLatitude==0)
		return LATITUD_X_DEFECTO;
	else
		return res_fLatitude;
}

float Controlador_GPS::get_longitude()
{
	if (res_fLongitude==0)
		return LONGITUD_X_DEFECTO;
	else
		return res_fLongitude;
}

int Controlador_GPS::get_satellites()
{
	return res_nSatellitesUsed;
}

float  Controlador_GPS::get_altitude()
{
	return res_fAltitude;
}

float Controlador_GPS::get_speed()
{
	return res_fSpeed;
}

float Controlador_GPS::get_bearing()
{
	return res_fBearing;
}

bool Controlador_GPS::get_grmc_leido()
{
	bool resp=GRMC_leido;
	GRMC_leido=false;

	return resp;
}

/***************************************************************************************************/
Controlador_GPS::Resultado_Analisis_Nmea Controlador_GPS::detectar_nmea()
{
	//bool salir_while=false;

	//bool es_sentencia_nmea=false;
	Resultado_Analisis_Nmea resultado_nmea=Resultado_Analisis_Nmea::ANALIZANDO_CADENA;

	//while((pos_analizada<tam_cadena_gps)&&(es_sentencia_nmea==false))
	{
		switch(cadena_gps[pos_analizada])
		{
			//se chequea si es un caracter de inicio de trama
			case '$':
				//si en el medio de la sentencia Nmea se encuetran otro signo $,
				//esto quiere decir que hubo un error al recibir la cadena.Por ese motivo
				//se vuelve a empezar analizar desde la nueva posicion del $,
				//reciniciando devuleta los contadores
				m_bFlagComputedCks = true;
				reiniciar_variables();
				break;
			case '*':
				retorno_carro_detectado=false;
				if (m_bFlagComputedCks)
					m_bFlagComputedCks = false;
				m_bFlagReceivedCks = true;
				break;
			case '\n':
				//se chequea si se recibio un \r y a continuacion un \n, ya que estos indican fin la trama
				if (retorno_carro_detectado==true)
				{
					//se chequea que si se detecto el \r seguido de \n, se haya parseado y guardado correctamente
					//el checksum recibido
					if(index_received_checksum==2)
					{
						// catch last ending item too
						if(agregar_a_datos_parseados(0)==true)
						{
							m_nWordIdx++;
						}
						else
							break;

						// cut received m_nChecksum
						if(guardar_checksum(0)==true)
							index_received_checksum++;
						else
							break;

						// sentence complete, read done
						//es_sentencia_nmea=true;
						resultado_nmea=Resultado_Analisis_Nmea::ES_NMEA;
					}
					else
						//si no cumple ninguna condici�n no es una trama NMEA, entonces se sale del while
						//salir_while=true;
						resultado_nmea=Resultado_Analisis_Nmea::NO_ES_NMEA;
				}
				break;
			case '\r':
				//si se detecto un \r, lo marco dado que puede ser un POSIBLE fin de trama
				retorno_carro_detectado=true;
				break;
			default:
				//si se recibe cualquier caracter menos $,\r y \n se lo analiza
				retorno_carro_detectado=false;
				//se calcula el m_nchecksum logico:contando todos los caracteres que estan entre $ y * exlcusivamente
				if (m_bFlagComputedCks) m_nChecksum ^= cadena_gps[pos_analizada];
				if (m_bFlagReceivedCks)
				{
					if(guardar_checksum(cadena_gps[pos_analizada])==true)
						index_received_checksum++;
					else
						//salir_while=true;
						resultado_nmea=Resultado_Analisis_Nmea::NO_ES_NMEA;
				}

				if (cadena_gps[pos_analizada] == ',')
				{
					if(agregar_a_datos_parseados(0)==true)
					{
						m_nWordIdx++;
						m_nNowIdx=0;
					}
					else
						//salir_while=true;
						resultado_nmea=Resultado_Analisis_Nmea::NO_ES_NMEA;
				}
				else if (m_bFlagComputedCks == true)
				{
					if(agregar_a_datos_parseados(cadena_gps[pos_analizada])==true)
						m_nNowIdx++;
					else
						//salir_while=true;
						resultado_nmea=Resultado_Analisis_Nmea::NO_ES_NMEA;
				}
		}

		//fuerzo la salida del while, si se activo la bandera salir_while
		//if(salir_while==true)
		//	break;

		pos_analizada++;

		if(pos_analizada>tam_cadena_gps)
			resultado_nmea=Resultado_Analisis_Nmea::NO_ES_NMEA;
	}
	return resultado_nmea;
}

/***************************************************************************************************/
bool Controlador_GPS::agregar_a_datos_parseados(char c)
{
	if((m_nWordIdx<TAM_FILAS_TMP_WORDS)&&(m_nNowIdx<TAM_COLUMNAS_TMP_WORDS))
	{
		tmp_words[m_nWordIdx][m_nNowIdx]=c;
	}
	else
	{
		LOG("ERROR: Se desbordo tmp_words\r\n");
		return false;
	}

	return true;
}

/***************************************************************************************************/
bool Controlador_GPS::guardar_checksum(char c)
{
	if(index_received_checksum<TAM_TMP_CHK)
	{
		tmp_szChecksum[index_received_checksum] = c;
	}
	else
	{
		LOG("ERROR: Se desbordo tmp_szChecksum\r\n");
		return false;
	}

	return true;
}
/***************************************************************************************************/
void Controlador_GPS::reiniciar_variables()
{

	//reinicio los arrays
	memset(tmp_words,0,sizeof(tmp_words));
	memset(tmp_szChecksum,0,sizeof(tmp_szChecksum));

	m_nChecksum = 0;
	// after getting  * we start cuttings the received m_nChecksum
	m_bFlagReceivedCks = false;
	index_received_checksum = 0;
	// word cutting variables
	m_nWordIdx = 0; m_nNowIdx = 0;
	retorno_carro_detectado=false;
}

/***************************************************************************************************/
Controlador_GPS::Estado_Gps Controlador_GPS::parsear_datos()
{
	int received_cks = 0;
	received_cks = 16*digit_to_dec(tmp_szChecksum[0]) + digit_to_dec(tmp_szChecksum[1]);

	//Se comprueba que los datos leidos a traves del puerto serial sean validos.
	//Para eso se chequea que el checksum de la cadena NMEA recibida sea correcto
	if (m_nChecksum != received_cks)
	{
		//si el checksum es invalido entonces se retorna DATO_NO_DISPONIBLE
		return Controlador_GPS::Estado_Gps::DATO_NO_DISPONIBLE;
	}

	/* *****************************************************************************
	 * SE EXTRAEN SOLAMENTE LOS DATOS DEL GPS DE LA LINEA DE GPGGA, LAS DEMAS TIPO DE
	 * CABECERAS COMO GPRMC,GSSA,ETC. SE DESCARTAN
	 * *****************************************************************************
	 */
	/* *****************************************************************************
	 * Se analiza si la linea NMEA recibida pertence al tipo GPGGA,
	 * si es asi se extrean los datos que tiene almacenados
	 * *****************************************************************************
	 * $GPGGA
	 * $GPGGA,hhmmss.ss,llll.ll,a,yyyyy.yy,a,x,xx,x.x,x.x,M,x.x,M,x.x,xxxx*hh
	 * ex: $GPGGA,230600.501,4543.8895,N,02112.7238,E,1,03,3.3,96.7,M,39.0,M,,0000*6A,
	 *
	 * WORDS:
	 *  1    = UTC of Position
	 *  2    = Latitude
	 *  3    = N or S
	 *  4    = Longitude
	 *  5    = E or W
	 *  6    = GPS quality indicator (0=invalid; 1=GPS fix; 2=Diff. GPS fix)
	 *  7    = Number of satellites in use [not those in view]
	 *  8    = Horizontal dilution of position
	 *  9    = Antenna altitude above/below mean sea level (geoid)
	 *  10   = Meters  (Antenna height unit)
	 *  11   = Geoidal separation (Diff. between WGS-84 earth ellipsoid and mean sea level.
	 *      -geoid is below WGS-84 ellipsoid)
	 *  12   = Meters  (Units of geoidal separation)
	 *  13   = Age in seconds since last update from diff. reference station
	 *  14   = Diff. reference station ID#
	 */
	if (strcmp(tmp_words[0], "GPGGA") == 0)
	{
		// Check GPS Fix: 0=no fix, 1=GPS fix, 2=Dif. GPS fix
		if (tmp_words[6][0] == '0')
		{
			// clear data
			res_fLatitude = 0;
			res_fLongitude = 0;

			return Estado_Gps::SIN_SENAL;
		}
		// parse time
		res_nUTCHour = digit_to_dec(tmp_words[1][0]) * 10 + digit_to_dec(tmp_words[1][1]);
		res_nUTCMin = digit_to_dec(tmp_words[1][2]) * 10 + digit_to_dec(tmp_words[1][3]);
		res_nUTCSec = digit_to_dec(tmp_words[1][4]) * 10 + digit_to_dec(tmp_words[1][5]);
		// parse latitude and longitude in Controlador_GPS format
		res_fLatitude = string_to_float(tmp_words[2]);
		res_fLongitude = string_to_float(tmp_words[4]);
		// get decimal format
		if (tmp_words[3][0] == 'S') res_fLatitude  *= -1.0;
		if (tmp_words[5][0] == 'W') res_fLongitude *= -1.0;
		float degrees = trunc(res_fLatitude / 100.0f);
		float minutes = res_fLatitude - (degrees * 100.0f);
		res_fLatitude = degrees + minutes / 60.0f;
		degrees = trunc(res_fLongitude / 100.0f);
		minutes = res_fLongitude - (degrees * 100.0f);
		res_fLongitude = degrees + minutes / 60.0f;

		//aplico Kalman a las coordenadas longitud y latitud
		res_fAltitude = kalman_latitud.filtro(res_fLatitude,0);
		res_fLongitude = kalman_longitud.filtro(res_fLongitude,0);

		// parse number of satellites
		res_nSatellitesUsed = (int)string_to_float(tmp_words[7]);

		// parse altitude
		res_fAltitude = string_to_float(tmp_words[9]);

		//como ya se leyo una coordenada del GPS correctamente se desabilita las
		//Irq de recpcion de la coordenadas
		serial_GPS->dissable_irq_rx();
		//serial_GPS->flush();
		irq_gps_habilitado=false;

		//se leyeron las coordenadas correctamente
		return Estado_Gps::DATO_DISPONIBLE;
	}

	/* *****************************************************************************
	 * Se analiza si la linea NMEA recibida pertence al tipo GPRMC,
	 * si es asi se extrean los datos que tiene almacenados
	 * *****************************************************************************
	 * $GPRMC
	 * note: a siRF chipset will not support magnetic headers.
	 * $GPRMC,hhmmss.ss,A,llll.ll,a,yyyyy.yy,a,x.x,x.x,ddmmyy,x.x,a*hh
	 * ex: $GPRMC,230558.501,A,4543.8901,N,02112.7219,E,1.50,181.47,230213,,,A*66,
	 *
	 * WORDS:
	 *  1	 = UTC of position fix
	 *  2    = Data status (V=navigation receiver warning)
	 *  3    = Latitude of fix
	 *  4    = N or S
	 *  5    = Longitude of fix
	 *  6    = E or W
	 *  7    = Speed over ground in knots
	 *  8    = Track made good in degrees True, Bearing This indicates the direction that the device is currently moving in,
	 *       from 0 to 360, measured in �azimuth�.
	 *  9    = UT date
	 *  10   = Magnetic variation degrees (Easterly var. subtracts from true course)
	 *  11   = E or W
	 */

	if (strcmp(tmp_words[0], "GPRMC") == 0)
	{
		// Check data status: A-ok, V-invalid
		if (tmp_words[2][0] == 'V')
		{
			// clear data
			res_fLatitude = 0;
			res_fLongitude = 0;

			return Estado_Gps::SIN_SENAL;
		}

		// parse time
		res_nUTCHour = digit_to_dec(tmp_words[1][0]) * 10 + digit_to_dec(tmp_words[1][1]);
		res_nUTCMin = digit_to_dec(tmp_words[1][2]) * 10 + digit_to_dec(tmp_words[1][3]);
		res_nUTCSec = digit_to_dec(tmp_words[1][4]) * 10 + digit_to_dec(tmp_words[1][5]);
		// parse latitude and longitude in Controlador_GPS format
		res_fLatitude = string_to_float(tmp_words[3]);
		res_fLongitude = string_to_float(tmp_words[5]);
		// get decimal format
		if (tmp_words[4][0] == 'S') res_fLatitude  *= -1.0;
		if (tmp_words[6][0] == 'W') res_fLongitude *= -1.0;
		float degrees = trunc(res_fLatitude / 100.0f);
		float minutes = res_fLatitude - (degrees * 100.0f);
		res_fLatitude = degrees + minutes / 60.0f;
		degrees = trunc(res_fLongitude / 100.0f);
		minutes = res_fLongitude - (degrees * 100.0f);
		res_fLongitude = degrees + minutes / 60.0f;

		//aplico Kalman a las coordenadas longitud y latitud
		res_fAltitude = kalman_latitud.filtro(res_fLatitude,0);
		res_fLongitude = kalman_longitud.filtro(res_fLongitude,0);

		//parse speed
		// The knot (pronounced not) is a unit of speed equal to one nautical mile (1.852 km) per hour
		res_fSpeed = string_to_float(tmp_words[7]);
		res_fSpeed *= 1.852; // convert to km/h
		// parse bearing
		res_fBearing = string_to_float(tmp_words[8]);
		// parse UTC date
		res_nUTCDay = digit_to_dec(tmp_words[9][0]) * 10 + digit_to_dec(tmp_words[9][1]);
		res_nUTCMonth = digit_to_dec(tmp_words[9][2]) * 10 + digit_to_dec(tmp_words[9][3]);
		res_nUTCYear = digit_to_dec(tmp_words[9][4]) * 10 + digit_to_dec(tmp_words[9][5]);

		//como ya se leyo una coordenada del GPS correctamente se desabilita las
		//Irq de recpcion de la coordenadas
		serial_GPS->dissable_irq_rx();
		serial_GPS->flush();
		irq_gps_habilitado=false;

		GRMC_leido=true;
		//se leyeron las coordenadas correctamente
		return Estado_Gps::DATO_DISPONIBLE;
	}



	return Estado_Gps::DATO_NO_DISPONIBLE;
}
/***************************************************************************************************/
void Controlador_GPS::configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin)
{
	this->led_gps_pin=led_pin;
	this->led_gps_puerto=led_puerto;
}
