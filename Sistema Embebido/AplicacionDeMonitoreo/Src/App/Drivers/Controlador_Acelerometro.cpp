/*
 * ControladorAcelerometro.cpp
 *
 *  Created on: Nov 3, 2018
 *      Author: Waldo A. Valiente
 */

#include <App/Drivers/Controlador_Acelerometro.hpp>
extern Serial * usart_debug;

// -------------------------------------------------------------------
// Definicion de Macros.

/* Default I2C clock */
#ifndef  MPU6050_I2C_CLOCK
# define MPU6050_I2C_CLOCK          400000            /*!< Default I2C clock speed */
#endif

/* Default I2C address */
#define MPU6050_I2C_ADDR			0xD0

/* Who I am register value */
#define MPU6050_I_AM				0x68


/**
 * @brief  Data rates predefined constants
 * @{
 */
#define MPU6050_DataRate_8KHz       0   /*!< Sample rate set to 8 kHz */
#define MPU6050_DataRate_4KHz       1   /*!< Sample rate set to 4 kHz */
#define MPU6050_DataRate_2KHz       3   /*!< Sample rate set to 2 kHz */
#define MPU6050_DataRate_1KHz       7   /*!< Sample rate set to 1 kHz */
#define MPU6050_DataRate_500Hz      15  /*!< Sample rate set to 500 Hz */
#define MPU6050_DataRate_250Hz      31  /*!< Sample rate set to 250 Hz */
#define MPU6050_DataRate_125Hz      63  /*!< Sample rate set to 125 Hz */
#define MPU6050_DataRate_100Hz      79  /*!< Sample rate set to 100 Hz */

#define I2C_TIMEOUT 				1  /*!< Timeout I2c */

/* MPU6050 registers */
#define MPU6050_AUX_VDDIO			0x01
#define MPU6050_SMPLRT_DIV			0x19
#define MPU6050_CONFIG				0x1A
#define MPU6050_GYRO_CONFIG			0x1B
#define MPU6050_ACCEL_CONFIG		0x1C
#define MPU6050_MOTION_THRESH		0x1F
#define MPU6050_INT_PIN_CFG			0x37
#define MPU6050_INT_ENABLE			0x38
#define MPU6050_INT_STATUS			0x3A
#define MPU6050_ACCEL_XOUT_H		0x3B
#define MPU6050_ACCEL_XOUT_L		0x3C
#define MPU6050_ACCEL_YOUT_H		0x3D
#define MPU6050_ACCEL_YOUT_L		0x3E
#define MPU6050_ACCEL_ZOUT_H		0x3F
#define MPU6050_ACCEL_ZOUT_L		0x40
#define MPU6050_TEMP_OUT_H			0x41
#define MPU6050_TEMP_OUT_L			0x42
#define MPU6050_GYRO_XOUT_H			0x43
#define MPU6050_GYRO_XOUT_L			0x44
#define MPU6050_GYRO_YOUT_H			0x45
#define MPU6050_GYRO_YOUT_L			0x46
#define MPU6050_GYRO_ZOUT_H			0x47
#define MPU6050_GYRO_ZOUT_L			0x48
#define MPU6050_MOT_DETECT_STATUS	0x61
#define MPU6050_SIGNAL_PATH_RESET	0x68
#define MPU6050_MOT_DETECT_CTRL		0x69
#define MPU6050_USER_CTRL			0x6A
#define MPU6050_PWR_MGMT_1			0x6B
#define MPU6050_PWR_MGMT_2			0x6C
#define MPU6050_FIFO_COUNTH			0x72
#define MPU6050_FIFO_COUNTL			0x73
#define MPU6050_FIFO_R_W			0x74
#define MPU6050_WHO_AM_I			0x75

/* Gyro sensitivities in degrees/s */
#define MPU6050_GYRO_SENS_250		((float) 131)
#define MPU6050_GYRO_SENS_500		((float) 65.5)
#define MPU6050_GYRO_SENS_1000		((float) 32.8)
#define MPU6050_GYRO_SENS_2000		((float) 16.4)

/* Acce sensitivities in g/s */
#define MPU6050_ACCE_SENS_2			((float) 16384)
#define MPU6050_ACCE_SENS_4			((float) 8192)
#define MPU6050_ACCE_SENS_8			((float) 4096)
#define MPU6050_ACCE_SENS_16		((float) 2048)

/***************************************************************************************************/

Controlador_Acel::Controlador_Acel()
{
	this->ptr_I2C_x = &hi2c1;
	this->i2c_direccion_disp = 0;
	this->Gyro_Mult = 0.0;
	this->Acce_Mult = 0.0;
	this->estado_lectura=Estado_Lectura::DATOS_LEIDOS;
	this->bandera_primera_vez=true;
	this->solicitud_transmit_realizada=false;
	this->op_acel=Operacion_Acelerometro::ESPERAR_LECTURA_ACEL;


	this->tiempo_inicial_peticion=0;
	this->tiempo_actual_peticion=0;
	this->tiempo_incial_entre_medicion=0;
	this->tiempo_i_prender_led_acel=HAL_GetTick();
	this->tiempo_a_prender_led_acel=0;
	this->tiempo_en_estado=0;

	//establezco los valores devueltos por el calibrador, para poder escalar las mediciones
	//correctmante

	/*********************VALORES DE CALIBRACION DEL ACELEROMETRO QUE ESTA EN CASA**********/
	config.acel_offset_X =  1221;
	config.acel_offset_Y = -724;
	config.acel_offset_Z = -153;
	config.acel_escala_min_X = -15527;
	config.acel_escala_max_X =  16405;
	config.acel_escala_min_Y = -16550;
	config.acel_escala_max_Y =  16113;
	config.acel_escala_min_Z = -17833;
	config.acel_escala_max_Z =  15748;
	config.giro_offset_X = 1136;
	config.giro_offset_Y =  156;
	config.giro_offset_Z =    7;


	//inicializo los objetos para Kalman
	Kalman_Accelerometer_X.setRQ( 0.7, 0.1) ;
	Kalman_Accelerometer_Y.setRQ( 0.7, 0.1);
	Kalman_Accelerometer_Z.setRQ( 0.7, 0.1);
	Kalman_Gyroscope_X.setRQ( 0.7, 0.1 );
	Kalman_Gyroscope_Y.setRQ( 0.7, 0.1 );
	Kalman_Gyroscope_Z.setRQ( 0.7, 0.1 );


	this->led_acel_pin=0;
	this->led_acel_puerto=NULL;


}

/***************************************************************************************************/

Controlador_Acel::~Controlador_Acel()
{

}

/***************************************************************************************************/
bool Controlador_Acel::controlador_init(Dispositivo numero_dispositivo, Sensibilidad_Acelerometro sensibilidad_acelerometro, Sensibilidad_Giroscopio  sensibilidad_giroscopo)
{
	bool estado=false;

	Controlador_Acel::Resultado resultado;

	//Al encender el aparato el led del Acelerometro siempre se prende durante
	//1 segundo
	HAL_GPIO_WritePin(led_acel_puerto,led_acel_pin,GPIO_PIN_SET);
	HAL_Delay(500);

	resultado = this->iniciar(numero_dispositivo,sensibilidad_acelerometro, sensibilidad_giroscopo);
	//HAL_Delay(50);
	if (resultado == Controlador_Acel::Resultado::ERROR)
	{
		estado=false;
		LOG("Conexion MPU6050: ERROR\r\n");
	}
	else
	if (resultado == Controlador_Acel::Resultado::DISPOSITIVO_INVALIDO)
	{
		estado=false;
		LOG("Conexion MPU6050: Dispositivo invalido\r\n");
	}
	else
	if( resultado == Controlador_Acel::Resultado::OK )
	{
		estado=true;
		LOG("Conexion MPU6050: OK\r\n");
	}

	//si hubo un error en el seteo del acelerometro el led del mismo parpadeara
	//5 veces y finalmente se apagara.
	if(estado==false)
	{
		for(int i=0;i<CANT_PARPADEO_LED_ACEL_INICIAL;i++)
		{
			HAL_GPIO_TogglePin(led_acel_puerto,led_acel_pin);
			HAL_Delay(1000);
		}

		HAL_GPIO_WritePin(led_acel_puerto,led_acel_pin,GPIO_PIN_RESET);
	}

	HAL_Delay(50);

	return estado;
}
/***************************************************************************************************/
void Controlador_Acel::configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin)
{
	this->led_acel_pin=led_pin;
	this->led_acel_puerto=led_puerto;
}

/***************************************************************************************************/
void Controlador_Acel::proximo_estado(Operacion_Acelerometro proximo_estado_ok, Operacion_Acelerometro proximo_estado_error, uint32_t timeout)
{
	//si se ingresa por primera vez a un estado
	if( tiempo_en_estado == 0 )
	{
		tiempo_en_estado = HAL_GetTick();
	}

	if( op_acel == proximo_estado_ok )
	{

		if( (timeout > 0) && ( (HAL_GetTick() - tiempo_en_estado) > timeout) )
		{
			op_acel = proximo_estado_error;
			tiempo_en_estado = 0;
		}
	}
	else
	{
		op_acel = proximo_estado_ok;
		tiempo_en_estado = 0;
	}
}

/***************************************************************************************************/
bool Controlador_Acel::leer_acelerometro(Mpu_Datos_Escalados *datos_escalados,Mpu_Datos_Escalados *datos_kalman,Mpu_Datos_Raw *datos_raw_aux)
{
	bool resp=false;

	switch(op_acel)
	{
		///////////////////////////////////////////////////////////////////////////////////////////
		case(Operacion_Acelerometro::ESPERAR_LECTURA_ACEL):
		{
			tiempo_actual_peticion= HAL_GetTick()-tiempo_inicial_peticion;

			//Si se cumplio el tiempo de espera, entonces cambio de estado
			if( tiempo_actual_peticion > TIEMPO_ESPERA_ACELEROMETRO )
			{
				proximo_estado(Operacion_Acelerometro::REALIZAR_PETICION);
				//reseteo las variables de control
		//		estado_lectura=Estado_Lectura::DATOS_LEIDOS;
			}
			else
			{
			    proximo_estado(Operacion_Acelerometro::ESPERAR_LECTURA_ACEL);
			}
		}
		break;
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		case(Operacion_Acelerometro::REALIZAR_PETICION):
		{
			//si se ejecuto correctamente la solicitud de datos al acelerometro por IRQ,
			//entonces cambio de estado
			if(solicitar_datos_mpu()==true)
			{
				tiempo_inicial_peticion=HAL_GetTick();
				proximo_estado(Operacion_Acelerometro::LEER_DATOS_RECIBIDOS);
			}
			else
			{
				proximo_estado(Operacion_Acelerometro::REALIZAR_PETICION, Operacion_Acelerometro::ESPERAR_LECTURA_ACEL, TIME_OUT_OP_ACEL);
			}
		}
		break;
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		case(Operacion_Acelerometro::LEER_DATOS_RECIBIDOS):
		{
			resp=leer_datos_recibidos(datos_escalados,datos_kalman,datos_raw_aux);

			if( resp == false )
			{
				proximo_estado(Operacion_Acelerometro::LEER_DATOS_RECIBIDOS, Operacion_Acelerometro::ESPERAR_LECTURA_ACEL, TIME_OUT_OP_ACEL);
			}
			else
			{
				//si hubo un ERROR en la lectura del acelerometro entonces paso al estado
				//notificacion de falla de conexion
				if((datos_escalados->acelerometro.x==0)&&(datos_escalados->acelerometro.y==0)&&
				   (datos_escalados->acelerometro.z==0))
				{
					proximo_estado(Operacion_Acelerometro::NOTIFICAR_SIN_SENAL);
				}
				else
				{
					//si se recibieron correctamente los datos del acelerometro, entonces cambio de estado
					proximo_estado(Operacion_Acelerometro::ESPERAR_LECTURA_ACEL);
				}
			}
		}
		break;
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		case(Operacion_Acelerometro::NOTIFICAR_SIN_SENAL):
		{
			//SI NO HAY SE�AL DE ACELEROMETRO
			tiempo_a_prender_led_acel=HAL_GetTick()-tiempo_i_prender_led_acel;

			if(tiempo_a_prender_led_acel>TIEMPO_ESPERA_LED_ACEL)
			{
				//el gsm no tiene se�al entonces parpadea el led del GSM
				HAL_GPIO_TogglePin(led_acel_puerto,led_acel_pin);

				tiempo_i_prender_led_acel=HAL_GetTick();
			}

			proximo_estado(Operacion_Acelerometro::ESPERAR_LECTURA_ACEL);
		}
		break;
		///////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////
		default:
			LOG("Estado incorrecto!!! [%d]", (int)op_acel);
		break;
		///////////////////////////////////////////////////////////////////////////////////////////
	}

	return resp;
}



/***************************************************************************************************/
Controlador_Acel::Resultado Controlador_Acel::iniciar( Dispositivo DeviceNumber, Sensibilidad_Acelerometro AccelerometerSensitivity, Sensibilidad_Giroscopio GyroscopeSensitivity)
{
	uint8_t WHO_AM_I = (uint8_t)MPU6050_WHO_AM_I;
	uint8_t temp;
	uint8_t d[2];

	/* Format I2C address */
	this->i2c_direccion_disp = MPU6050_I2C_ADDR | (uint8_t)DeviceNumber;

	/* Check if device is connected */
	//if(HAL_I2C_IsDeviceReady(Handle,address,2,5)!=HAL_OK)
	if(HAL_I2C_IsDeviceReady( this->ptr_I2C_x, this->i2c_direccion_disp, 2, I2C_TIMEOUT) != HAL_OK )
	{
		return Controlador_Acel::Resultado::ERROR;

	}

	/* Check who am I */
	//------------------
	/* Send address */
	if(HAL_I2C_Master_Transmit( this->ptr_I2C_x, this->i2c_direccion_disp, &WHO_AM_I, 1, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	/* Receive multiple byte */
	if(HAL_I2C_Master_Receive( this->ptr_I2C_x, this->i2c_direccion_disp, &temp, 1, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	/* Checking */
	if(temp != MPU6050_I_AM)
	{
		/* Return ERROR */
		return Controlador_Acel::Resultado::DISPOSITIVO_INVALIDO;
	}

	//------------------
	/* Wakeup Controlador_Acel */
	//------------------
	/* Format array to send */
	d[0] = MPU6050_PWR_MGMT_1;
	d[1] = 0x00;

	/* Try to transmit via I2C */
	if(HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, (uint8_t *)d, 2, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }	//------------------

	/* Set sample rate to 1kHz */
	if(configurar_rango_datos( MPU6050_DataRate_1KHz )==Controlador_Acel::Resultado::ERROR)
		return Controlador_Acel::Resultado::ERROR;

	/* Config accelerometer */
	if(configurar_acelerometro( AccelerometerSensitivity)==Controlador_Acel::Resultado::ERROR)
		return Controlador_Acel::Resultado::ERROR;

	/* Config Gyroscope */
	if(configurar_giroscopio(GyroscopeSensitivity )==Controlador_Acel::Resultado::ERROR)
			return Controlador_Acel::Resultado::ERROR;

	/* Return OK */
	return Controlador_Acel::Resultado::OK;

}

/***************************************************************************************************/
bool Controlador_Acel::solicitar_datos_mpu()
{

	uint8_t reg = MPU6050_ACCEL_XOUT_H;

	if(estado_lectura==Estado_Lectura::DATOS_LEIDOS)
	{
		//se realiza el pedido de transmit en forma no bloqueante, que se  solicita la lectrua del acel por irq
		if(solicitud_transmit_realizada==false)
		{
			if(HAL_I2C_Master_Transmit(this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &reg, 1, I2C_TIMEOUT) != HAL_OK)
				return false;
			else
				solicitud_transmit_realizada=true;
		}

		//se realiza el pedido del receive en forma no bloqueante, se recibe la confirmacion del transmit
		if(HAL_I2C_Master_Receive_IT(this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, datos_array, sizeof(datos_array))!= HAL_OK)
			return false;
		else
			solicitud_transmit_realizada=false;

		/*while(HAL_I2C_Master_Transmit(this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &reg, 1, I2C_TIMEOUT) != HAL_OK);
		while(HAL_I2C_Master_Receive_IT(this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, datos_array, sizeof(datos_array))!= HAL_OK);
*/
		__HAL_I2C_DISABLE_IT(this->ptr_I2C_x, I2C_IT_EVT |I2C_IT_BUF| I2C_IT_ERR);
		//============================== Region Critica==========================
		estado_lectura=Estado_Lectura::DATOS_SOLICITADOS;
		//======================================================================
		__HAL_I2C_ENABLE_IT(this->ptr_I2C_x, I2C_IT_EVT |I2C_IT_BUF| I2C_IT_ERR);

		return true;
	}
	else
	{
		return false;
	}


}
/***************************************************************************************************/
void Controlador_Acel::recibir_datos_callback()
{
	int16_t temp;


	if( estado_lectura==Estado_Lectura::DATOS_SOLICITADOS )
	{
		datos_raw.tiempo_entre_medicion+=HAL_GetTick()-tiempo_incial_entre_medicion;
		tiempo_incial_entre_medicion=HAL_GetTick();

		//se resetea la variable datos_raw.tiempo_entre_medicion si supero el limite de lo permitido para el tipo
		//Uint32
		if( datos_raw.tiempo_entre_medicion>=LIMITE_UINT32_T)
			datos_raw.tiempo_entre_medicion=0;

		/* Format accelerometer data */
		datos_raw.acel_X = (int16_t)(datos_array[0] << 8 | datos_array[1]);
		datos_raw.acel_Y = (int16_t)(datos_array[2] << 8 | datos_array[3]);
		datos_raw.acel_Z = (int16_t)(datos_array[4] << 8 | datos_array[5]);

		/* Format */
		datos_raw.giro_X = (int16_t)(datos_array[8] << 8 | datos_array[9]);
		datos_raw.giro_Y = (int16_t)(datos_array[10] << 8 | datos_array[11]);
		datos_raw.giro_Z = (int16_t)(datos_array[12] << 8 | datos_array[13]);

		/* Format temperature */
		temp = (datos_array[6] << 8 | datos_array[7]);
		datos_raw.temperatura = (int16_t)((float)((int16_t)temp) / (float)340.0 + (float)36.53);

		estado_lectura=Estado_Lectura::DATOS_RECIBIDOS;
	}


}
/***************************************************************************************************/
bool Controlador_Acel::leer_datos_recibidos(Mpu_Datos_Escalados *datos_escalados,Mpu_Datos_Escalados *datos_kalman,Mpu_Datos_Raw *datos_raw_aux)
{
	if(estado_lectura==Estado_Lectura::DATOS_RECIBIDOS)
	{

		//deshabilito el canal de recepcion de I2C
		__HAL_I2C_DISABLE(this->ptr_I2C_x);
	    //============================== Region Critica==========================
		*datos_raw_aux=datos_raw;
		estado_lectura=Estado_Lectura::DATOS_LEIDOS;
		//==========================================================================
		__HAL_I2C_DISABLE(this->ptr_I2C_x);

		//para evitar problemas de concurrencias almaceno los valores leidos en variables temporales
		datos_escalados->acelerometro.x=escalar_acelerometro(datos_raw_aux->acel_X,config.acel_offset_X,config.acel_escala_min_X,config.acel_escala_max_X);
		datos_escalados->acelerometro.y=escalar_acelerometro(datos_raw_aux->acel_Y,config.acel_offset_Y,config.acel_escala_min_Y,config.acel_escala_max_Y);
		datos_escalados->acelerometro.z=escalar_acelerometro(datos_raw_aux->acel_Z,config.acel_offset_Z,config.acel_escala_min_Z,config.acel_escala_max_Z);

		datos_escalados->giroscopio.x=((float)(datos_raw_aux->giro_X+config.giro_offset_X))*this->Gyro_Mult;
		datos_escalados->giroscopio.y=((float)(datos_raw_aux->giro_Y+config.giro_offset_Y))*this->Gyro_Mult;
		datos_escalados->giroscopio.z=((float)(datos_raw_aux->giro_Z+config.giro_offset_Z))*this->Gyro_Mult;

		//Aplico filtro de Kalman y de Ventana de Umbrales
		aplicar_filtros_acel_x(datos_escalados->acelerometro.x,&(datos_kalman->acelerometro.x));
		aplicar_filtros_acel_y(datos_escalados->acelerometro.y,&(datos_kalman->acelerometro.y));
		aplicar_filtros_acel_z(datos_escalados->acelerometro.z,&(datos_kalman->acelerometro.z));

		datos_escalados->tiempo_medicion=datos_raw_aux->tiempo_entre_medicion;
		datos_escalados->temperatura=datos_raw_aux->temperatura;

		datos_kalman->tiempo_medicion=datos_raw_aux->tiempo_entre_medicion;
    	datos_kalman->temperatura=datos_escalados->temperatura;

    	//calculo la norma del acelerometro y del giroscopo
		calcular_norma(datos_escalados);
		calcular_angulo(datos_escalados);

		calcular_norma(datos_kalman);
		calcular_angulo(datos_kalman);


		return true;
	}
	return false;

}

/***************************************************************************************************/

Controlador_Acel::Resultado Controlador_Acel::configurar_rango_datos( uint8_t rate )
{
	uint8_t d[2];
	/* Format array to send */
	d[0] = MPU6050_SMPLRT_DIV;
	d[1] = rate;

	/* Set data sample rate */
	while( HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, (uint8_t *)d, 2, I2C_TIMEOUT ) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	/* Return OK */
	return Controlador_Acel::Resultado::OK;
}

/***************************************************************************************************/

Controlador_Acel::Resultado Controlador_Acel::configurar_acelerometro( Sensibilidad_Acelerometro sensibilidad_acelerometro )
{
	uint8_t temp;
	uint8_t regAdd =(uint8_t )MPU6050_ACCEL_CONFIG;

	/* Config accelerometer */
	while(HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &regAdd, 1, I2C_TIMEOUT ) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	while(HAL_I2C_Master_Receive( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &temp, 1, I2C_TIMEOUT ) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }	temp = (temp & 0xE7) | (uint8_t) sensibilidad_acelerometro << 3;
	while(HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &temp, 1, I2C_TIMEOUT ) != HAL_OK)
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	/* Set sensitivities for multiplying gyro and accelerometer data */
	switch ( sensibilidad_acelerometro)
	{
		case Sensibilidad_Acelerometro::RANGO_2G:
			this->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_2;
			break;
		case Sensibilidad_Acelerometro::RANGO_4G:
			this->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_4;
			break;
		case Sensibilidad_Acelerometro::RANGO_8G:
			this->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_8;
			break;
		case Sensibilidad_Acelerometro::RANGO_16G:
			this->Acce_Mult = (float)1 / MPU6050_ACCE_SENS_16;
			break;
		default:
			break;
		}

	/* Return OK */
	return Controlador_Acel::Resultado::OK;
}

/***************************************************************************************************/

Controlador_Acel::Resultado Controlador_Acel::configurar_giroscopio( Sensibilidad_Giroscopio sensibilidad_giroscopio )
{
	uint8_t temp;
	uint8_t regAdd =(uint8_t )MPU6050_GYRO_CONFIG;

	/* Config gyroscope */
	while(HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &regAdd, 1, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }
	while(HAL_I2C_Master_Receive( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &temp, 1, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	temp = (temp & 0xE7) | (uint8_t)sensibilidad_giroscopio << 3;
	while(HAL_I2C_Master_Transmit( this->ptr_I2C_x, (uint16_t)this->i2c_direccion_disp, &temp, 1, I2C_TIMEOUT) != HAL_OK )
	{
		/* Error_Handler() function is called when Timeout ERROR occurs.
		   When Acknowledge failure occurs (Slave don't acknowledge it's address)
		   Master restarts communication */
		if (HAL_I2C_GetError(this->ptr_I2C_x) != HAL_I2C_ERROR_AF)
		{
			return Controlador_Acel::Resultado::ERROR;
		}
	 }

	switch (sensibilidad_giroscopio) {
			case Sensibilidad_Giroscopio::RANGO_250s:
				this->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_250;
				break;
			case Sensibilidad_Giroscopio::RANGO_500s:
				this->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_500;
				break;
			case Sensibilidad_Giroscopio::RANGO_1000s:
				this->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_1000;
				break;
			case Sensibilidad_Giroscopio::RANGO_2000s:
				this->Gyro_Mult = (float)1 / MPU6050_GYRO_SENS_2000;
				break;
			default:
				break;
		}
	/* Return OK */
	return Controlador_Acel::Resultado::OK;
}

/***************************************************************************************************/

float Controlador_Acel::escalar_acelerometro( int16_t valor, int16_t offset,int16_t min_escala,int16_t max_escala)
{
	if (valor < 0)
	{
		return -(valor - offset) /(float) (min_escala - offset);
	}
	else
	{
		return (valor - offset) /(float) (max_escala - offset);
	}
}

/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_acel_x(float valor,float *destino)
{
	float aux=0;

	//aplico filtro de kalman
	aux = Kalman_Accelerometer_X.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_ACELEROMETRO_MIN)&&(aux<UMBRAL_ACELEROMETRO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}

/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_acel_y(float valor,float *destino)
{
	float aux=0;

	//aplico filtro de kalman
	aux = 	Kalman_Accelerometer_Y.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_ACELEROMETRO_MIN)&&(aux<UMBRAL_ACELEROMETRO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}

/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_acel_z(float valor,float *destino)
{
	float aux=0;

	//aplico filtro de kalman
	aux = Kalman_Accelerometer_Z.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_ACELEROMETRO_MIN)&&(aux<UMBRAL_ACELEROMETRO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}
/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_giro_x(float valor,float *destino)
{
	float aux=0;

	//aplico filtro de kalman
	aux = Kalman_Gyroscope_X.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_GIROSCOPO_MIN)&&(aux<UMBRAL_GIROSCOPO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}

/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_giro_y(float valor,float *destino)
{
	int32_t aux=0;

	//aplico filtro de kalman
	aux = Kalman_Gyroscope_Y.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_GIROSCOPO_MIN)&&(aux<UMBRAL_GIROSCOPO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}

/***************************************************************************************************/
void Controlador_Acel::aplicar_filtros_giro_z(float valor,float *destino)
{
	float aux=0;

	//aplico filtro de kalman
	aux = 	Kalman_Gyroscope_Z.filtro(valor,0);

	//Aplico ventana de umbrales minimo y maximo
	if((aux>UMBRAL_GIROSCOPO_MIN)&&(aux<UMBRAL_GIROSCOPO_MAX))
	{
		*destino=0;
	}
	else
	{
		*destino=aux;
	}
}
/***************************************************************************************************/
void Controlador_Acel::calcular_norma(Mpu_Datos_Escalados * datos)
{
	double pow_x=datos->acelerometro.x*datos->acelerometro.x;
	double pow_y=datos->acelerometro.y*datos->acelerometro.y;
	double pow_z=datos->acelerometro.z*datos->acelerometro.z;
	datos->acelerometro.norma=(float)sqrt(pow_x+pow_y+pow_z);

	pow_x=datos->giroscopio.x*datos->giroscopio.x;
	pow_y=datos->giroscopio.y*datos->giroscopio.y;
	pow_z=datos->giroscopio.z*datos->giroscopio.z;
	datos->giroscopio.norma=(float)sqrt(pow_x+pow_y+pow_z);

}

//*************************************************************************************************
void Controlador_Acel::calcular_angulo(Mpu_Datos_Escalados *datos)
{
	if(bandera_primera_vez==true)
	{
		establecer_primera_lectura_acel();
		datos->angulo=calcular_angulo_coseno(datos->acelerometro,datos->acelerometro);
		bandera_primera_vez=false;
	}
	else
	{
		datos->angulo=calcular_angulo_coseno(datos->acelerometro,acel_primera_vez);
	}

}

/***************************************************************************************************/
 int Controlador_Acel::calcular_angulo_coseno(Valor_Eje op1, Valor_Eje op2)
{
	float numerador=0;
	float denominador=0;
	double coseno=0;
	double arco_radianes=0;
	float angulo=0;

	volatile Valor_Eje op1Ms,op2Ms;

	op1Ms.x=op1.x*9.81;
	op1Ms.y=op1.y*9.81;
	op1Ms.z=op1.z*9.81;

	op2Ms.x=op2.x*9.81;
	op2Ms.y=op2.y*9.81;
	op2Ms.z=op2.z*9.81;

	//NOTA: Para que no que importe la posicion en que se coloque, se deberá harcodear las coordenadas
	//      de op1, como se estaria el prototipo de pie en la cintura, para que den el angulo inicial en cero
	numerador=op1Ms.x*op2Ms.x+op1Ms.y*op2Ms.y+op1Ms.z*op2Ms.z;
	denominador=(sqrt(op1Ms.x*op1Ms.x+op1Ms.y*op1Ms.y+op1Ms.z*op1Ms.z))*(sqrt(op2Ms.x*op2Ms.x+op2Ms.y*op2Ms.y+op2Ms.z*op2Ms.z));

	coseno=numerador/denominador;

	if((coseno<=1)&& (coseno>=-1))
	{
		arco_radianes=acos(coseno);
		angulo=arco_radianes*RAD_TO_DEG;
	}
	else
	{
		angulo=0;
	}

	return (int)angulo;
}

/***************************************************************************************************/
void Controlador_Acel::establecer_primera_lectura_acel()
{
	acel_primera_vez.x=1;
	acel_primera_vez.y=0;
	acel_primera_vez.z=0;
}
