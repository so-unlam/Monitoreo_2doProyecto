#include "App/Bibliotecas/Kalman.hpp"

/***************************************************************************************************/

Kalman::Kalman()
{
	this->R     =  0.0;
	this->Q     =  0.0;
	this->A     =  1;
	this->B     =  0;
	this->C     =  1;
	this->x     = -1.0;
	this->cov   = -1.0;
	this->flags =  0;
}

/***************************************************************************************************/

Kalman::Kalman( float R, float Q )
{
	this->R     =  R;
	this->Q     =  Q;
	this->A     =  1;
	this->B     =  0;
	this->C     =  1;
	this->x     = -1.0;
	this->cov   = -1.0;
	this->flags =  0;
}

/***************************************************************************************************/

Kalman::~Kalman()
{


}

/***************************************************************************************************/

float Kalman::filtro( float valor, float u )
{
    if ( this->flags == 0 )
    {
    	this->x = 1 / this->C * valor;
    	this->cov = 1 / this->C * this->Q * ( 1 / this->C );
    	this->flags = 1;
    }
    else
    {
    	// Compute prediction
    	float predX = this->A * this->x + this->B * u;
    	float predCov = this->A * this->cov * this->A + this->R;

    	// Kalman gain
    	float K = predCov * this->C * (1 / (this->C * predCov * this->C + this->Q));

    	// Correction
    	this->x = predX + K * (valor - this->C * predX);
    	this->cov = predCov - K * this->C * predCov;
	}
    return this->x;
}

/***************************************************************************************************/

void Kalman::setRQ( float R, float Q )
{
	this->Q     = Q;
	this->R     = R;

	this->A     =  1;
	this->B     =  0;
	this->C     =  1;
	this->x     = -1.0;
	this->cov   = -1.0;
	this->flags =  0;

}
/***************************************************************************************************/

float Kalman::GetUltimaMedicion( void )
{
	return this->x;
}

/***************************************************************************************************/

void Kalman::setValorRuidoDeMedicion( float Q )
{
	this->Q = Q;
}

/***************************************************************************************************/

void Kalman::setValorRuidoDeProceso( float R )
{
	this->R = R;
}

/***************************************************************************************************/

float Kalman::getValorRuidoDeMedicion( void )
{
	return this->Q;
}

/***************************************************************************************************/

float Kalman::getValorRuidoDeProceso( void )
{
	return this->R;
}

/***************************************************************************************************/
