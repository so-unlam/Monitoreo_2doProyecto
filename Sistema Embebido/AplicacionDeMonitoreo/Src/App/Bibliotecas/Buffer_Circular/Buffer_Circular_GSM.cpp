/*
 * Buffer_Circular_GSM.cpp
 *
 *  Created on: 8 Aug 2019
 *      Author: Esteban
 */

#include <App/Bibliotecas/Buffer_Circular/Buffer_Circular_GSM.h>

/***************************************************************************************************/
Buffer_Circular_GSM::Buffer_Circular_GSM():Buffer_Circular_Base()
{

	this->m_size     = BUFFER_SIZE_GSM;
	this->m_capacity = BUFFER_SIZE_GSM;
}
/***************************************************************************************************/
Buffer_Circular_GSM::~Buffer_Circular_GSM()
{

}
/***************************************************************************************************/
bool Buffer_Circular_GSM::fifo_push(Peticion_Operacion_Gsm* peticion)
{
    if( fifo_is_full() == true )
    {
    	return false;
    }

    //se realiza la copia de la estructura de la peticion en el buffer circular
    //de GSM
    m_buffer[m_tail] = *peticion;
    m_tail++;

    if( m_tail == m_size )
    	m_tail = 0;

    m_capacity --;

    return true;
}

/***************************************************************************************************/
bool Buffer_Circular_GSM::fifo_pop(Peticion_Operacion_Gsm* peticion)
{
    if( fifo_is_empty() == true )
    {
        return false;
    }

    //se realiza la copia de la primera estructura de la peticion pendiente, almacenada
    //en el buffer circular, al contenido del puntero peticion
    *peticion = m_buffer[m_head];

    m_head++;
    if( m_head == m_size )
    	m_head = 0;

    m_capacity ++;

    return true;
}


/***************************************************************************************************/
void Buffer_Circular_GSM::flush()
{
	m_head 		= 0;
	m_tail 		= 0;
	m_size 		= BUFFER_SIZE_GSM;
	m_capacity 	= BUFFER_SIZE_GSM;
}


