/*
 * Buffer_Circular_Serial.cpp
 *
 *  Created on: 7 Aug 2019
 *      Author: Esteban
 */


#include <App/Bibliotecas/Buffer_Circular/Buffer_Circular_Serial.h>

/***************************************************************************************************/
Buffer_Circular_Serial::Buffer_Circular_Serial():Buffer_Circular_Base()
{

	this->m_size     = BUFFER_SIZE_SERIAL;
	this->m_capacity = BUFFER_SIZE_SERIAL;
}
/***************************************************************************************************/
Buffer_Circular_Serial::~Buffer_Circular_Serial()
{

}
/***************************************************************************************************/
bool Buffer_Circular_Serial::fifo_push_byte(byte_t byte)
{
    if( fifo_is_full() == true )
    {
    	return false;
    }

    m_buffer[m_tail] = byte;
    m_tail++;

    if( m_tail == m_size )
    	m_tail = 0;

    m_capacity --;

    return true;
}

/***************************************************************************************************/
size_t Buffer_Circular_Serial::fifo_push_bytes(byte_t* bytes, size_t count)
{
    for( size_t i = 0; i < count; i++ )
    {
        if( fifo_push_byte(bytes[i]) == false )
        {
            return i;
        }
    }

    return count;
}


/***************************************************************************************************/
bool Buffer_Circular_Serial::fifo_pop_byte(byte_t* byte)
{
    if( fifo_is_empty() == true )
    {
        return false;
    }

    *byte = m_buffer[m_head];

    m_head++;
    if( m_head == m_size )
    	m_head = 0;

    m_capacity ++;

    return true;
}

/***************************************************************************************************/
size_t Buffer_Circular_Serial::fifo_pop_bytes(byte_t* bytes, size_t count)
{
    for( size_t i = 0; i < count; i++ )
    {
        if( fifo_pop_byte(bytes + i) == false )
        {
            return i;
        }
    }

    return count;
}

/***************************************************************************************************/
void Buffer_Circular_Serial::flush()
{
	m_head 		= 0;
	m_tail 		= 0;
	m_size 		= BUFFER_SIZE_SERIAL;
	m_capacity 	= BUFFER_SIZE_SERIAL;
}
