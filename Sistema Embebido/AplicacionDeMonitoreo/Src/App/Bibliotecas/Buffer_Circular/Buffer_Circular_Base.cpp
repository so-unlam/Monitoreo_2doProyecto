#include <App/Bibliotecas/Buffer_Circular/Buffer_Circular_Base.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

/***************************************************************************************************/
Buffer_Circular_Base::Buffer_Circular_Base()
{
	m_head 		= 0;
	m_tail 		= 0;
	m_size 		= 0;
	m_capacity 	= 0;
}

/***************************************************************************************************/
Buffer_Circular_Base::~Buffer_Circular_Base()
{

}

/***************************************************************************************************/
bool Buffer_Circular_Base::fifo_is_empty()
{
	return (m_capacity == m_size)? (true):(false);
}

/***************************************************************************************************/
bool Buffer_Circular_Base::fifo_is_full()
{
	return (m_capacity <= 0)? (true):(false);
}

/***************************************************************************************************/
size_t Buffer_Circular_Base::fifo_bytes_filled()
{
	return (m_size - m_capacity);
}

/***************************************************************************************************/
size_t Buffer_Circular_Base::fifo_bytes_free()
{
	return m_capacity;
}

/***************************************************************************************************/
size_t Buffer_Circular_Base::get_fifo_size()
{
	return m_size;
}

/***************************************************************************************************/
uint8_t Buffer_Circular_Base::get_filled_percentage()
{
	uint8_t porcentage =static_cast <uint8_t>(fifo_bytes_filled()*get_fifo_size()/100);
	return porcentage;
}

/***************************************************************************************************/
bool Buffer_Circular_Base::analyze_full_buffer_percentage()
{
	if( get_filled_percentage()<=PORCENTAJE_MINIMO_BUFFER )
	{
		return true;
	}
	else
	{
		return false;
	}
}
