/*
 * Detector_Caidas.cpp
 *
 *  Created on: Feb 16, 2019
 *      Author: wav
 */


#include <App/Bibliotecas/Detector_Caidas.hpp>

/***************************************************************************************************/

Detector_Caidas::Detector_Caidas()
{
	this->tiempoImpactoDetectado               = 0;
    this->tiempoCaidaLibreDetectada            = 0;
	this->tiempoEsperaReposoFinalCompletada    = 0;
	this->tiempoCaidaDetectada                 = 0;
	this->tiempoRecuperacion                   = 0;

	this->msgCriticoEnviado    = false;
	this->mostrarFalso         = true;

	this->valores.sumaAngulo           = 0;
	this->valores.cantMedicionesAngulo = 0;

	this->faseActual = Fase::Incial;
}

/***************************************************************************************************/

Detector_Caidas::~Detector_Caidas()
{

}

/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::detectarLaCaida( Mpu_Datos_Escalados &cad )
{
	Detector_Caidas::Resultado r = Detector_Caidas::Resultado::SinNovedades;

	valores.normalAcelerometro=cad.acelerometro.norma;
	valores.angulo=cad.angulo;
	valores.tiempo=cad.tiempo_medicion;


	switch (this->faseActual)
	{
		case Fase::Incial:
			r = this->faseInicializacion();
			break;

		case Fase::CaidaLibre:
			r = this->faseCaidaLibre();
			break;

		case Fase::EsperarReposoFinal:
			r = this->faseEsperarReposoFinal();
			break;

		case Fase::Impacto:
			r = this->faseImpacto();
			break;

		case Fase::ReposoFinal:
			r = this->faseReposoFinal();
			break;

    	case Fase::Recuperacion:
			r = this->faseRecuperacion();
			break;
	}

	return r;
}
/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseInicializacion( void )
{
	LOG("\nInicializando Algoritmo de Deteccion de Caidas....\n");
	this->faseActual = Fase::CaidaLibre;

	return Detector_Caidas::Resultado::SinNovedades;
}


/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseCaidaLibre( void )
{
	switch(this->detectarCaidaLibre())
	{
		case FaseEstado::SEGUIR_FASE_SIGUIENTE: // this.SEGUIR_FASE_SIGUIENTE:
			this->faseActual = Fase::Impacto; //this.FASE_IMPACTO;
			break;

		case FaseEstado::FaseInicial: // this.SEGUIR_FASE_INICIAL:
			this->faseActual = Fase::CaidaLibre; // this.FASE_CAIDALIBRE;
			break;

		default:
			break;
	}

	return Detector_Caidas::Resultado::SinNovedades;
}

/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseImpacto( void )
{

	switch( this->detectarImpacto() )
	{
		case FaseEstado::SEGUIR_FASE_SIGUIENTE: //  this.SEGUIR_FASE_SIGUIENTE:
			this->faseActual = Fase::EsperarReposoFinal; //FASE_ESPERAR_REPOSOFINAL;
			break;

		case FaseEstado::FaseInicial:  // this.SEGUIR_FASE_INICIAL:
			this->faseActual = Fase::CaidaLibre;  ///FASE_CAIDALIBRE;
			break;

		default:
			break;
	}

	return Detector_Caidas::Resultado::SinNovedades;
}

/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseEsperarReposoFinal( void )
{
	//cantidad de milisegundos a esperar para analizar la fase resposo final para detectar una caida
	const uint32_t TIEMPO_ESPERA_CAIDA = 1500;

 	if(this->esperarReposoFinal( TIEMPO_ESPERA_CAIDA )== false)
 		this->faseActual = Fase::ReposoFinal;  ///FASE_REPOSOFINAL;

	return Detector_Caidas::Resultado::SinNovedades;
}

/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseReposoFinal( void )
{
	Detector_Caidas::Resultado resp=Detector_Caidas::Resultado::SinNovedades;

	switch( this->detectarReposoFinal() )
	{
		case  FaseEstado::FaseInicial:   // this.SEGUIR_FASE_INICIAL:
			this->faseActual = Fase::CaidaLibre; //FASE_CAIDALIBRE;
		   break;

		case FaseEstado::SEGUIR_FASE_SIGUIENTE:  // this.SEGUIR_FASE_SIGUIENTE:
			//aca se detecto una caida
			//this.notificarCaida();
			this->faseActual = Fase::Recuperacion; //FASE_RECUPERACION;

			resp=  Detector_Caidas::Resultado::DetectaCaida;
			break;

		default:
			break;
	}

	return resp;
}

/***************************************************************************************************/

Detector_Caidas::Resultado Detector_Caidas::faseRecuperacion( void )
{
	Detector_Caidas::Resultado r = Detector_Caidas::Resultado::SinNovedades;

	switch(this->detectarRecuperacion() )
	{
		case FaseEstado::FaseInicial:   /// this.SEGUIR_FASE_INICIAL:
			if(this->verificarCaidaCritica()==true)
				r = Detector_Caidas::Resultado::DetectaCaidaCritica;
			break;
		case FaseEstado::ContinuarEnFaceActual:   //this.SEGUIR_FASE_ACTUAL:
			break;

		case  FaseEstado::SEGUIR_FASE_SIGUIENTE:  // this.SEGUIR_FASE_SIGUIENTE:
			//aca se detecto una caida
			this->faseActual = Fase::CaidaLibre; //.FASE_CAIDALIBRE;
			r = Detector_Caidas::Resultado::DetectaRecupero;
			this->msgCriticoEnviado=false;
			break;
		default:
			break;
	}

	return r;
}

/***************************************************************************************************/

Detector_Caidas::FaseEstado Detector_Caidas::detectarCaidaLibre ( void )
{
	Detector_Caidas::FaseEstado fe;

	const float UMBRAL_CAIDALIBRE_ACC = 0.55; //en G

	if(valores.normalAcelerometro <= UMBRAL_CAIDALIBRE_ACC)
	{
		//Aca deben tomarse el tiempo de la fase
		this->tiempoCaidaLibreDetectada = valores.tiempo;

		LOG("*LIBRE!\r\n");
		//LOG("*CAt:%u|v:%f\r\n",valores.tiempo,valores.normalAcelerometro);
		fe = FaseEstado::SEGUIR_FASE_SIGUIENTE; // this.SEGUIR_FASE_SIGUIENTE;
	}
	else
	{
		//LOG("FALSA CAIDA LIBRE DETECTADO!!!\r\n");
		fe = FaseEstado::FaseInicial; // this.SEGUIR_FASE_INICIAL;
	}

	return fe;
}

/***************************************************************************************************/

Detector_Caidas::FaseEstado Detector_Caidas::detectarImpacto( void )
{
	Detector_Caidas::FaseEstado fe;

   const float UMBRAL_IMPACTO_ACC		       		= 2.5;  //en G 3.3
   const uint32_t UMBRAL_CAIDALIBRE_IMPACTO_TIEMPO	= 500;  //en seg 200 mseg

   uint32_t diferenciaTiempo = (this->valores.tiempo - this->tiempoCaidaLibreDetectada);


   if(diferenciaTiempo <= UMBRAL_CAIDALIBRE_IMPACTO_TIEMPO)
   {
	   if(valores.normalAcelerometro >= UMBRAL_IMPACTO_ACC)
	   {
			this->tiempoImpactoDetectado = this->valores.tiempo;
			//LOG("IMPACTO DETECTADO!!!\r\n");
			LOG("*IMPACTO\r\n");
			HAL_Delay(5);
			fe = FaseEstado::SEGUIR_FASE_SIGUIENTE; // SEGUIR_FASE_SIGUIENTE;
	    }
	   else
	   {
		   fe = FaseEstado::ContinuarEnFaceActual; // SEGUIR_FASE_ACTUAL;
	   }
   }
   else
   {
	   //LOG("FALSO IMPACTO!!!\r\n");
	   fe = FaseEstado::FaseInicial; //SEGUIR_FASE_INICIAL;
   }

	return fe;
}

/***************************************************************************************************/

int Detector_Caidas::esperarReposoFinal( uint32_t umbralTiempoEspera )
{

	uint32_t diferenciaTiempo = (this->valores.tiempo - this->tiempoImpactoDetectado);

	if(diferenciaTiempo >= umbralTiempoEspera)
	{
		this->tiempoEsperaReposoFinalCompletada = this->valores.tiempo;
		LOG("*ESPERA\r\n");
		return false;

	}

	//LOG("ESPERANDO REPOSO FINAL!!\r\n");
	return true;
}

/***************************************************************************************************/

Detector_Caidas::FaseEstado Detector_Caidas::detectarReposoFinal( void )
{
	const float UMBRAL_REPOSOFINAL_ACC_MIN	    = 0.55; //en G
	const float UMBRAL_REPOSOFINAL_ACC_MAX	    = 2.5; //en G
	const float UMBRAL_REPOSOFINAL_ANGULO       = 65; 	//en grados
	const uint32_t UMBRAL_REPOSOFINAL_TIEMPO	= 800;//en segundos
	const int   CANT_MINIMA_MEDICIONES_ANGULO   = 5; //Cantidad minima de mediciones para poder tomar el promedio del angulo
	int 	   anguloFinalPromedio                   = 0;
	uint32_t   diferenciaTiempo 			         = 0;

 	this->valores.sumaAngulo = this->valores.sumaAngulo + this->valores.angulo;
	this->valores.cantMedicionesAngulo++;

	//Tomo por lo menos 5 mediciones para calcular el promedio
	if( this->valores.cantMedicionesAngulo < CANT_MINIMA_MEDICIONES_ANGULO )
		return Detector_Caidas::FaseEstado::ContinuarEnFaceActual; //SEGUIR_FASE_ACTUAL;

	anguloFinalPromedio = (this->valores.sumaAngulo / this->valores.cantMedicionesAngulo);

	LOG("angulo:%d\r\n",anguloFinalPromedio);

	if(((this->valores.normalAcelerometro < UMBRAL_REPOSOFINAL_ACC_MIN) ||
	    (this->valores.normalAcelerometro > UMBRAL_REPOSOFINAL_ACC_MAX))||
		(anguloFinalPromedio < UMBRAL_REPOSOFINAL_ANGULO))
	{
		//LOG("FALSO REPOSO FINAL!!\r\n");
		this->resetearAnguloPromedio();

		return Detector_Caidas::FaseEstado::FaseInicial; //this.SEGUIR_FASE_INICIAL;
	}

	diferenciaTiempo = (this->valores.tiempo - this->tiempoEsperaReposoFinalCompletada);

	if(diferenciaTiempo > UMBRAL_REPOSOFINAL_TIEMPO)
	{
		//ACA SE DETECTO QUE SE CAYO
		LOG("*REPOSO\r\n!!!!");
		this->tiempoCaidaDetectada = this->valores.tiempo;
		this->tiempoRecuperacion   = this->valores.tiempo;
		//console.log("tiempo caida"+this._tiempoCaidaDetectada);
		this->resetearAnguloPromedio();

		return Detector_Caidas::FaseEstado::SEGUIR_FASE_SIGUIENTE; //this.SEGUIR_FASE_SIGUIENTE;
	}

	return Detector_Caidas::FaseEstado::ContinuarEnFaceActual; //this.SEGUIR_FASE_ACTUAL;

}

/***************************************************************************************************/

Detector_Caidas::FaseEstado Detector_Caidas::detectarRecuperacion( void )
{
	const float UMBRAL_RECUPERACION_ACC_MIN	    = 0.55; //en G
	const float UMBRAL_RECUPERACION_ACC_MAX	    = 2.8; 	//en G
	const float UMBRAL_RECUPERACION_ANGULO      = 30; 	//en grados
	const uint32_t UMBRAL_RECUPERACION_TIEMPO	= 15000; //en milisegundos
	const int CANT_MINIMA_MEDICIONES_ANGULO   	= 5; 	//Cantidad minima de mediciones para poder tomar el promedio del angulo
	int 	 anguloFinalPromedio            	= 0;
	uint32_t diferenciaTiempo 	                = 0;


	if((valores.normalAcelerometro < UMBRAL_RECUPERACION_ACC_MIN) ||
	   (valores.normalAcelerometro > UMBRAL_RECUPERACION_ACC_MAX))
	{
		//ACA NO SE ENCUENTRA EL MUESTREO DENTRO DEL RANGO ACEPTABLE,
		//POR LO QUE SE DEBEN SEGUIR TOMANDO MUESTRAS
		//LOG("FALSA RECUPERACION!!!!\r\n");
		this->resetearAnguloPromedio();
		return Detector_Caidas::FaseEstado::ContinuarEnFaceActual; // this.SEGUIR_FASE_ACTUAL;
	}


 	this->valores.sumaAngulo = this->valores.sumaAngulo + this->valores.angulo;
	this->valores.cantMedicionesAngulo++;

	//Tomo por lo menos 5 mediciones para calcular el promedio
	if(this->valores.cantMedicionesAngulo<CANT_MINIMA_MEDICIONES_ANGULO)
		return Detector_Caidas::FaseEstado::ContinuarEnFaceActual; //this.SEGUIR_FASE_ACTUAL;

	anguloFinalPromedio=(this->valores.sumaAngulo/this->valores.cantMedicionesAngulo);

	LOG("angulo:%d\r\n",anguloFinalPromedio);

	diferenciaTiempo = (this->valores.tiempo - this->tiempoRecuperacion);

	if(diferenciaTiempo > UMBRAL_RECUPERACION_TIEMPO)
	{
		this->tiempoRecuperacion = this->valores.tiempo;

		if(anguloFinalPromedio <= UMBRAL_RECUPERACION_ANGULO)
		{
			//ACA SE LEVANTO
			LOG("*RECUPERA\r\n");
			this->resetearAnguloPromedio();
			return Detector_Caidas::FaseEstado::SEGUIR_FASE_SIGUIENTE; //this.SEGUIR_FASE_SIGUIENTE;
		}
		else
		{
			//ACA TODAVIA NO SE CUMPLIO EL ANGULO PROMEDIO PARA CONSIDERARLO QUE SE LEVANTO
			//LOG("CRITICA\r\n");

			this->resetearAnguloPromedio();

			return Detector_Caidas::FaseEstado::FaseInicial; //this.SEGUIR_FASE_INICIAL;
		}

	}
	//ACA TODAVIA NO SE TOMARON SUFICIENTES MUESTRAS PARA DETERMINAR EL ANGULO PRIMEDIO
	return Detector_Caidas::FaseEstado::ContinuarEnFaceActual; //this.SEGUIR_FASE_ACTUAL;

}

/***************************************************************************************************/

void Detector_Caidas::resetearAnguloPromedio(void)
{
	this->valores.sumaAngulo           = 0;
	this->valores.cantMedicionesAngulo = 0;
}

/***************************************************************************************************/

bool Detector_Caidas::verificarCaidaCritica()
{
	bool resp=false;
	//Cantidad de milisegundos a esperar desde que se detecto la caida para enviar un mensaje
	//critico si la persona sigue tirada en el suelo.
	const uint32_t TIEMPO_ENVIO_MSG_CRITICO= 5000;//10000 Apropiado
	uint32_t tiempoTotalCaido = this->tiempoRecuperacion-this->tiempoCaidaDetectada;

	LOG("tiempototalCaido:%u |msgCritico:%d\r\n",tiempoTotalCaido,this->msgCriticoEnviado);
	//si se detecto y la persona sigue tirada en el suelo, entonces se enviar un mensaje critico
	if ((tiempoTotalCaido>TIEMPO_ENVIO_MSG_CRITICO)&&(this->msgCriticoEnviado==false))
	{
		//COMO SE HABIA DETECTADO UNA CAIDA Y AUN NO SE LEVANTO ENVIO UNA ALERTA CRITICCA
		LOG("*SIGUE CAIDO\r\n");
		HAL_Delay(10);

		this->msgCriticoEnviado=true;

		resp=true;
	}
	return resp;
}

