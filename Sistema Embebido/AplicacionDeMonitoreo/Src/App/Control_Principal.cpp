/*
 * my_main.cpp
 *
 *  Created on: Oct 27, 2018
 *      Author: wav
 */

#include <App/Control_Principal.hpp>
/*
========================================================================================
========================================================================================
*/
 Main control_principal;
 extern UART_HandleTypeDef huart1;  //Bluethoot o CP2102
 extern UART_HandleTypeDef huart2;  //GPS
 extern UART_HandleTypeDef huart3;  //GSM

//Puntero al objeto del puerto serie utilizado para debug
//Serial * usart_debug=NULL;
extern Serial * usart_debug;

bool leyo=false;
uint32_t tiempo_inicio_boton=HAL_GetTick();		//tiempo inicial en que se apreto el boton de panico

uint8_t cant_pulsaciones_boton; //variable que cuenta la cantidad de veces que se presiona el
								//boton de panico en un iintervalo de tiempo

#define btoa(x) ((x)?"true":"false")

 /*
========================================================================================
========================================================================================
================================== METODOS DEL LENGUAJE C++ ============================
========================================================================================
*/

 /**************************************************************************************/
// Implemento el contructor del objeto de prueba Main.

Main::Main()
{
	this->argc = 0;
	this->argv[0] = (char *) '\0';

	this->b_interrupt=false;
	this->peticion_gsm_creada=false;
	this->activar_lectura_cont_gps=false;
	this->gps_leido=false;
	this->deteccion_caida_en_curso=false;

	this->comando_activo=Tipos_Comandos::INACTIVO;
	this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;

	this->tiempo_i_loop=HAL_GetTick();
	this->tiempo_a_loop=0;
	this->tiempo_i_espera_leer_gps=HAL_GetTick();
	this->tiempo_a_espera_leer_gps=0;
	this->cont_vueltas=0;

	this->tam_suceso_encriptado=strlen(SMD5_SUCESO_ACTUALIZADO)+1;
	snprintf((char *)this->clave_encriptacion,sizeof(this->clave_encriptacion),"%s",TOKEN_DISPOSITIVO);

}

/***************************************************************************************************/
Main::~Main()
{
}

/***************************************************************************************************/
int Main::loop()
{

	/*************************************************************************************************
	 *Despues de haber configurado los puertos Serial, instanciando los objetos asignados que le corresponde
	 *a cada uno de ellos, se procede verificar el funcionamiento de los dispositivos conectados a la Blue Pill
	 */

	LOG("inicializando Sistema...\r\n\r\n");

	this->comprobar_funcionamiento_HW();

	LOG("Esperando comandos...\r\n\r\n");

	//Este es el loop principal de la aplicacion del embebido
	while (!this->b_interrupt)
	{
		
		 this->tiempo_a_loop=HAL_GetTick() - this->tiempo_i_loop;

		 if(this->tiempo_a_loop  >= TIEMPO_ESPERA_LOOP)
		 {

			 //se compruab que la variable cont_vueltas no exceda el valor limite
			 //del tipo de datos uint32_t, de forma tal que no se produzca overflow
			 if(this->cont_vueltas==LIMITE_UINT32_T)
				 this->cont_vueltas=0;
			 else
				 this->cont_vueltas++;

			 this->leer_comando_bluetooth();
			 this->ejecutar_comando_bluetooth();

			 this->tiempo_i_loop = HAL_GetTick();

		 }

	}

	return 0;
}

/**************************************************************************************************
* ESTE METODO DETERMINA LA OCURRENCIA DE LOS EVENTOS EN EL DIAGRAMA DE ESTADO
***************************************************************************************************/
void Main::leer_comando_bluetooth()
{
	/************************************Comandos de Bluethoot:********************
	 * #MI : Inicia el muestreo de datos del aclerometro
	 * #MF : Finaliza el muestreo de datos del aclerometro
	 * #CI : Inicia el control de Caidas
	 * #CF : Finaliza el control de Caidas
	 * #LI : Iniciar llamada telefonica (SIM800L)
	 * #LF : Finalizar llamada telefonica (SIM800L)
	 * #SI : Envia un SMS a un numero telefonico (SIM800L)
	 * #PI : Envia un mensaje POST al servidor (SIM800L)
	 * #GI : Activa la lectura del GPS
	 * #GF : Desactiva la lectura del GPS
	 *****************************************************************************
	 */
	byte_t comando_op [100]={0};
	uint8_t indice=0;

	this->comando_activo = Tipos_Comandos::INACTIVO;

	//si se recibio alguna linea de un dispositivo serial como  Bluethoot o TTL
	//se analiza si es un comando de operacion segun el protocolo creado
	if(usart_bluetooth.read_line(comando_op,sizeof(comando_op))==0)
		return;

	 LOG("%s",comando_op);


	if(comando_op[indice]!='#')
		return;

	indice++;

	switch(comando_op[indice])
	{
		case 'M':
			indice++;

			if(comando_op[indice]=='I')
			{
				this->comando_activo = Tipos_Comandos::INICIAR_MUESTREO;
			}
			else if(comando_op[indice]=='F')
			{
				this->comando_activo = Tipos_Comandos::FINALIZAR_MUESTREO;
			}
			break;
		case 'C':
			indice++;

			if(comando_op[indice]=='I')
			{
				this->comando_activo = Tipos_Comandos::INICIAR_CONTROL;
				LOG("INICIANDO CONTROL\r\n");
			}
			else if(comando_op[indice]=='F')
			{
				this->comando_activo = Tipos_Comandos::FINALIZAR_CONTROL;
				LOG("FINALIZANDO CONTROL\r\n");
			}
			break;

		case 'L':
			indice++;

			if(comando_op[indice]=='I')
			{
				this->comando_activo = Tipos_Comandos::INICIAR_LLAMADA;
				LOG("LLAMANDO...\r\n");
			}
			else if(comando_op[indice]=='F')
			{
				this->comando_activo = Tipos_Comandos::FINALIZAR_LLAMADA;
				LOG("COLGANDO...\r\n");
			}
			break;

		case 'S':
			indice++;

			if(comando_op[indice]=='I')
			{
				this->comando_activo = Tipos_Comandos::ENVIAR_SMS;
				LOG("ENVIANDO SMS...\r\n");

			}
			break;

		case 'P':
			indice++;

			if(comando_op[indice]=='I')
			{
				this->comando_activo = Tipos_Comandos::REALIZAR_POST;
				LOG("REALIZANDO POST...\r\n");
			}
			break;
		case 'G':
				indice++;

				if(comando_op[indice]=='I')
				{
					this->comando_activo = Tipos_Comandos::ACTIVAR_GPS_CONT;
				}
				else if(comando_op[indice]=='F')
				{
					this->comando_activo = Tipos_Comandos::DESCATIVAR_GPS_CONT;
				}
				break;
		default:
			break;
	}

}
/**************************************************************************************************
 * ESTE METODO CONTIENE LOS ESTADOS DEL DIAGRAMA DE ESTADOS
 ***************************************************************************************************/
void Main::ejecutar_comando_bluetooth()
{
	switch (this->estado_comando)
	{
		case Estado_Comando_Op::ESPERANDO_COMANDOS:
			this->estado_comando=cambiar_estado_esperando_comandos();
			break;

		case Estado_Comando_Op::REALIZANDO_MUESTREO:
			this->realizar_muestreo();
			this->estado_comando=cambiar_estado_realizando_muestreo();
			break;

		case Estado_Comando_Op::FINALIZANDO_MUESTREO:
			this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			break;

		case Estado_Comando_Op::REALIZANDO_CONTROL:

			this->iniciar_control();
			this->estado_comando=cambiar_estado_realizando_control();
			break;

		case Estado_Comando_Op::FINALIZANDO_CONTROL:
			if(this->finalizar_control()==true)
				this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			break;

		case Estado_Comando_Op::ACTIVANDO_LECT_CONT_GPS:
			this->activar_lectura_cont_gps=true;

			this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			LOG("habilitando lectura continua de GPS...\r\n");
			break;

		case Estado_Comando_Op::DESACTIVANDO_LECT_CONT_GPS:
			this->activar_lectura_cont_gps=false;

			this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			LOG("dehabilitando lectura continua de GPS...\r\n");
			break;


		case Estado_Comando_Op::INICIANDO_LLAMADA:
			if(this->peticion_gsm_creada==false)
			{
				memset(&peticion_gsm,0,sizeof(peticion_gsm));
				peticion_gsm.tipo_operacion=Tipos_Comandos::INICIAR_LLAMADA;
				strncpy((char*)peticion_gsm.llamada.num_tel,NUM_TELEFONO_CONTACTO,sizeof(peticion_gsm.llamada.num_tel));
				controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);

				this->peticion_gsm_creada=true;
			}
			if(controlador_gsm.ejecutar_op_encoladas_gsm()==true)
			{
				this->peticion_gsm_creada=false;
				this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			}
			break;

		case Estado_Comando_Op::FINALIZANDO_LLAMDA:
			if(this->peticion_gsm_creada==false)
			{
				peticion_gsm.tipo_operacion=Tipos_Comandos::FINALIZAR_LLAMADA;
				controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);

				this->peticion_gsm_creada=true;

			}
			if(controlador_gsm.ejecutar_op_encoladas_gsm()==true)
			{
				this->peticion_gsm_creada=false;
				this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			}
			break;

		case Estado_Comando_Op::ENVIANDO_SMS:
			if(this->peticion_gsm_creada==false)
			{
				memset(&peticion_gsm,0,sizeof(peticion_gsm));
				peticion_gsm.tipo_operacion=Tipos_Comandos::ENVIAR_SMS;
				strncpy((char*)peticion_gsm.sms.num_tel,NUM_TELEFONO_CONTACTO,sizeof(peticion_gsm.sms.num_tel));
				snprintf((char*)peticion_gsm.sms.msj,
								 sizeof(peticion_gsm.sms.msj),
										"maps.google.com/?q=-34.674984,-58.564941");


				controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);
				this->peticion_gsm_creada=true;
			}
			if(controlador_gsm.ejecutar_op_encoladas_gsm()==true)
			{
				this->peticion_gsm_creada=false;
				this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			}


			break;

		case Estado_Comando_Op::ENVIANDO_POST:
			if(this->peticion_gsm_creada==false)
			{
				memset(&peticion_gsm,0,sizeof(peticion_gsm));
				peticion_gsm.tipo_operacion=Tipos_Comandos::REALIZAR_POST;

				/*snprintf((char*)peticion_gsm.post.msj_json_post,
								 sizeof(peticion_gsm.post.msj_json_post),
										"{\"T\":\"%s\","
										"\"C\":1,"
										"\"H\":\"10:20:50\","
										"\"O\":\"-58.573771\","
										"\"A\":\"-34.678258\"}",
										TOKEN_DISPOSITIVO);*/
				armar_msj_gsm(Detector_Caidas::Resultado::DetectaCaida,Tipos_Comandos::REALIZAR_POST);
				controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);

				this->peticion_gsm_creada=true;
			}
			if(controlador_gsm.ejecutar_op_encoladas_gsm()==true)
			{
				this->peticion_gsm_creada=false;
				this->estado_comando=Estado_Comando_Op::ESPERANDO_COMANDOS;
			}


			break;
		default:
			break;
	}
}
/***************************************************************************************************/
Main::Estado_Comando_Op Main::cambiar_estado_esperando_comandos()
{

	if(this->comando_activo==Tipos_Comandos::INICIAR_MUESTREO)
		return Estado_Comando_Op::REALIZANDO_MUESTREO;

	if(this->comando_activo==Tipos_Comandos::INICIAR_CONTROL)
		return Estado_Comando_Op::REALIZANDO_CONTROL;

	if(this->comando_activo==Tipos_Comandos::ACTIVAR_GPS_CONT)
		return Estado_Comando_Op::ACTIVANDO_LECT_CONT_GPS;

	if(this->comando_activo==Tipos_Comandos::DESCATIVAR_GPS_CONT)
		return Estado_Comando_Op::DESACTIVANDO_LECT_CONT_GPS;

	if(this->comando_activo==Tipos_Comandos::REALIZAR_POST)
		return Estado_Comando_Op::ENVIANDO_POST;

	if(this->comando_activo==Tipos_Comandos::ENVIAR_SMS)
		return Estado_Comando_Op::ENVIANDO_SMS;

	if(this->comando_activo==Tipos_Comandos::INICIAR_LLAMADA)
		return Estado_Comando_Op::INICIANDO_LLAMADA;

	if(this->comando_activo==Tipos_Comandos::FINALIZAR_LLAMADA)
		return Estado_Comando_Op::FINALIZANDO_LLAMDA;

	return Estado_Comando_Op::ESPERANDO_COMANDOS;
}
/***************************************************************************************************/
Main::Estado_Comando_Op Main::cambiar_estado_realizando_muestreo()
{

	if(this->comando_activo==Tipos_Comandos::FINALIZAR_MUESTREO)
		return Estado_Comando_Op::FINALIZANDO_MUESTREO;

	return Estado_Comando_Op::REALIZANDO_MUESTREO;
}
/***************************************************************************************************/
Main::Estado_Comando_Op Main::cambiar_estado_realizando_control()
{

	if(this->comando_activo==Tipos_Comandos::FINALIZAR_CONTROL)
		return Estado_Comando_Op::FINALIZANDO_CONTROL;

	return Estado_Comando_Op::REALIZANDO_CONTROL;
}
/***************************************************************************************************/
bool Main::comprobar_funcionamiento_HW()
{
	bool estado=true;

	//Inicializo el Controlador GSM
	LOG("====================================\r\n");
	LOG("Comprobando conexion de HW...\r\n");
	LOG("====================================\r\n");

	//Se configuran los leds asignados a cada dispositivo
	this->controlador_acel.configurar_led(LED_MPU_GPIO_Port,LED_MPU_Pin);
	this->controlador_gsm.configurar_led(LED_GSM_GPIO_Port,LED_GSM_Pin);
	this->controlador_gps.configurar_led(LED_GPS_GPIO_Port,LED_GPS_Pin);

	//se inician los controladores de cada dispositivo
	estado = this->controlador_acel.controlador_init(Controlador_Acel::Dispositivo::NRO_0,
														Controlador_Acel::Sensibilidad_Acelerometro::RANGO_16G,
														Controlador_Acel::Sensibilidad_Giroscopio::RANGO_2000s);

	this->controlador_gps.controlador_init(&(control_principal.usart_gps));
	this->controlador_gsm.controlador_init(&(control_principal.usart_gsm));

	LOG("====================================\r\n");

	if( estado == true )
	{
		LOG("Sistema configurado correctamente\r\n");
	}
	else
	{
		LOG("ERROR:No se pudo configurar el sistema!!!\r\n");
	}

	HAL_Delay(50);
	LOG("====================================\r\n\n");

	return estado;
}


/***************************************************************************************************/
void Main::realizar_muestreo()
{
	Mpu_Datos_Escalados datos_escalados;
	Mpu_Datos_Escalados datos_kalman;
	Mpu_Datos_Raw datos_raw;

	char buffAX[25], buffAY[25], buffAZ[25],buffAN[25];
	//solicito datos al MPU6050

	if(controlador_acel.leer_acelerometro(&datos_escalados,&datos_kalman,&datos_raw)==true)
	{
		//************************** Muestro datos escalados************************
		float_a_string(buffAX,sizeof(buffAX),datos_escalados.acelerometro.x);
		float_a_string(buffAY,sizeof(buffAY),datos_escalados.acelerometro.y);
		float_a_string(buffAZ,sizeof(buffAZ),datos_escalados.acelerometro.z);
		float_a_string(buffAN,sizeof(buffAN),datos_escalados.acelerometro.norma);

		/*NOTA: se deben mandar a Android como maximo 20 bytes en un send, con la velocidad
		 * 		de 115200 baudios, Sino se pierden caracteres.Ademas se tuve que agregar un
		 * 		delay obligatorio porque sino esta al enviarlo por Blutooth a Android a 115200
		 * 		baudios Android pierde caracteres al recibirlos,por la velocidad de transferencia.
		 */

		LOG("%s;%s;%s;%s;%d;%u\r\n",buffAX, buffAY, buffAZ,buffAN,
										     datos_escalados.angulo,
											 datos_escalados.tiempo_medicion);

		HAL_Delay(21);

		/*******************Muestro datos kalman**********************************/
/*		float_a_string(buffAX,sizeof(buffAX),datos_kalman.acelerometro.x);
		float_a_string(buffAY,sizeof(buffAY),datos_kalman.acelerometro.y);
		float_a_string(buffAZ,sizeof(buffAZ),datos_kalman.acelerometro.z);
		float_a_string(buffAN,sizeof(buffAN),datos_kalman.acelerometro.norma);

		LOG("%s;%s;%s;%s;%d;%u\r\n",buffAX, buffAY, buffAZ,buffAN,
												 datos_kalman.angulo,
												 datos_kalman.tiempo_medicion);*/
		//HAL_Delay(22);

	}


}

/***************************************************************************************************/
void Main::iniciar_control()
{
	Detector_Caidas::Resultado resultado_deteccion=Detector_Caidas::Resultado::SinNovedades;
	Mpu_Datos_Escalados datos_escalados;
	Mpu_Datos_Escalados datos_kalman;
	Mpu_Datos_Raw datos_raw;

	char buffAX[25], buffAY[25], buffAZ[25],buffAN[25];

	//se envian al servidor las notificiones que se encuentran encoladas en el Controlador_GSM
	if(controlador_gsm.ejecutar_op_encoladas_gsm()!=true)
	{
		return;
	}


	//si se encuentra activado la lectura de GPS, entonces se leen las coordenadas de ubicacion
	//del mismo
	if(this->activar_lectura_cont_gps==true)
	{
		this->tiempo_a_espera_leer_gps=HAL_GetTick()-this->tiempo_i_espera_leer_gps;
		if(this->tiempo_a_espera_leer_gps>TIEMPO_ESPERA_GPS)
		{
			if(controlador_gps.leer_gps()==true)
			{
				this->gps_leido=true;
				this->tiempo_i_espera_leer_gps=HAL_GetTick();
			}

			this->gps_leido=true;
			this->tiempo_i_espera_leer_gps=HAL_GetTick();

		}
	}



	//se lee el acelerometro
	if(controlador_acel.leer_acelerometro(&datos_escalados,&datos_kalman,&datos_raw)==true)
	{
		//************************** Muestro datos escalados************************
		float_a_string(buffAX,sizeof(buffAX),datos_escalados.acelerometro.x);
		float_a_string(buffAY,sizeof(buffAY),datos_escalados.acelerometro.y);
		float_a_string(buffAZ,sizeof(buffAZ),datos_escalados.acelerometro.z);
		float_a_string(buffAN,sizeof(buffAN),datos_escalados.acelerometro.norma);

		/*LOG("%s;%d;%u\r\n",buffAN,
						   datos_escalados.angulo,
						   datos_escalados.tiempo_medicion);

		HAL_Delay(3);*/

		//ACA SE DEBERIA LLAMAR AL ALGORITMO DE CAIDAS
		resultado_deteccion=detector_caidas.detectarLaCaida(datos_escalados);

		//Si algoritmo de caidas detecto algun evento(DetectaCaida,DetectaCaidaCritica oDetectaRecupero),
		//entonces se encola en el Controlador_GSM una notificcion para enviarle al
		//servidor el msj con el evento generado juntos a las coordenadas de GPS leidas
		//para que alerte al Smartphone  corresponidente
		if(resultado_deteccion!=Detector_Caidas::Resultado::SinNovedades)
		{
			//si se detecta caida se inhabilita el Post de actualizacion de coordenadas
			//hasta que se detecte recuperacion
			if(resultado_deteccion==Detector_Caidas::Resultado::DetectaCaida)
				this->deteccion_caida_en_curso=true;

			if(resultado_deteccion==Detector_Caidas::Resultado::DetectaRecupero)
				this->deteccion_caida_en_curso=false;

			armar_msj_gsm(resultado_deteccion,Tipos_Comandos::REALIZAR_POST);
			controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);
			LOG("#DETECTO:%d\r\n",resultado_deteccion);
			HAL_Delay(15);
		}
		leyo=true;



	}

	if((this->gps_leido==true)&&(this->deteccion_caida_en_curso==false))
	{
		//Si el algoritmo de caida no detecto eventos, entonces se encola en el Controlador_GSM
		//una notificcion para enviarle al servidor el msj "SinNovedades" juntos a las coordenadas
		//de GPS leidas para para que actualice el mapa de google maps
		armar_msj_gsm(resultado_deteccion,Tipos_Comandos::REALIZAR_POST);
		controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);
		LOG("|||%s||||\r\n",btoa(leyo));
		//LOG("lat:%.4f || long:%.4f\r\n",controlador_gps.get_latitude(),controlador_gps.get_longitude());

		leyo=false;
		this->gps_leido=false;
	}

	//se verifca si se presiono el boton de panico
	verificar_boton_panico();



}

/***************************************************************************************************/
bool Main::finalizar_control()
{
	return controlador_gsm.ejecutar_op_encoladas_gsm();
}
/***************************************************************************************************/
void Main::verificar_boton_panico()
{
	if(cant_pulsaciones_boton==2)
	{
		///se encola una alerta al Controlador_gsm para enviarla por SMS
		armar_msj_gsm(Detector_Caidas::Resultado::AlertaBotonPanico,Tipos_Comandos::REALIZAR_POST);
		controlador_gsm.encolar_notificacion_gsm(&peticion_gsm);

		cant_pulsaciones_boton=0;

		LOG("SE APRETO BOTON [%u]\r\n", cant_pulsaciones_boton);
		HAL_Delay(15);	}
	//Se vuelve habilitar la IRQ del boton de panico

}
/***************************************************************************************************/
void Main::armar_msj_gsm(Detector_Caidas::Resultado resultado_deteccion,Tipos_Comandos tipo_op_alerta)
{
	byte_t coordenadas_gps[25]={'\0'};
	bool resp=false;

	if(tipo_op_alerta!=Tipos_Comandos::REALIZAR_POST)
	{
		LOG("ERROR:Invocacion incorrecta\r\n");
		return;
	}

	memset(&peticion_gsm,0,sizeof(peticion_gsm));

	//Establezco el tipo de operacion GSM a realizar
	peticion_gsm.tipo_operacion=Tipos_Comandos::REALIZAR_POST;

	//Agrego de manera encriptada en el mensaje post: la direccion MAC del dispotivo + el resultado de la deteccion
	switch(resultado_deteccion)
	{
		case(Detector_Caidas::Resultado::SinNovedades):
			strncpy((char*)peticion_gsm.post.msj_json_post,SMD5_SUCESO_ACTUALIZADO,sizeof(peticion_gsm.post.msj_json_post));
			break;
		case(Detector_Caidas::Resultado::DetectaCaida):
			strncpy((char*)peticion_gsm.post.msj_json_post,SMD5_CAIDA_DETECTADA,sizeof(peticion_gsm.post.msj_json_post));
			break;
		case(Detector_Caidas::Resultado::DetectaCaidaCritica):
			strncpy((char*)peticion_gsm.post.msj_json_post,SMD5_SIGUE_CAIDO,sizeof(peticion_gsm.post.msj_json_post));
			break;
		case(Detector_Caidas::Resultado::DetectaRecupero):
			strncpy((char*)peticion_gsm.post.msj_json_post,SMD5_SE_LEVANTO,sizeof(peticion_gsm.post.msj_json_post));
			break;
		case(Detector_Caidas::Resultado::AlertaBotonPanico):
			strncpy((char*)peticion_gsm.post.msj_json_post,SMD5_BOTON_PANICO,sizeof(peticion_gsm.post.msj_json_post));
			break;
		default:
			break;
	}

	//se construye una variable auxiliar con la concatenacion de las coordenadas de GPS
	snprintf((char *)coordenadas_gps,sizeof(coordenadas_gps),"%.4f%.4f",
			controlador_gps.get_latitude(),
			controlador_gps.get_longitude());

	//se termina de armar el mensaje post encriptado con la parte XOR del GPS.
	resp=encrypt_decrypt_xor((char*)coordenadas_gps,
							(char*)&peticion_gsm.post.msj_json_post[tam_suceso_encriptado]-1,
							(char*)clave_encriptacion,
							sizeof(peticion_gsm.post.msj_json_post)-tam_suceso_encriptado);

	if(resp==false)
		LOG("ERROR EN PARAMETROS\r\n");
	else
		LOG("%s",peticion_gsm.post.msj_json_post);
}

/***************************************************************************************************/
int Main::recibir_y_enviar(Serial * serial_origen, Serial *serial_destino)
{
	 byte_t string_aux [255]={0};

	 //si se recibio algun string del de un dispositivo serial como  Bluethoot, GPS, TTL, GSM
	 if(serial_origen->read_line(string_aux,sizeof(string_aux))!=0)
	 {
		 //se envia la cadena recibida  a otro dispositivo serial
		  serial_destino->send("%s",string_aux);
	 }
	 return 0;
}

/*
==========================================================================================
==========================================================================================
===================================== FUNCIONES DEL LENGUAJE C ===========================
==========================================================================================
ESTAS FUNCIONES NO FORMAN PARTE DE LA CLASE PRINCIPAL. HACEN DE NEXO ENTRE LA PARTE EL
MAIN.C,DESARROLLADO EN C, Y EL CONTROL_PRINCIPAL, DESARROLLADO EN C++.
==========================================================================================
==========================================================================================

*/
int CONTROLLER_init( int param )
{
	// Inicializacion de cosas.

	int estado=0;
	control_principal.argc = 0;

	//creo los objetos para el GSM,GPS, Bluethoot y TTL
	control_principal.usart_bluetooth.usart_init(&huart1);
	control_principal.usart_gps.usart_init(&huart2);
	control_principal.usart_gsm.usart_init(&huart3);
	//inicializo el puerto para debug;
	usart_debug=&control_principal.usart_bluetooth;

	return estado;


}


/***************************************************************************************************/

int CONTROLLER_loop()
{
	control_principal.argc = 2;

	return control_principal.loop();

}
/***************************************************************************************************/

int CONTROLLER_usart_handler(UART_HandleTypeDef *huart)
{
	if((int)USART1==(int)huart->Instance)
	{
		control_principal.usart_bluetooth.interrupt_handler();
		return 0;
	}
	if((int)USART2==(int)huart->Instance)
	{
		control_principal.usart_gps.interrupt_handler();
		return 0;
	}
	if((int)USART3==(int)huart->Instance)
	{
		control_principal.usart_gsm.interrupt_handler();
		return 0;
	}
	return 0;
}

/***************************************************************************************************/
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
	control_principal.controlador_acel.recibir_datos_callback();
}

/***************************************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	switch (cant_pulsaciones_boton)
	{
		case 0:
			tiempo_inicio_boton=HAL_GetTick();
			cant_pulsaciones_boton++;
			break;
		case 1:
			if((HAL_GetTick()-tiempo_inicio_boton)>UMBRAL_TIEMPO_IRQ_BOTON)
				cant_pulsaciones_boton++;
			else
				cant_pulsaciones_boton=0;
			break;
		default:
			cant_pulsaciones_boton=0;
	}
}


