/*
 * my_main.hpp
 *
 *  Created on: Oct 27, 2018
 *      Author: wav
 */

#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include "stm32f1xx_hal.h"
//comentario de prueba
/***************************************************************************************************/
/* Lo ve el compilardor g++ de my_main.cpp                                                         */
/***************************************************************************************************/

#ifdef __cplusplus

extern "C"
{
    #include <App/Drivers/Serial.h>
	#include <App/Drivers/Controlador_GSM.h>
	#include <App/Drivers/Controlador_GPS.h>
	#include <App/Drivers/Controlador_Acelerometro.hpp>
	#include <App/Bibliotecas/Detector_Caidas.hpp>
	#include <App/Bibliotecas/Utilitarios.hpp>

	int CONTROLLER_init( int param );
	int CONTROLLER_loop( void );
	int CONTROLLER_usart_handler(UART_HandleTypeDef *huart);

}


class Main
{
	private:

		//Enumeracion que indica el estado del comando de operacion del bluetooth
		enum class Estado_Comando_Op
		{
			ESPERANDO_COMANDOS 			= 0x01,
			REALIZANDO_MUESTREO			= 0x02,
			FINALIZANDO_MUESTREO		= 0x03,
			REALIZANDO_CONTROL			= 0x04,
			FINALIZANDO_CONTROL			= 0x05,
			ACTIVANDO_LECT_CONT_GPS 	= 0x06,
			DESACTIVANDO_LECT_CONT_GPS 	= 0x07,
			INICIANDO_LLAMADA			= 0x08,
			FINALIZANDO_LLAMDA			= 0x09,
			ENVIANDO_SMS			 	= 0x10,
			ENVIANDO_POST				= 0x11
		};

		//variables que controlan el tiempo en el loop
		uint32_t tiempo_a_loop; 			//tiempo actual para leer en el loop
		uint32_t tiempo_i_loop; 			//tiempo inicial para leer en el loop
		uint32_t tiempo_a_espera_leer_gps;  //tiempo actual que se debe esperar para leer el gps
		uint32_t tiempo_i_espera_leer_gps;  //tiempo inicial que se debe esperar para leer el gps

		uint32_t cont_vueltas; //contador de vueltas del loop principal

		bool b_interrupt; 					//variable que controla el while del metodo loop de la clase Main
		bool gps_leido;						//variable que indica si se leyo el gps
		bool peticion_gsm_creada;			//variable que indica si se encolo una peticion una GSM
		bool activar_lectura_cont_gps;		//variable que indica si activo la lectura continua de GPS al recibir el comando #GI

		bool deteccion_caida_en_curso;		//variable que indica que se encuentra en curso la deteccion de caida
		Tipos_Comandos comando_activo;		//variable que indica el evento ocurrido en la maquina de estados del comando de bluetooth,
											//el cual indica que comando se encuentra activo en un momento dado
		Estado_Comando_Op estado_comando; 	//variable que indica en que estado se encuentra la maquina de estados de los comandos de bluetooth.
		Peticion_Operacion_Gsm peticion_gsm;//estructura que contiene todo el formato del mensaje a enviar por GSM

		//variables que sirven para realizar la encriptacion de los mensajes POST
		size_t tam_suceso_encriptado;
		byte_t clave_encriptacion[25];
		/***************************************************************************************************
		* @brief 	Metodo que lee los comandos de Bluethoot segun el protocolo creado y
		* 			determina la accion	a realizar
		* 			ESTE METODO DETERMINA LA OCURRENCIA DE LOS EVENTOS EN EL DIAGRAMA DE ESTADO
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */

		void leer_comando_bluetooth();

		/***************************************************************************************************
		* @brief 	Metodo que ejecuta la accion correspondiente al comando de Bluethoot
		* 			ESTE METODO CONTIENE LOS ESTADOS DEL DIAGRAMA DE ESTADOS
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		void ejecutar_comando_bluetooth();

		/***************************************************************************************************
		* @brief 	Metodo que realiza el muestreo de datos del acelerometro y los envia por bluethoot al Smartphone
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		void realizar_muestreo();

		/***************************************************************************************************
		* @brief 	Metodo que realiza el control de caidas en tiempo real
		* 			determina la accion	a realizar
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		void iniciar_control();
		bool finalizar_control();


		/***************************************************************************************************
		* @brief 	Metodo que recibe datos de un puerto serial origen y los reenvia a traves de otro puerto destino
		* @param	serial_origin: puntero del objeto el puerto serial origen
		*		 	serial_destiny: puntero del objeto el puerto serial destino
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		int recibir_y_enviar(Serial * serial_origen, Serial *serial_destino);

		/***************************************************************************************************
		* @brief 	Metodo que comprueba el funcionamiento y conexion de los perifericos conectados al STM32
		* @param	---
		* @return   true : si funciona todo bien
		* 			false: si hubo un error
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		bool comprobar_funcionamiento_HW();

		/***************************************************************************************************
		* @brief 	Metodo que arma el mensaje de alerta que se va a enviar por GSM
		* @param	resultado_deteccion: resultado devuelto por el algoritmo de caidas
		* 			tipo_alerta:variable que indica que tipo de mensaje si enviara por GSM
		* 			cuando se debe notificar una alerta
		* @return   --
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		void armar_msj_gsm(Detector_Caidas::Resultado resultado_deteccio,Tipos_Comandos tipo_op_alertan);

		/***************************************************************************************************
		* @brief 	Metodos que cambian de estado en la maquina de estados que se ejecuta en ejecutar_comando_bluetooth()
		* @param	---
		* @return   nuevo estado a ejecutar en la maquina de estado
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */
		Estado_Comando_Op cambiar_estado_esperando_comandos();
		Estado_Comando_Op cambiar_estado_realizando_muestreo();
		Estado_Comando_Op cambiar_estado_realizando_control();

		/***************************************************************************************************
		* @brief 	Metodos que verifica si se activo el boton de panico y en caso de que eso suceda emite una
		* 			alerta via gsm
		* @param	---
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		* */

		void verificar_boton_panico();
	public:
		//==============Atributos publicos de la clase Main====================
		//Creo los objetos de las USART para manejar el GPS,BLUETHOOT,GSM y TTL
		Serial usart_bluetooth;
		Serial usart_gps;
		Serial usart_gsm;

		//objetos de las clases controladores y detector de caidas
		Controlador_GSM  controlador_gsm;
		Controlador_GPS  controlador_gps;
		Controlador_Acel controlador_acel;
		Detector_Caidas  detector_caidas;

		int argc;
		char *argv[];

		//==============Metodos publicos de la clase Main====================

		Main();
		virtual ~Main();
		int loop();



};



/***************************************************************************************************/
/* Lo ve el compilador gcc de main.c                                                               */
/***************************************************************************************************/

#else

	//funcion en C que inicializa el objeto de la clase Main
	int CONTROLLER_init( int param );

	//funcion en C que invoca el metodo loop de la clase Main
	int CONTROLLER_loop( void );


#endif /* __cplusplus */
#endif /* _CONTROLLER_H_ */
