/* @file Controlador.h
 * @brief Implementacion del Controlador GSM
 * @date: Oct 29, 2018
 * @author: Esteban Carnuccio.
 */

#ifndef __CONTROLADOR_GSM_H_
#define __CONTROLADOR_GSM_H_

#include <App/Drivers/Serial.h>
#include "App/Bibliotecas/Buffer_Circular/Buffer_Circular_GSM.h"
#include "main.h"


class Controlador_GSM
{

public:

	//Enumeracion que indica el estado activo de la maquina de estados del Controlador_GPS
	 enum class Estado_Op_Gsm
	 {
		  INACTIVO					 = 1,
		  COMPROBAR_SENAL			 = 2,
		  OBTENER_PETICION_PENDIENTE = 3,
		  NOTIFICAR_SIN_SENAL_GSM	 = 4,
		  REALIZAR_OPERACION		 = 5,
		  ENCOLAR_PETICION			 = 6,
		  REINICIAR_OPERACION		 = 7
	};

	 //Enumeracion que indica si el GSM tiene senal o no, o si esta realizando una operacion
	 enum class Estado_Senal_Gsm
	 {
		 GSM_SIN_SENAL				 = 1,
		 GSM_CON_SENAL				 = 2,
		 ESPERANDO_RESPUESTA		 = 3

	 };
   /***************************************************************************************************
	* @brief	Constructor de la clase por defecto.
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	Controlador_GSM();

   /***************************************************************************************************
	* @brief 	Destructor de la clase por defecto.
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	~Controlador_GSM();

	/***************************************************************************************************
	* @brief 	Metodo que configura el puerto usart usado para el GSM y
	* 			chequea si esta conectado correctamente
	* @param    SerialGSM: puntero al objeto Serial asociado al GSM
	* @return 	true: si el GSM funciona correctamente
	* 			false: si no se pudo detectar el GSM
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool controlador_init(Serial * SerialGSM);

	/***************************************************************************************************
	* @brief 	Metodo que agrega a una cola las peticiones de operaciones GSM que debe ejecutar
	* 			el GSM. Esto se realiza para no perder peticiones por si al mismo tiempo se
	* 			deben realizar varias operaciones GSM. Por lo que se encolan y luego se ejecutaran una tras una,
	* 			a medida que se vayan completando.
	* @param    op: peticion de operacion GSM a encolar
	* @return 	true:
	* 			false:
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool encolar_notificacion_gsm(Peticion_Operacion_Gsm *op);

	/***************************************************************************************************
	* @brief 	Metodo que ejecuta la maquina de estados del Controlador_GSM, ejecutando para ello las
	* 			peticiones de operaciones GSM que se encuentren encoladas
	* @param    SerialGSM: puntero al objeto Serial asociado al GSM
	* @return 	true:
	* 			false:
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool ejecutar_op_encoladas_gsm();

	/***************************************************************************************************
	* @brief	Metodo que alamcena el pin y el puerto del led del GSM
	* @param	led_puerto: indica el puerto asignado al led del gps
	* 			led_pin: indica el pin asignado al led del gps
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	void configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin);

private:

	Serial * serial_GSM; //puntero al objeto asignado al puerto del GSM

	//variables de tiempo de espera no bloqueante entre la recepcion de la respuesta de un comando AT y la
	//proxima ejecucion de un comando AT
	uint32_t tiempo_espera_ejecutar_at;
	uint32_t tiempo_inicial_ejecutar_at;

	//variables de tiempo de espera no bloqueante entre la ejecucion de un comando AT y la
	//recepcion de la respuesta de ese comando
	uint32_t tiempo_espera_respuesta_at;
	uint32_t tiempo_inicial_respuesta_at;
	uint32_t tiempo_total_ejecucion;
	uint32_t tiempo_total_inicial_ejecucion;
	uint32_t tiempo_i_prender_led_gsm;
	uint32_t tiempo_a_prender_led_gsm;

	uint32_t tiempo_en_estado;

	Peticion_Operacion_Gsm peticion_activa;
	Peticion_Operacion_Gsm peticion_encolada;
	Estado_Op_Gsm estado_op;
	Estado_Senal_Gsm estado_senal;

	//contador de comandos AT involucrados en una operacion del usuario
	uint8_t num_comando_at;
	uint8_t estado_comando_at;


	//cadena donde se almacena la respuesta emitida por el GSM, al haber ejecutado un comando AT
	byte_t respuesta[100];

	//se genera un Buffer circular para administrar las peticiones de operaciones de
	//GSM, que se solicitan que realice el driver GSM
	Buffer_Circular_GSM cola_circular_peticiones;

	//variables que indican el puerto y el pin del led para el GPS
	GPIO_TypeDef * led_gsm_puerto;
	uint32_t led_gsm_pin;

	/***************************************************************************************************
	* @brief 	Metodo que chequea si el GSM esta conectado y funcionando corretamenta
	* @return 	Estado_Senal_Gsm: retorna una enumeracion con el estado de la senal del GSM
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	Estado_Senal_Gsm comprobar_funcinamiento_SIM800L();

	/***************************************************************************************************
	* @brief 	Metodo que quita de la cola de peticiones una peticion de operacion
	* @param	peticion: puntero a una estructura donde se almacenara la petcion desencolada
	* @return
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/

	bool desencolar_notificacion_gsm(Peticion_Operacion_Gsm * peticion);

	/***************************************************************************************************
	* @brief 	Metodo que determina el tipo de operacion que solicita ejecutar una petcion.
	* 			Esta operacion puede ser: Inicia_llamada_telefonica,finalizar_llamada,
	* 			Enviar_sms y Realizar_post.
	* @return
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/

	bool determinar_tipo_op_gsm();

	/***************************************************************************************************
	* @brief 	Metodo que ejecuta comandos AT, recibe sus respuesta y realiza un preanalizado de la misma
	* @param 	comando:comando AT a ejecutar
	* 			caracter_corte_respuesta: caracter que indica el corte de la recepcion de datos seriales
	* @return	true: si la respuesta es "OK"
	* 			false: si la respuesta no es "OK"
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool realizar_operacion_ok(const char * comando,byte_t caracter_corte_respuesta);

	/***************************************************************************************************
	* @brief 	Metodo que ejecuta comandos AT, recibe sus respuesta y la retorna en la variable
	* 			respuesta de esta clase
	*  @param 	comando:comando AT a ejecutar
	* 			caracter_corte_respuesta: caracter que indica el corte de la recepcion de datos seriales
	* @return	retorna la cantidad de caracteres que contiene la respuesta de haber ejecutado el comando AT
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	size_t ejecutar_comando_at(const char * comando,byte_t caracter_corte_respuesta);


	/***************************************************************************************************
	* @brief 	Metodo que realizar inicia una llamada telefonica a numero determinado de telefono
	* @param 	num_tel: numero de telefono que se desea llamar
	* @return 	true: cuando se inicia la llamada, al ejecutar correctamente todos los comandos AT
	* 			involucrados
	* 			false: mientras se esten ejecutandos los comandos AT para iniciar la llamada
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool realizar_llamada(byte_t* num_tel);

	/***************************************************************************************************
	* @brief 	Metodo que finaliza una llamada telefonica inciada previamente a un numero de telefono
	* @return 	true: cuando se finaliza la llamada, al ejecutar correctamente todos los comandos AT
	* 			involucrados
	* 			false: mientras se esten ejecutandos los comandos AT para finalizar la llamada
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool finalizar_llamada();

	/***************************************************************************************************
	* @brief 	Metodo envia un SMS a un numero determinado de telefono
	* @param 	num_tel: numero de telefono al que se desea mandar el SMS
	* 			mensaje: mensaje de texto que se enviara en el SMS
	* @return 	true: cuando se envio el SMS, al ejecutar correctamente todos los comandos AT
	* 			involucrados
	* 			false: mientras se esten ejecutandos los comandos AT para enviar el SMS
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool enviar_sms(byte_t * num_tel,byte_t *mensaje);

	/***************************************************************************************************
	* @brief 	Metodo envia un mensaje POST al servidor
	* @param 	msj_json_enviar: datos en formato JSON a enviar en el mensaje POST
	* 			tam_msj_enviar: cantidad de caracteres que contiene JSON a enviar
	* 			msj_json_resp: puntero a un buffer en donde se retonra la respuesta,en formato JSON, que
	* 			devolvera el servidor al recibir el mensaje POST
	* 			tam_msj_resp: tamano que tiene el buffer de respuesta
	* @return 	true: cuando se envio el el mensaje POST, al ejecutar correctamente todos los comandos AT
	* 			involucrados
	* 			false: mientras se esten ejecutandos los comandos AT para enviar el mensaje POST
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	bool realizar_POST(byte_t * msj_json_enviar,size_t tam_msj_enviar,byte_t * msj_json_resp,size_t tam_msj_resp);

	/***************************************************************************************************
	 * @brief  Metodo que controla cambia de estados dentro de la maquina de estados del GSM, comprobando
	 * 		   constantemete si no se encuentra ejecutando mucho tiempo un  estado determinado. En caso de que
	 * 		   esto suceda se resetea la maquina de estados
	 *
	 * @param  proximo_estado_ok: si no se cumple el timeout encotnces se debera cambiar la maquina de estado a estado
	 * @param  proximo_estado_error: si se cumple el timeout encotnces se debera cambiar la maquina de estado a estado
	 * @param  timeout: tiempo maximo permitido que se podra estar en un estado.
	 *
	 */
	void proximo_estado(Estado_Op_Gsm proximo_estado_ok, Estado_Op_Gsm proximo_estado_error=Estado_Op_Gsm::INACTIVO, uint32_t timeout=0);

};


// -------------------------------------------------------------------

#endif /* __CONTROLADOR_GSM_H_ */
