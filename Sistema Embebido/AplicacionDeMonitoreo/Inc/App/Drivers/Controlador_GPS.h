/*
    File:       nmea.h
    Version:    0.1.0
    Date:       Feb. 23, 2013
	License:	GPL v2

	NMEA GPS content parser

    ****************************************************************************
    Copyright (C) 2013 Radu Motisan  <radu.motisan@gmail.com>

	http://www.pocketmagic.net

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
    ****************************************************************************
 */

#include <App/Drivers/Serial.h>
#include <App/Bibliotecas/Utilitarios.hpp>
#include <App/Bibliotecas/Kalman.hpp>
#include <math.h>

class Controlador_GPS
{


	public:

		//puntero al objeto asignado al puerto del GPS
		Serial * serial_GPS;

		//Enumeracion que contiene el estado de la se�al del GPS, una vez que se
		//analiza la cadena NMEA recibida
		enum class  Estado_Gps
		{
			SIN_SENAL      	   = 0x01, //no hay se�al en el GPS
			DATO_NO_DISPONIBLE = 0x02, //hay se�al, pero todavia no se han podido leer las coordenadas
			DATO_DISPONIBLE    = 0x03//se obtuvieron las coordenadas del GPS
		};

		//Estados de la maquina de estados del Controlador_gps
		enum class  Operacion_Gps
		{
			LEER_GPS 					= 0x00,  //se lee las coordenadas del GPS
			DETECTAR_COORDENADAS  	    = 0x01,
			NOTIFICAR_SIN_SENAL 		= 0x02  //se notifica via LED que el GPS no tiene se�al
		};

		//Enumeracion que indica los posibles resultados que se pueden obtener mientras se analiza
		//la linea recibida por serial para averiguar si es o no una cadena NMEA
		enum class  Resultado_Analisis_Nmea
		{
			ANALIZANDO_CADENA			= 0x00,  //se lee las coordenadas del GPS
			NO_ES_NMEA			  	    = 0x01,
			ES_NMEA						= 0x02  //se notifica via LED que el GPS no tiene se�al
		};


	   /***************************************************************************************************
		* @brief	Constructor de la clase por defecto.
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		Controlador_GPS();


	   /***************************************************************************************************
		* @brief 	Destructor de la clase por defecto.
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		~Controlador_GPS();

		/***************************************************************************************************
		* @brief 	Metodo que configura el puerto usart usado para el GPS y chequea si esta conectado
		* 			correctamente
		* @param    SerialGPS: puntero al objeto Serial asociado al GPS
		* @return 	true: si el GSM funciona correctamente
		* 			false: si no se pudo detectar el GSM
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		bool			controlador_init(Serial * SerialGPS);

		/***************************************************************************************************
		* @brief 	Metodo que ejecuta la maquina de estados del Controlador_GPS
		* @param    SerialGPS: puntero al objeto Serial asociado al GPS
		* @return 	true: si obtuvo coordenadas validas del GPS, a traves de la cadena NMEA
		* 			false: si no pudo coordenadas validas o todavia sigue analizandolos datos
		* 				   recibidos del GPS.
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		bool 			leer_gps();

		/***************************************************************************************************
		* @brief	Metodo que alamcena el pin y el puerto del led del GPS
		* @param	led_puerto: indica el puerto asignado al led del gps
		* 			led_pin: indica el pin asignado al led del gps
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		void configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin);

		/***************************************************************************************************
		* @brief 	Metodos que se pueden utilizar para obtener los valores obtenidos del GPS
		* 			a partir del analisis de la cadena NMEA.
		* @author 	Esteban Carnuccio
		*/
		int				get_hour();
		int				get_minute();
		int				get_second();
		int				get_day();
		int				get_month();
		int				get_year();
		float			get_latitude();
		float			get_longitude();
		int				get_satellites();
		float			get_altitude();
		float			get_speed();
		float			get_bearing();
		bool 			get_grmc_leido();

private:

		/***************************************************************************************************
		* @brief 	Metodo que va analizando si la linea recibida del GPS, a traves del puerto serial,
		* 			es una cadena NMEA.
		* @return 	Resultado_Analisis_Nmea: resultado del analisis de la liena recibida
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		Resultado_Analisis_Nmea detectar_nmea();

		/***************************************************************************************************
		* @brief 	Metodo que va agreagando a un matriz los distintos campos que conforman la cadena NMEA
		* 			a medida que los va parseando
		* @return 	true: si realizo la operacion correctamente
		* 			false: si hubo un error al realizar la accion
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		bool agregar_a_datos_parseados(char c);

		/***************************************************************************************************
		* @brief 	Metodo que va agreagando en un vector el checksum recibido en la cadena NMEA recibida
		* @return 	true: si realizo la operacion correctamente
		* 			false: si hubo un error al realizar la accion
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		bool guardar_checksum(char c);

		/***************************************************************************************************
		* @brief 	Metodo que reinicia las variables utilizadas en el parseo de los datos del GPS
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		void reiniciar_variables();


		/***************************************************************************************************
		* @brief 	Metodo que extrae todos los datos almacenados en los campos de la cadena NMEA y los almacena
		* 			en variables de la Clase Controlador_GPS, para que posteriormente puedan ser
		* 			obtenidas atraves de sus metodos get
		* @return 	Estado_Gps: Retorna una enumeracion con el estado de la se�al del GPS
		* @date 	01/02/2019
		* @author 	Esteban Carnuccio
		*/
		Estado_Gps	parsear_datos();



		char			tmp_words[TAM_FILAS_TMP_WORDS][TAM_COLUMNAS_TMP_WORDS],	//	hold parsed words for one given NMEA sentence
						tmp_szChecksum[TAM_TMP_CHK];	//	hold the received checksum for one given NMEA sentence

		// will be set to true for characters between $ and * only
		bool			m_bFlagComputedCks ;			// used to compute checksum and indicate valid checksum interval (between $ and * in a given sentence)
		int				m_nChecksum ;					// numeric checksum, computed for a given sentence
		bool			m_bFlagReceivedCks ;			// after getting  * we start cuttings the received checksum
		int				index_received_checksum ;		// used to parse received checksum

		// word cutting variables
		int				m_nWordIdx ,					// the current word in a sentence
						m_nPrevIdx,						// last character index where we did a cut
						m_nNowIdx ;						// current character index

		// globals to store parser results
		float			res_fLongitude;					// GPRMC and GPGGA
		float			res_fLatitude;					// GPRMC and GPGGA
		unsigned char	res_nUTCHour, res_nUTCMin, res_nUTCSec,		// GPRMC and GPGGA
						res_nUTCDay, res_nUTCMonth, res_nUTCYear;	// GPRMC
		int				res_nSatellitesUsed;			// GPGGA
		float			res_fAltitude;					// GPGGA
		float			res_fSpeed;						// GPRMC
		float			res_fBearing;					// GPRMC


		bool			retorno_carro_detectado;
		bool 			irq_gps_habilitado;
		bool 			GRMC_leido;

		Operacion_Gps 	estado_op_gps;

		byte_t cadena_gps [255];
		size_t tam_cadena_gps;
		size_t pos_analizada;

		Kalman			kalman_longitud;
		Kalman			kalman_latitud;

		//variables de tiempo utilizadas para prender y apagar el LED del GPS
		uint32_t        tiempo_i_duracion_leer_gps;
		uint32_t 		tiempo_f_duracion_leer_gps;
		uint32_t		tiempo_i_prender_led_gps;
		uint32_t		tiempo_a_prender_led_gps;

		//variables que indican el puerto y el pin del led para el GPS
		GPIO_TypeDef * led_gps_puerto;
		uint32_t led_gps_pin;
	};
