/*
 * ControladorAcelerometro.hpp
 *
 *  Created on: Nov 3, 2018
 *      Author: Waldo A. Valiente
 */

#ifndef APP_Controlador_Acelerometro_HPP_
#define APP_Controlador_Acelerometro_HPP_

#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_i2c.h"
#include "i2c.h"
#include "math.h"
#include <App/Bibliotecas/Estructuras_Acelerometro.hpp>
#include <App/Bibliotecas/Kalman.hpp>
#include <App/Drivers/Serial.h>



// -------------------------------------------------------------------
// Definicion de Constantes.
extern I2C_HandleTypeDef hi2c1;

// -------------------------------------------------------------------
// Definicion de Clase.

class Controlador_Acel
{

public:

	//Variable que almacena la configuracion con los offset de la calibracion
	MpuDatosCalibracion config;

	/**
	 * @brief  MPU6050 can have 2 different slave addresses, depends on it's input AD0 pin
	 *         This feature allows you to use 2 different sensors with this library at the same time
	 */
	enum class Dispositivo
	{
		NRO_0 = 0x00, /*!< AD0 pin is set to low */
		NRO_1 = 0x02  /*!< AD0 pin is set to high */
	};

	/**
	 * @brief  MPU6050 result enumeration
	 */
	enum class Resultado
	{
		OK = 0x00,			         /*!< Everything OK */
		ERROR,              		 /*!< Unknown error */
		DISPOSITIVO_DESCONECTADO, 	 /*!< There is no device with valid slave address */
		DISPOSITIVO_INVALIDO,        /*!< Connected device with address is not MPU6050 */
		DATO_NO_LEIDO,
		TIME_OUT
	};
	/**
	 * @brief  Parameters for accelerometer range
	 */
	enum class Sensibilidad_Acelerometro
	{
		RANGO_2G = 0x00, /*!< Range is +- 2G */
		RANGO_4G = 0x01, /*!< Range is +- 4G */
		RANGO_8G = 0x02, /*!< Range is +- 8G */
		RANGO_16G = 0x03 /*!< Range is +- 16G */
	};

	/**
	 * @brief  Parameters for gyroscope range
	 */
	enum class Sensibilidad_Giroscopio
	{
		RANGO_250s  = 0x00,  /*!< Range is +- 250 degrees/s */
		RANGO_500s  = 0x01,  /*!< Range is +- 500 degrees/s */
		RANGO_1000s = 0x02, /*!< Range is +- 1000 degrees/s */
		RANGO_2000s = 0x03  /*!< Range is +- 2000 degrees/s */
	};
	enum class Estado_Lectura
	{
		TRANSMIT_ENVIADO = 0x00,
		DATOS_SOLICITADOS,
		DATOS_RECIBIDOS,
		DATOS_LEIDOS

	};

	//Umbrales que sirven para aplicar una ventana de filtro a los valores leidos.
	enum class Umbrales_Ventana
	{
		UMBRAL_ACEL_MAX 	=   25, //Para la escala de 16G
		UMBRAL_ACEL_MIN 	=  -25, //Para la escala de 16G
		UMBRAL_GIRO_MAX     =  100, //Para la escala de 2000 deg/s
		UMBRAL_GIRO_MIN     = -100  //Para la escala de 2000 deg/s
	};


	//Constantes que indican los estados de la maquina de estados del Controlador_acelerometro
	enum class  Operacion_Acelerometro
	{
		ESPERAR_LECTURA_ACEL  = 0x00,
		REALIZAR_PETICION     = 0x01, //se realiza una peticion de datos del acelerometro por IRQ
		LEER_DATOS_RECIBIDOS  = 0x02, //se lee los datos del acel que fueron solicitados por IRQ
		NOTIFICAR_SIN_SENAL   = 0x03  //se notifica via LED que el Acel no tiene se�al
	};


   /***************************************************************************************************
	* @brief	Constructor de la clase por defecto.
	* @param 	huart, puntero a la estructura del stm32, con los datos de la terminal serial.
	* @date   	03/11/2018
	* @author
	*/
	Controlador_Acel();

   /***************************************************************************************************
	* @brief 	Destructor de la clase por defecto.
	* @date		03/11/2019
	* @author
	*/
	~Controlador_Acel();


	/***************************************************************************************************
	 * @brief  Initializes MPU6050 and I2C peripheral
	 * @param  *DataStruct: Pointer to empty @ref MPU6050_t structure
	 * @param  DeviceNumber: MPU6050 has one pin, AD0 which can be used to set address of device.
	 *          This feature allows you to use 2 different sensors on the same board with same library.
	 *          If you set AD0 pin to low, then this parameter should be Dispositivo_0,
	 *          but if AD0 pin is high, then you should use Dispositivo_1
	 *
	 *          Parameter can be a value of @ref Dispositivo_t enumeration
	 * @param  AccelerometerSensitivity: Set accelerometer sensitivity. This parameter can be a value of @ref SensibilidadAcelerometro_t enumeration
	 * @param  GyroscopeSensitivity: Set gyroscope sensitivity. This parameter can be a value of @ref SensibilidadGiroscopio_t enumeration
	 * @retval Initialization status:
	 *            - Resulado_t: Everything OK
	 *            - Other member: in other cases
	 */
	bool controlador_init(Dispositivo,Sensibilidad_Acelerometro,Sensibilidad_Giroscopio);


	/***************************************************************************************************
	* @brief 	Metodo que se utiliza desde el main para invocar a la maquina de estados responsable
	* 			de leer el acelerometro
	* @param	datos_escalados: objeto que se utiliza para alamcenar los datos escalados una vez obtenidos
	* 			datos_kalman:    objeto que se utiliza para alamcenar los datos kalman    una vez obtenidos
	* 			datos_raw_aux:   objeto que se utiliza para alamcenar los datos raw       una vez obtenidos
	* @return   true: si obtuvo los valores del acelerometro
	* 			false: si todavia no leyo nada del aclerometro
	* @date		03/11/2019
	* @author
	*/
	bool leer_acelerometro(Mpu_Datos_Escalados *datos_escalados,Mpu_Datos_Escalados *datos_kalman,Mpu_Datos_Raw *datos_raw_aux);


	/***************************************************************************************************
	* @brief 	Metodo que handler que se invoca cuando se recibe una IRQ del acelerometro indicando que
	* 			este leyo las aceleraciones
	* @date		03/11/2019
	* @author
	*/
	void recibir_datos_callback();


	/***************************************************************************************************
	* @brief	Metodo que alamcena el pin y el puerto del led del Acelerometro
	* @param	led_puerto: indica el puerto asignado al led del MPU
	* 			led_pin: indica el pin asignado al led del MPU
	* @date 	01/02/2019
	* @author 	Esteban Carnuccio
	*/
	void configurar_led(GPIO_TypeDef * led_puerto,uint32_t led_pin);

private:


	/***************************************************************************************************
	 * @brief  Incializa el MPU6050 y en caso de no poder hacerlo parpadea el led asignado
	 * 		   al mismo
	 * @param  *DataStruct: Pointer to empty @ref MPU6050_t structure
	 * @param  DeviceNumber: MPU6050 has one pin, AD0 which can be used to set address of device.
	 *          This feature allows you to use 2 different sensors on the same board with same library.
	 *          If you set AD0 pin to low, then this parameter should be Dispositivo_0,
	 *          but if AD0 pin is high, then you should use Dispositivo_1
	 *
	 *          Parameter can be a value of @ref Dispositivo_t enumeration
	 * @param  AccelerometerSensitivity: Set accelerometer sensitivity. This parameter can be a value of @ref SensibilidadAcelerometro_t enumeration
	 * @param  GyroscopeSensitivity: Set gyroscope sensitivity. This parameter can be a value of @ref SensibilidadGiroscopio_t enumeration
	 * @retval true:si se inicializo correctamente
	 * 		   false: si no pudo inicializarse
	 *
	 */
	Resultado iniciar(Dispositivo,Sensibilidad_Acelerometro,Sensibilidad_Giroscopio);

	/***************************************************************************************************
	 * @brief  Metodo que controla cambia de estados dentro de la maquina de estados del acelerometro, comprobando
	 * 		   constantemete si no se encuentra ejecutando mucho tiempo un  estado determinado. En caso de que
	 * 		   esto suceda se resetea la maquina de estados
	 *
	 * @param  proximo_estado_ok: si no se cumple el timeout encotnces se debera cambiar la maquina de estado a estado
	 * @param  proximo_estado_error: si se cumple el timeout encotnces se debera cambiar la maquina de estado a estado
	 * @param  timeout: tiempo maximo permitido que se podra estar en un estado.
	 *
	 */
	void proximo_estado(Operacion_Acelerometro proximo_estado_ok, Operacion_Acelerometro proximo_estado_error = Operacion_Acelerometro::ESPERAR_LECTURA_ACEL , uint32_t timeout = 0);

	/**
	 * @brief  Sets gyroscope sensitivity
	 * @param  *DataStruct: Pointer to @ref MPU6050_t structure indicating MPU6050 device
	 * @param  GyroscopeSensitivity: Gyro sensitivity value. This parameter can be a value of @ref SensibilidadGiroscopio_t enumeration
	 * @retval Member of @ref Resulado_t enumeration
	 */
	Resultado configurar_giroscopio( Sensibilidad_Giroscopio sensibilidad_giroscopio );

	/**
	 * @brief  Sets accelerometer sensitivity
	 * @param  *DataStruct: Pointer to @ref MPU6050_t structure indicating MPU6050 device
	 * @param  AccelerometerSensitivity: Gyro sensitivity value. This parameter can be a value of @ref SensibilidadAcelerometro_t enumeration
	 * @retval Member of @ref Resulado_t enumeration
	 */
	Resultado configurar_acelerometro( Sensibilidad_Acelerometro sensibilidad_acelerometro );

	/**
	 * @brief  Sets output data rate
	 * @param  rate: Data rate value. An 8-bit value for prescaler value
	 * @retval Member of @ref Resulado_t enumeration
	 */
	Resultado configurar_rango_datos( uint8_t rate );

	/***************************************************************************************************
	* @brief 	Metodo que se utiliza para leer los valores del aclerometro capturados por el handler del
	* 			la IRQ
	* @param	datos_escalados: objeto que se utiliza para alamcenar los datos escalados una vez obtenidos
	* 			datos_kalman:    objeto que se utiliza para alamcenar los datos kalman    una vez obtenidos
	* 			datos_raw_aux:   objeto que se utiliza para alamcenar los datos raw       una vez obtenidos
	* @return   true: si obtuvo los valores del acelerometro
	* 			false: si todavia no leyo nada del aclerometro
	* @date		03/11/2019
	* @author
	*/

	bool leer_datos_recibidos(Mpu_Datos_Escalados *datos_escalados,Mpu_Datos_Escalados *datosKalman,Mpu_Datos_Raw *datos_raw_aux);

	/***************************************************************************************************
	* @brief 	Metodo que le solicita al acelerometro que lea las aceleraciones y las retorne por IRQ cuando
	* 			ya las haya leido
	* @date		03/11/2019
	* @author
	*/
	bool solicitar_datos_mpu();

	/***************************************************************************************************
	* @brief 	Metodo que escala un eje del acelerometro de acuerdo a los parametros de configuracion
	* 			prestablecidos
	* @param	valor:  valor a escalar
	* 			offset: offset del eje
	* 			min_escala: escala minima del eje
	* 			max_escala: escala maxima del eje
	* @return   valor escalado
	* @date		03/11/2019
	* @author
	*/
	float escalar_acelerometro( int16_t valor, int16_t offset,int16_t min_escala,int16_t max_escala);

	/***************************************************************************************************
	* @brief 	Metodos encaergados de aplicar Kalman para un valor determinado
	* @param	valor:  valor a filtar
	* 			destino: puntero a la variable en donde se va a almacenar el valor filtrado
	* @date		03/11/2019
	* @author
	*/
	void aplicar_filtros_acel_x(float valor,float *destino);
	void aplicar_filtros_acel_y(float valor,float *destino);
	void aplicar_filtros_acel_z(float valor,float *destino);
	void aplicar_filtros_giro_x(float valor,float *destino);
	void aplicar_filtros_giro_y(float valor,float *destino);
	void aplicar_filtros_giro_z(float valor,float *destino);

	/***************************************************************************************************
	* @brief 	Metodos encargados de calcular los angulos
	* @date		03/11/2019
	* @author
	*/
	void calcular_norma(Mpu_Datos_Escalados * datos);
	void calcular_angulo(Mpu_Datos_Escalados *datos);
	int calcular_angulo_coseno(Valor_Eje op1, Valor_Eje op2);
	void establecer_primera_lectura_acel();


	I2C_HandleTypeDef* ptr_I2C_x; 	/*!< Puntero a estructura I2C, iniciado en main.c */
	uint8_t i2c_direccion_disp;  	/*!< Direccion del dispositovio I2C */
	uint8_t datos_array[14];

	float Gyro_Mult;         		/*!< Corrector del giroscopio para datos crudos a "grados//segundos" */
	float Acce_Mult;         		/*!< Corrector del acelerometro para datos crudos a "g" */

	//objetos utilizidos para utilizar Kalman
	Kalman Kalman_Accelerometer_X;
	Kalman Kalman_Accelerometer_Y;
	Kalman Kalman_Accelerometer_Z;
	Kalman Kalman_Gyroscope_X;
	Kalman Kalman_Gyroscope_Y;
	Kalman Kalman_Gyroscope_Z;

	//variable temporal donde se alamcenan los datos raw recibidos por IRQ
	Mpu_Datos_Raw datos_raw;


	Operacion_Acelerometro op_acel; //indica en que estado se encuentra la maquina de estados del Controlador_acel
	Estado_Lectura estado_lectura;	//indica el estado de la lectura a traves de la IRQ, o sea:
									//-si se genero la peticion de los datos
									//-si se recibio la IRQ y se alamacenaron los datos
									//-si se leyeron los datos almacenados

	bool solicitud_transmit_realizada;
	//variables de tiempo
	uint32_t tiempo_i_prender_led_acel;
	uint32_t tiempo_a_prender_led_acel;
	uint32_t tiempo_inicial_peticion;
	uint32_t tiempo_actual_peticion;
	uint32_t tiempo_incial_entre_medicion;
	uint32_t tiempo_en_estado;

	//variables utilizadas para calcular el angulo
	Valor_Eje acel_primera_vez;
	bool bandera_primera_vez;

	//variables que indican el puerto y el pin del led para el Acelerometro
	GPIO_TypeDef * led_acel_puerto;
	uint32_t led_acel_pin;

 };

#endif /* APP_Mpu6050_HPP_ */
