/*
 * @file Serial.hpp
 * @brief Implementacion del driver puerto Serial.
 * @date: Oct 29, 2018
 * @author: Waldo A. Valiente, basado en codigo C de Esteban Carnuccio.
 */

#ifndef DRIVER_SERIAL_HPP_
#define DRIVER_SERIAL_HPP_

#include "stm32f1xx_hal.h"
#include "App/Bibliotecas/Buffer_Circular/Buffer_Circular_Serial.h"
#include <string.h>
#include <stdarg.h>

inline void noop( ) {};

class Serial
{

public:

   /***************************************************************************************************
	* @brief	Constructor de la clase por defecto.
	* @date   	29/10/2018
	* @author
	*/
	Serial();

   /***************************************************************************************************
	* @brief 	Destructor de la clase por defecto.
	* @date		29/10/2019
	* @author
	*/
	~Serial();

       /***************************************************************************************************
     * @brief Inicia la recepcion de datos a traves de la UART, realizando su configuraci�n
     * @param huart: puntero a la estructura con los datos de la USART
     * @date 01/05/2018
	 * @author Esteban Carnucco
     */
	void usart_init(UART_HandleTypeDef * huart);

	/***************************************************************************************************
	 * @brief Maneja la interrupcion global  de las USART  la interrupcion de reactivacion
	 * 		  USART a traves de la linea 25 de EXIT.
	 * @date 01/05/2018
	 * @author Esteban Carnucco
	 */
	void interrupt_handler(void);
	/***************************************************************************************************
	 * @brief Este metodo prepara el mensaje String para ser enviado al destino
	 * 		  a traves de la Usart seleccionada
	 * @param fmt: parametros del mensaje a enviar
	 * @return cantidad de caracteres enviados
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	size_t send(const char *fmt, ...);

	/***************************************************************************************************
	 * @brief Metodo que determina si los datos se van a recibir mediante polling, interrupciones o DMA
	 * @param buf:buffer donde se alamacenaran los datos recibidos
	 * 		  len: tamano del buffer
	 * @return cantidad de caracteres recibidos
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	size_t read(byte_t *buf, size_t maxlen);

	/***************************************************************************************************
	 * @brief Metodo no bloqueante que devuelve los datos de un buffer circular cuando detecta un caracter de corte,
	 * 		  los cuales fueron recibidos de la USART y almacenadas en el buffer circular por medio de interrupciones
	 * @param buf:buffer donde se alamacenaran los datos recibidos
	 * 		  len: tamano del buffer
	 * 		  character_cut: caracter de corte
	 * @return cantidad de caracteres recibidos
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	size_t read_until_character(byte_t *buf, size_t maxlen,byte_t character_cut);

	/***************************************************************************************************
	 * @brief Metodo no bloqueante que devuelve una linea de caracteres  de un buffer circular,
	 * 		  los cuales fueron recibidos de la USART y almacenadas en el buffer circular por medio de interrupciones
	 * @param buf:buffer donde se alamacenaran los datos recibidos
	 * 		  len: tamano del buffer
	 * 		  character_cut: caracter de corte
	 * @return cantidad de caracteres recibidos
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	size_t read_line(byte_t *buf, size_t maxlen);

	/***************************************************************************************************
	 * @brief 	Metodo que vacia el buffer circular de recepcion del Serial,
	 * 		    eliminando todos los bytes almacenados en el
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	void flush();


	/***************************************************************************************************
	 * @brief Estos metodos permite deshabilitar o habilitar la IRQ de recepcion del puerto Serial
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	void enable_irq_rx();
	void dissable_irq_rx();

private:

	//Puntero a la estructura de datos de la haurt que se usa para enviar y recibir datos
	UART_HandleTypeDef *handler_uart;

	//Variables que indican la configuracion de recepcion  y envio de datos
	/* transmit (out of STM32) and receive (in to STM32) FIFOs */
	Buffer_Circular_Serial rx,tx;
	byte_t buffer_aux_tx[BUFFER_SIZE_SERIAL]={0};
	byte_t buffer_aux_rx[BUFFER_SIZE_SERIAL]={0};

	//variable que indica la cantidad de caracteres leidos que se encuentran almacenados en buffer_aux_rx
	size_t num_character_read;
	bool buffer_rx_full;
    /***************************************************************************************************
     * @brief Este metodo almacena en un buffer circular los datos que son recepcionados los datos que recibe
     * la USART atraves de de IRQ. Este metodo es invocado por interrupt_handler
     * @param err: Indica si hubo un error en la recepcion
     * @date 15/11/2018
	 * @author Esteban Carnucco
     */
	void save_data_fifo_rx();

    /***************************************************************************************************
     * @brief Este metodo almacena en un buffer circular los datos que van a ser enviados a otro dispositivo
     * atraves de la USART por medio de IRQ o DMA.
     * @param err: Indica si hubo un error en el envio
     * @date 15/11/2018
	 * @author Esteban Carnucco
     */
	void get_data_fifo_tx_irq();


	/***************************************************************************************************
	 * @brief Metodo no bloqueante que devuelve los datos de un buffer circular, mientras que haya datos en este,
	 * 		  los cuales fueron recibidos de la USART y almacenadas en el buffer circular por medio de interrupciones
	 * @param buf:buffer donde se alamacenaran los datos recibidos
	 * 		  len: tamano del buffer
	 * @return cantidad de caracteres recibidos
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	unsigned int read_mode_irq(byte_t *buf, size_t maxlen);


	/***************************************************************************************************
	 * @brief Metodo que envia datos atraves de la USART utilizando el mecanismo de interrupciones
	 *        Para ello los datos son almacenados en buffer circular y luego enviados cuando ocurra
	 *        la interrurpcion de transmicion
	 * @param buf:string a enviar.
	 * 		  len: tamano del string
	 * @return cantidad de caracteres enviados
	 * @date 15/11/2018
	 * @author Esteban Carnucco
	 */
	size_t send_mode_irq(byte_t *buf, size_t len);
};

#ifndef LOG
    #define LOG(fmt, ...) ( usart_debug && usart_debug->send((fmt), ##__VA_ARGS__) )
#endif

// -------------------------------------------------------------------

#endif /* DRIVER_SERIAL_HPP_ */






