

/**
 * @brief  Main MPU6050 structure
 */

class MpuDatosCalibracion
{
public:
	int16_t acel_offset_X;
	int16_t acel_offset_Y;
	int16_t acel_offset_Z;
	int16_t acel_escala_min_X;
	int16_t acel_escala_min_Y;
	int16_t acel_escala_min_Z;
	int16_t acel_escala_max_X;
	int16_t acel_escala_max_Y;
	int16_t acel_escala_max_Z;

	int16_t giro_offset_X;
	int16_t giro_offset_Y;
	int16_t giro_offset_Z;
};


/**
 * @brief  Main MPU6050 structure
 */
class Mpu_Datos_Raw  {
public:
	int16_t acel_X; 			/*!< Accelerometer value X axis */
	int16_t acel_Y; 			/*!< Accelerometer value Y axis */
	int16_t acel_Z; 			/*!< Accelerometer value Z axis */
	int16_t giro_X;			    /*!< Gyroscope value X axis */
	int16_t giro_Y;			    /*!< Gyroscope value Y axis */
	int16_t giro_Z;     		/*!< Gyroscope value Z axis */
	int16_t temperatura;     	/*!< Temperature in degrees */
	uint32_t tiempo_entre_medicion;/*!< Tiempo en ms que indica cuando se tomaron los valores sensados  */
};

class Valor_Eje
{
public:
	float x;
	float y;
	float z;
	float norma;

};

class Mpu_Datos_Escalados
{
public:
	Valor_Eje acelerometro;
	Valor_Eje giroscopio;
	int angulo;
	uint32_t tiempo_medicion;
	int16_t temperatura;
};



