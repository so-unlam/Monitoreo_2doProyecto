/*
 * Utilitarios.h
 *
 *  Created on: 17 Jul 2019
 *      Author: Esteban
 */

#ifndef APP_BIBLIOTECAS_UTILITARIOS_HPP_
#define APP_BIBLIOTECAS_UTILITARIOS_HPP_

#include "stdio.h"
#include "inttypes.h"
#include <stdlib.h>
#include <string.h>


/***************************************************************************************************/
void float_a_string(char * buffer,size_t tam,float numero)
{
	int32_t aux=100*numero;
	int32_t entera = abs(aux/100);
	int32_t decimal = abs(aux%100);

	if(numero==0)
	{
		strcpy(buffer,"0");
		return;
	}
	if(numero<0)
	{
		if(decimal<10)
			snprintf(buffer,tam,"-%" PRId32".0%" PRId32"",entera,decimal);
		else
			snprintf(buffer,tam,"-%" PRId32".%" PRId32"",entera,decimal);
	}
	else
	{
		if(decimal<10)
			snprintf(buffer,tam,"%" PRId32".0%" PRId32"",entera,decimal);
		else
			snprintf(buffer,tam,"%" PRId32".%" PRId32"",entera,decimal);
	}
}


/***************************************************************************************************/
/*
 * returns base-16 value of chars '0'-'9' and 'A'-'F';
 * does not trap invalid chars!
 */
int digit_to_dec(char digit)
{
	if (int(digit) >= 65)
		return int(digit) - 55;
	else
		return int(digit) - 48;
}

/***************************************************************************************************/
/* returns base-10 value of zero-terminated string
 * that contains only chars '+','-','0'-'9','.';
 * does not trap invalid strings!
 */
float string_to_float(char* s)
{
	long  integer_part = 0;
	float decimal_part = 0.0;
	float decimal_pivot = 0.1;
	bool isdecimal = false, isnegative = false;

	char c;
	while ( ( c = *s++) )
	{
		// skip special/sign chars
		if (c == '-') { isnegative = true; continue; }
		if (c == '+') continue;
		if (c == '.') { isdecimal = true; continue; }

		if (!isdecimal)
		{
			integer_part = (10 * integer_part) + (c - 48);
		}
		else
		{
			decimal_part += decimal_pivot * (float)(c - 48);
			decimal_pivot /= 10.0;
		}
	}
	// add integer part
	decimal_part += (float)integer_part;

	// check negative
	if (isnegative)  decimal_part = - decimal_part;

	return decimal_part;
}

//Funcion que sirve para encrptar descencriptar
bool encrypt_decrypt_xor(char *input, char *output,char * key, int size_output)
{

	int i=0;
	int j=0;
	int size_input=strlen(input);
	char aux=0;

	//el tamano de la salida debe ser el doble del de la entrada para que no se produzca overflow.
	if(size_output<(size_input*2))
		return false;

	//se produce la encriptacion o descencriptacion con XOR
	for(i = 0; i < size_input; i++)
	{
		//aplico XOR en ASCII
		aux = input[i] ^ key[i % (strlen(key)/sizeof(char))];

		//convierto cada ASCII en Hexadecimal
		//es el triple del tamano del buffer destino porque en la conversion snprintf al
		//final agrega el \0. Entonces son 2 char en hexa +el \0
		snprintf(&output[j],sizeof(char)*3,"%02X",aux);
		j+=2;
	}

	return true;
}

#endif /* APP_BIBLIOTECAS_UTILITARIOS_HPP_ */
