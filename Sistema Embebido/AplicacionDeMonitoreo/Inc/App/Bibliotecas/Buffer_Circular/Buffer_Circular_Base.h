/* @file Serial.hpp
 * @brief Implementacion del driver puerto Serial.
 * @date: Oct 29, 2018
 * @author: Waldo A. Valiente, basado en codigo C de Esteban Carnuccio.
 */

#ifndef __BUFFER_CIRCULAR_H_
#define __BUFFER_CIRCULAR_H_

#include "App/Bibliotecas/Definiciones.h"


class Buffer_Circular_Base
{

public:

   /***************************************************************************************************
	* @brief	Constructor de la clase Base por defecto.
	* @param 	huart, punero a la estructura del stm32, con los datos de la terminal serial.
	* @date 	16/11/2018
	* @author 	Esteban Carnuccio
	*/
	Buffer_Circular_Base();

	/***************************************************************************************************
	* @brief 	Destructor de la clase Base por defecto.
	* @date 	16/11/2018
	* @author 	Esteban Carnuccio
	*/
	~Buffer_Circular_Base();


    /***************************************************************************************************
     * @brief 	Metodo que  verifica si hay datos almacenados en el buffer cirular
     * @returns	true: si hay datos en el buffer y false en caso contrario
     * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
     */
	bool fifo_is_empty();

	/***************************************************************************************************
	 * @brief 	Metodo que  verifica si hay  el buffer cirular esta lleno de datos
	 * @returns	true: si no hay lugar en el buffer y false en caso contrario
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	bool fifo_is_full();

	/***************************************************************************************************
	 * @brief 	Metodo que retorna la cantidad de bytes que hay en el Buffer circular, que todavia
	 * 			no han sido leidos
	 * @returns	cantidad de bytes validos almacenados
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	size_t fifo_bytes_filled();

	/***************************************************************************************************
	 * @brief 	Metodo que retorna la cantidad de bytes libres que hay en el buffer circular
	 * @returns	cantidad de bytes libres para poder usar
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	size_t fifo_bytes_free();

	/***************************************************************************************************
	 * @brief 	Metodo que retorna el tamano total del buffer circular
	 * @returns	retorna el taano total del buffer circular
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	size_t get_fifo_size();

	/***************************************************************************************************
	 * @brief 	Metodo que retorna el porcentaje ocupado del buffer circular
	 * @returns	retorna porcentaje ocupado
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	uint8_t get_filled_percentage();

	/***************************************************************************************************
	 * @brief 	Metodo que retorna si el porcentaje ocupado del buffer circular supera determinado
	 * 			umbral
	 * @returns	true: si no supera el umbral
	 * 			false: si supera el umbral de ocupacion
	 * @date 	16/11/2018
	 * @author 	Esteban Carnuccio
	 */
	bool analyze_full_buffer_percentage();


protected:
	uint8_t m_head;
	uint8_t m_tail;
	uint8_t m_size;
	uint8_t m_capacity;

};

// -------------------------------------------------------------------

#endif /* __BUFFER_CIRCULAR_H_ */
