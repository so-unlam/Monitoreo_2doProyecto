/*
 * Buffer_Circular_GSM.h
 *
 *  Created on: 7 Aug 2019
 *      Author: Esteban
 */

#ifndef APP_BIBLIOTECAS_BUFFER_CIRCULAR_GSM_H_
#define APP_BIBLIOTECAS_BUFFER_CIRCULAR_GSM_H_

#include <App/Bibliotecas/Buffer_Circular/Buffer_Circular_Base.h>

class Buffer_Circular_GSM:public Buffer_Circular_Base
{
	public:
	   /***************************************************************************************************
		* @brief	Constructor de la clase Derivada por defecto.
		* @date 	16/11/2018
		* @author 	Esteban Carnuccio
		*/

		Buffer_Circular_GSM();

		/***************************************************************************************************
		* @brief 	Destructor de la clase Derivada por defecto.
		* @date 	16/11/2018
		* @author 	Esteban Carnuccio
		*/

		~Buffer_Circular_GSM();

	    /***************************************************************************************************
	     * @brief 	Metodo que agrega un byte al buffer circular
	     * @param   byte: byte a agregar al buffer circular
	     * @returns	cantidad de bytes agregados al buffer circular
	     * @date 	16/11/2018
		 * @author 	Esteban Carnuccio
	     */
		bool fifo_push(Peticion_Operacion_Gsm* peticion);

		/***************************************************************************************************
		 * @brief 	Metodo que obtiene un byte del buffer circular
		 * @param   byte: puntero a cadena donde se almacenara el dato extraido del buffer circular
		 * @returns	0: si ubo error o si no hay datos en buffer
		 * 			1:si se agrego al byte correctamente
		 * @date 	16/11/2018
		 * @author 	Esteban Carnuccio
		 */
		bool fifo_pop(Peticion_Operacion_Gsm* peticion);


		/***************************************************************************************************
		 * @brief 	Metodo que vacia el buffer circular, eliminando todos los bytes almacenados en el
		 * @date 	16/11/2018
		 * @author 	Esteban Carnuccio
		 */
		void flush();

	private:
		Peticion_Operacion_Gsm 	m_buffer[BUFFER_SIZE_GSM]; //buffer circular

};

#endif /* APP_BIBLIOTECAS_BUFFER_CIRCULAR_GSM_H_ */
