/*
 * Detector_Caidas.hpp
 *
 *  Created on: Feb 16, 2019
 *      Author: wav
 */

#ifndef APP_DETECTORCAIDAS_HPP_
#define APP_DETECTORCAIDAS_HPP_

#include <App/Drivers/Controlador_Acelerometro.hpp>
#include "stm32f1xx_hal.h"

extern Serial * usart_debug;

class Detector_Caidas
{
public:

	/**
	 * @brief  MPU6050 result enumeration
	 */
	enum class Resultado:int {
		SinNovedades = 	1, 	  /*!< Sin alerta seguir procesando.        */
		DetectaCaida,        /*!< Detecto una caida.                   */
		DetectaCaidaCritica, /*!< Detecto que continua caido.          */
		DetectaRecupero,     /*!< Detecto que se recupero de la caida. */
		AlertaBotonPanico	 /*!< Se presiono el boton dde panico      */
	};

	enum class Fase:int {
		Incial = 1,          /*!< Fase 1, inicial.                   */
		CaidaLibre,          /*!< Fase 2, de caida libre.            */
		Impacto,			 /*!< Fase 3, de impacto.                */
		EsperarReposoFinal,  /*!< Fase 4, de esperar al reposo final */
		ReposoFinal,         /*!< Fase 5, de reposo final.           */
		Recuperacion         /*!< Fase 6, recuperado de la caida.    */
	};

	enum class FaseEstado
	{
		FaseInicial 		  = 0x01,          /*!< Va a la fase inicial, sin importar donde este.   */
		ContinuarEnFaceActual = 0x02,    /*!< No cambia la fase.                               */
		SEGUIR_FASE_SIGUIENTE   = 0x03      /*!< Avanza los estados hasta la fase siguiente.      */
	};

	class Valores
	{
		public:
			float normalAcelerometro;	/*!< Almacena la normal del acelerometro.           */
			int angulo;               /*!< Posee el angulo relativo del aceleometro.      */
			uint32_t  tiempo;                /*!< Posee el tiempo trascurrido entre mediciones.  */
			int  sumaAngulo;			/*!< Almacene la cantidad de grados totales.        */
			int  cantMedicionesAngulo;  /*!< Almacena la cantidad total de mediciones angualres.  */

	};

	/***************************************************************************************************
	* @brief	Constructor de la clase por defecto.
	* @date   	16/02/2019
	* @author
	*/
	Detector_Caidas();

   /***************************************************************************************************
	* @brief 	Destructor de la clase por defecto.
	* @date		16/02/2019
	* @author
	*/
	~Detector_Caidas();


   /***************************************************************************************************
	* @brief 	Detecta la caida segun los datos medidos del acelerometro normalizados.
	* @param    cad, Datos del acelerometros kalibrados y fltrados.
	* @return   enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado detectarLaCaida( Mpu_Datos_Escalados &cad );

   /***************************************************************************************************
	* @brief 	Accion a ejecutar en la inicializacion.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseInicializacion( void );

   /***************************************************************************************************
	* @brief 	Acciones que va a ejecutar en la fase de caida libre.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseCaidaLibre( void );

   /***************************************************************************************************
	* @brief 	Acciones que va a ejecutar en la fase de Impacto.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseImpacto( void );

   /***************************************************************************************************
	* @brief 	Acciones que va a ejecutar en la fase de Esperar el Reposo Final.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseEsperarReposoFinal( void );

   /***************************************************************************************************
	* @brief 	Acciones que va a ejecutar en la fase de Reposo Final.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseReposoFinal( void );

   /***************************************************************************************************
	* @brief 	Acciones que va a ejecutar en la fase de Recuperacion.
	* @return   Enumeracion con los posibles resultados (Sin novedades, DetectaCaida, Detecta Recupero)
	* @date		16/02/2019
	* @author
	*/
	Resultado faseRecuperacion( void );

	FaseEstado detectarCaidaLibre( void );

	FaseEstado detectarImpacto( void );

	FaseEstado detectarReposoFinal( void );

	FaseEstado detectarRecuperacion( void );

	int esperarReposoFinal( uint32_t umbralTiempoEspera );

   /***************************************************************************************************
	* @brief 	pone el cero la suma del angulo y cantidad de mediciones. Se utiliza al resetear una fase.
	* @date		16/02/2019
	* @author
	*/
	void resetearAnguloPromedio(void);

	bool verificarCaidaCritica();

private:
	Controlador_Acel controlador_acelerometro;
	uint32_t tiempoImpactoDetectado;
	uint32_t tiempoCaidaLibreDetectada;
	uint32_t tiempoEsperaReposoFinalCompletada;
	uint32_t tiempoCaidaDetectada;
	uint32_t tiempoRecuperacion;

	Fase faseActual;

	bool msgCriticoEnviado;
	bool mostrarFalso;

	Valores valores;


};


#endif /* APP_DETECTORCAIDAS_HPP_ */
