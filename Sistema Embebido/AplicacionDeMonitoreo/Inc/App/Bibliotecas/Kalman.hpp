/*
 * Kalman.hpp
 *
 *  Created on: Feb 9, 2019
 *      Author: wav
 */

#ifndef APP_KALMAN_HPP_
#define APP_KALMAN_HPP_

/***************************************************************************************************/
#include "stm32f1xx_hal.h"


/***************************************************************************************************/

/**
 * @brief  Main Kalman structure
 *
 *   R(Process noise): La cantidad de ruido que podemos esperar de nuestro propio sistema.
 *                     Si la cantidad de ruido es constante este valor deberá ser bajo.
 *                     Es el ruido causado por el propio sistema.
 *   Q(Measurement noise): Se asemeja a la medición del ruido, o sea es la cantidad de ruido que
 *                         es causada por nuestras mediciones.Dado que esperamos que nuestras
 *                         mediciones contendrán la mayor parte del ruido, es lógico establecer
 *                         este parámetro en un número alto. Seria el ruido causado por
 *                         errores en las mediciones
 *    A(Vector State):Es la variable de transición que describe cómo un Estado se transforma de un estado
 *                    anterior a la actual.O sea indica cómo un estado anterior efectúa un nuevo estado
 *   B(Control Vector): Vector utilizado cuando hay control de movimiento
 */

class Kalman
{
public:

	   /***************************************************************************************************
		* @brief	Constructor de la clase por defecto.
		* param R:  Factor de inicializacion del Ruido de Proceso.
		* @date   	09/02/2019
		* @author
		*/
		Kalman( );

		/***************************************************************************************************
		* @brief	Constructor de la clase parametrizador.
		* param R:  Factor de inicializacion del Ruido de Proceso.
		* @date   	09/02/2019
		* @author
		*/
		Kalman( float R, float Q );

	   /***************************************************************************************************
		* @brief 	Destructor de la clase por defecto.
		* @date		09/11/2019
		* @author
		*/
		~Kalman();

	   /***************************************************************************************************
		* @brief 	Aplica filtro Kalman.
		* @param 	valor: Valor a aplicar filtro
		* @date		09/11/2019
		* @author
		*/
		/// filter
		float filtro( float valor, float u );

	   /***************************************************************************************************
		* @brief 	Devuelve la ultima medicion.
		* @return 	Ultima medicion
		* @date		09/11/2019
		* @author
		*/
		//KALMAN_lastMeasurement
		float GetUltimaMedicion( void );

	   /***************************************************************************************************
		* @brief 	Establece el valor para el Ruido de medicion (Q) y Ruido de Proceso (R).
		* @param    R Ruido de Proceso.
		* @param 	Q ruido de medicion.
		* @date		09/11/2019
		* @author
		*/
		void setRQ( float R, float Q );


	   /***************************************************************************************************
		* @brief 	Establece el valor para el ruido de medicion (Q).
		* @return 	Ultima medicion
		* @date		09/11/2019
		* @author
		*/
		void setValorRuidoDeMedicion( float Q );

	   /***************************************************************************************************
		* @brief 	Establece el valor para el ruido de Proceso (R).
		* @return 	Ultima medicion
		* @date		09/11/2019
		* @author
		*/
		void setValorRuidoDeProceso( float R );

	   /***************************************************************************************************
		* @brief 	Devuelve el valor para el ruido de medicion (Q).
		* @return 	Ultima medicion
		* @date		09/11/2019
		* @author
		*/
		float getValorRuidoDeMedicion( void );

		/***************************************************************************************************
		* @brief 	Devuelve el valor para el ruido de Proceso (R).
		* @return 	Ultima medicion
		* @date		09/11/2019
		* @author
		*/
		float getValorRuidoDeProceso( void );
private:

		float A;		/*!< vector de estado   */
		float B;		/*!< vector de control  */
		float C;		/*!< vector de medicion */
		float x;		/*!< Señal estimada sin ruido */
		float cov;		/*!< ???     */
		int   flags;    /*!< Bandera */

		float R; 		/*!< Ruido de Proceso */
		float Q;		/*!< Ruido de Medida  */
};

#endif /* APP_KALMAN_HPP_ */

/***************************************************************************************************/
