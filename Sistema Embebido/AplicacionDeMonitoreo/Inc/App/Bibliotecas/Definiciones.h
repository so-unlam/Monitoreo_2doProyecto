/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEFINICIONES_H_
#define __DEFINICIONES_H_

#include <stdint.h>
#include <stddef.h>

//==========================================================================
//Constantes utilizadas en el archivo del Controlador_Principal
//==========================================================================
//Macro que indica el tiempo espera en el main entre cada lectura
//de datos por el puerto serie
#define TIEMPO_ESPERA_LOOP     	   1
#define TIEMPO_ESPERA_GPS  	   	   60000 // cada 1 segundos se lee el GPS
#define TIEMPO_ESPERA_ACELEROMETRO 1
#define TIEMPO_ESPERA_LED_GPS  	   1000 // cada 1 segundos se prende y apaga el led del GPS
#define TIEMPO_ESPERA_LED_ACEL 	   1000 // cada 1 segundos se prende y apaga el led del ACELEROMETRO
#define TIEMPO_ESPERA_LED_GSM  	   1000 // cada 1 segundos se prende y apaga el led del GSM
#define UMBRAL_TIEMPO_IRQ_BOTON	   3000 //Umbral de tiempo que debe pasar entre una pulsacion del boton de panico
										//y otra pulsacion para que sea considerada como valida por el handler que
										//captura la interrupcion externa que genera el boton.
//==========================================================================
//Constantes utilizadas en el archivo del Buffer_circular
//==========================================================================
#define BUFFER_SIZE_SERIAL 255
#define BUFFER_SIZE_GSM 4
#define BUFFER_SIZE_CLASE_BASE 1
#define PORCENTAJE_MINIMO_BUFFER 75

//==========================================================================
//Constantes utilizadas en el archivo Serial.h
//==========================================================================

#define CHARACTER_CUT_NEW_LINE '\n' //caracter de corte para la lectura de datos del  buffer circular de recpcion
#define TAM_BYTES_RESGUARDO 3	    //cantidad de bytes que se tienen como colchon para que no se pisen datos

//==========================================================================
//Constantes utilizadas en el archivo del Controlador_Principal
//==========================================================================
//constantes que indican el limite de las variables uint8_t,uint16_t y uint32_t.
//Sirven para controlar que no se desborden los varaibles de estos tipos cuando se
//utilizan como contadores.
#define LIMITE_UINT8_T  		   255
#define LIMITE_UINT16_T  	  	   65535
#define LIMITE_UINT32_T  		   4000000000

//==========================================================================
//constantes utilizadas dentro del archivo Controlador_Acelerometro
//==========================================================================
//Umbrales que sirven para aplicar una ventan de filtro a los valores leidos.
#define UMBRAL_ACELEROMETRO_MAX  0  //Para la escala de 16G
#define UMBRAL_ACELEROMETRO_MIN 0  //Para la escala de 16G
#define UMBRAL_GIROSCOPO_MAX  1    //Para la escala de 2000 deg/s
#define UMBRAL_GIROSCOPO_MIN -1    //Para la escala de 2000 deg/s

#define CANT_PARPADEO_LED_ACEL_INICIAL 15 //cantidad de veces que debe parpadear el led del acelerometro
										 //si en la etapa inicial de configuracion se detecta un error en el mismo

//constante utilizada para la conversion de Radianes a Degree
#define RAD_TO_DEG (180/3.14);

//tiempo de espera maximo que puede tardar una operacion de Aclerometro en ejecutarse.
//pasado este tiempo se reiniciara la operacion
#define TIME_OUT_OP_ACEL 60000
//==========================================================
//constantes utilizadas dentro del archivo Controlador_Gps
//==========================================================
#define TAM_TMP_CHK 3 //son 3 por el /0
#define TAM_FILAS_TMP_WORDS 35
#define TAM_COLUMNAS_TMP_WORDS 20
#define CANT_MAX_COMPROBACIONES_GPS 100
#define LATITUD_X_DEFECTO 			-34.6084
#define LONGITUD_X_DEFECTO 			-58.3722

//==========================================================
//constantes utilizadas dentro del archivo Controlador_GSM
//==========================================================
#define CTRL_Z 26
#define TAM_NUM_TEL				   20
#define TAM_MSJ_SMS 			   100
#define TAM_MSJ_POST			   100
#define TAM_MSJ_PETICION_GSM	   100
//Direccion MAC del dispositivo GSM
#define TOKEN_DISPOSITIVO		   "98:4F:EE:01:0E:32"
#define NUM_TELEFONO_CONTACTO	   "541134926279"
//Datos del GPRS de la empresa del chip prepago de telefonia movil
#define APN_PERSONAL  "datos.personal.com"
#define USER_PERSONAL "datos"
#define PWD_PERSONAL  "datos"
//direccion donde se realiza el mensaje POST
//#define URL_SEND_NOTIFCATION "http://www.so-unlam.com.ar/laravel/mobile/send_notication_nuevo"
#define URL_SEND_NOTIFCATION "http://www.so-unlam.com.ar/laravel/mobile/send_notication_text_plain"
#define CANT_VECES_COMPROBACION_GSM 2
//tiempo de espera no bloqueante que se debe esperar entre la ejecucion de un comando AT del GSM
//y su respuesta
#define TIEMPO_ESPERA_RESPUESTA_COMANDO_AT 10
//Tiempo de espera pasado como parametro del comando AT+HTTPDATA
#define TIEMPO_ESPERA_HTTPDATA 3000

//tiempo de espera maximo que puede tardar una operacion de GSM en ejecutarse.
//pasado este tiempo se reiniciara la operacion
#define TIMEOUT_OP_GSM 60000


//===================================================================
//definiciones autogeneradas por el archivo MD5.html de acuerdo a la macro
//TOKEN_DISPOSITIVO, que consistiria en la MAC del embebido
//===================================================================

// Suceso Actualizado en MD5 = 0001 + Mac Addres SE.
#define SMD5_SUCESO_ACTUALIZADO "91e6b6819b4bb2c9c453f8b72c93c305"

// Caida Detectada en MD5 = 0002 + Mac Addres SE.
#define SMD5_CAIDA_DETECTADA    "7720cf232aa3004663b45bc6d70715ba"

// Sigue Caido en MD5 = 0003 + Mac Addres SE.
#define SMD5_SIGUE_CAIDO        "90d97d832b607c92fdb2a6188b24dcc7"

// Se Levanto en MD5 = 0004 + Mac Addres SE.
#define SMD5_SE_LEVANTO		    "f7d7dca932454be077bb7783cee6f739"

// Boton Panico en MD5 = 0005 + Mac Addres SE.
#define SMD5_BOTON_PANICO	    "0c92a2cbc743745f1f6471d84464af83"



//===================================================================
//Tipos de datos propios
//===================================================================

//Tipos de dato propio
typedef uint8_t byte_t;


//Enumeracion que indica el tipo de operacion que se debe realizar en el
//Controlador_Principal, segun el comando recibido a traves de Bluetooth
enum class Tipos_Comandos
{
	INACTIVO			= 0x01,
	INICIAR_MUESTREO    = 0x02,
	FINALIZAR_MUESTREO  = 0x03,
	INICIAR_CONTROL		= 0x04,
	FINALIZAR_CONTROL	= 0x05,
	INICIAR_LLAMADA		= 0x06,
	FINALIZAR_LLAMADA	= 0x07,
	ENVIAR_SMS			= 0x08,
	REALIZAR_POST		= 0x09,
	ACTIVAR_GPS_CONT	= 0x10,
	DESCATIVAR_GPS_CONT = 0x11
};

//Estructuras de datos que contendran los operaciones a realizar por el GSM,
//en donde se encuentra el mensaje a enviar, como asi tambien el destinatario entre otros
struct Sms
{
	byte_t num_tel[TAM_MSJ_PETICION_GSM];
	byte_t msj[TAM_MSJ_PETICION_GSM];
};

struct Llamada_telefonica
{
	byte_t num_tel[TAM_MSJ_PETICION_GSM*2];//el tamano es doble de TAM_MSJ_POST
								   //si no se pisan datos y se produce overflow
								   //debido a que se utiliza en una union
};

struct Http_Post
{
	byte_t  msj_json_post[TAM_MSJ_PETICION_GSM];
	byte_t resp_json_post[TAM_MSJ_PETICION_GSM];
};
/****************************************************************************
 * ATENCION!!!!TENER CUIDADO CON EL TAMA�O DE LOS ARRAYS DE LAS ESTRUCTURAS.
 * AL USAR UNA UNION EL TAMANO TOTAL DE TODAS LAS ESTRUCTURAS, DEBE SER IGUAL AL QUE
 * TIENE EL TAMANO MAXIMO, EN ESTE CASO HTT_POST.
 * DADO QUE AL USAR UNA UNION, SUS VARIABLES INTERNAS VAN OCUPAR SIEMPRE
 * LA MISMA POSICION DE MEMORIA. ESTO QUIERE DECIR QUE LAS VARIABLES
 * Sms, Llamada_telefonica Y Http_Post, DENTRO DE LA UNION, OCUPAN LAS MISMA
 * POSICION DE MEMORIA, POR LO QUE PUEDE OCURRIR OVERFLOW SINO SE TIENE CUIDADO.
 * PARA QUE NO SUCEDA OVERFLOW, LOS ARRAY INTERNOS DE CADA ESTRUCTURA QUE CONFORMAN
 * LA UNION, DEBEN DAR SUMADOS EL TOTAL DEL TAMANO QUE TIENE LA ESTRUCUTRA HTTPOST.
 * POR ESO EN LA ESTRUCTURA Llamada_telefonica, EL ARRAY num_tel ES EL DOBLE
 * DEL TAMANO DE  TAM_MSJ_POST.
 * ****************************************************************************
 */
struct Peticion_Operacion_Gsm
{
	Tipos_Comandos tipo_operacion;
	union
	{
		Sms sms;
		Llamada_telefonica llamada;
		Http_Post post;
	};
};
#endif

